<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_035{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_036{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_021{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:14.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:14.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Courier New,serif;font-size:20.1px;color:rgb(238,235,225);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Courier New,serif;font-size:20.1px;color:rgb(238,235,225);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture7PrintFormat/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:30.60px;top:237.28px" class="cls_002"><span class="cls_002">INTRODUCTION TO</span></div>
<div style="position:absolute;left:30.60px;top:273.28px" class="cls_002"><span class="cls_002">PROGRAMMING</span></div>
<div style="position:absolute;left:44.93px;top:353.76px" class="cls_003"><span class="cls_003">INSY 5336</span></div>
<div style="position:absolute;left:44.56px;top:407.04px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:45.31px;top:443.92px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
<div style="position:absolute;left:45.31px;top:470.80px" class="cls_005"><span class="cls_005">Information Systems and Operations Management</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:90.64px;top:64.48px" class="cls_006"><span class="cls_006">Comments</span></div>
<div style="position:absolute;left:62.51px;top:118.36px" class="cls_007"><span class="cls_007">•</span></div>
<div style="position:absolute;left:89.51px;top:118.36px" class="cls_035"><span class="cls_035">Comments</span><span class="cls_008">: notes of explanation within a program </span><span class="cls_007">(usually used</span></div>
<div style="position:absolute;left:89.51px;top:144.52px" class="cls_007"><span class="cls_007">in scripts).</span></div>
<div style="position:absolute;left:98.51px;top:174.36px" class="cls_009"><span class="cls_009">-  Ignored by Python interpreter</span></div>
<div style="position:absolute;left:98.51px;top:201.48px" class="cls_009"><span class="cls_009">-  Begin with a # character.</span></div>
<div style="position:absolute;left:62.51px;top:229.48px" class="cls_007"><span class="cls_007">•</span></div>
<div style="position:absolute;left:89.51px;top:229.48px" class="cls_035"><span class="cls_035">End-line comment</span><span class="cls_008">: appears at the end of a line of code</span></div>
<div style="position:absolute;left:98.51px;top:259.32px" class="cls_009"><span class="cls_009">-  Typically explains the purpose of that line</span></div>
<div style="position:absolute;left:62.51px;top:287.48px" class="cls_010"><span class="cls_010">•  Why including comments in code?</span></div>
<div style="position:absolute;left:98.51px;top:320.52px" class="cls_009"><span class="cls_009">-  Describe the algorithm for documentation purpose</span></div>
<div style="position:absolute;left:98.51px;top:347.40px" class="cls_009"><span class="cls_009">-  Document code author and other details</span></div>
<div style="position:absolute;left:98.51px;top:374.52px" class="cls_009"><span class="cls_009">-  Used as a debugging tool</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:183.55px;top:19.60px" class="cls_006"><span class="cls_006">Comment Examples</span></div>
<div style="position:absolute;left:110.08px;top:76.40px" class="cls_011"><span class="cls_011"># Get the name of the file and open it</span></div>
<div style="position:absolute;left:110.08px;top:96.32px" class="cls_012"><span class="cls_012">name = input('Enter file:')</span></div>
<div style="position:absolute;left:110.08px;top:115.28px" class="cls_012"><span class="cls_012">handle = open(name, 'r')</span></div>
<div style="position:absolute;left:110.08px;top:153.20px" class="cls_011"><span class="cls_011"># Count word frequency</span></div>
<div style="position:absolute;left:110.08px;top:172.40px" class="cls_012"><span class="cls_012">counts = dict()</span></div>
<div style="position:absolute;left:110.08px;top:192.32px" class="cls_012"><span class="cls_012">for line in handle:</span></div>
<div style="position:absolute;left:148.58px;top:211.28px" class="cls_012"><span class="cls_012">words = line.split()</span></div>
<div style="position:absolute;left:148.58px;top:230.24px" class="cls_012"><span class="cls_012">for word in words:</span></div>
<div style="position:absolute;left:187.08px;top:249.20px" class="cls_012"><span class="cls_012">counts[word] = counts.get(word,0) + 1</span></div>
<div style="position:absolute;left:110.08px;top:288.32px" class="cls_011"><span class="cls_011"># Find the most common word</span></div>
<div style="position:absolute;left:110.08px;top:307.28px" class="cls_012"><span class="cls_012">bigcount = None</span></div>
<div style="position:absolute;left:110.08px;top:326.24px" class="cls_012"><span class="cls_012">bigword = None</span></div>
<div style="position:absolute;left:110.08px;top:345.20px" class="cls_012"><span class="cls_012">for word,count in counts.items():</span></div>
<div style="position:absolute;left:148.58px;top:364.40px" class="cls_012"><span class="cls_012">if bigcount is None or count > bigcount:</span></div>
<div style="position:absolute;left:187.08px;top:384.32px" class="cls_012"><span class="cls_012">bigword = word</span></div>
<div style="position:absolute;left:187.08px;top:403.28px" class="cls_012"><span class="cls_012">bigcount = count</span></div>
<div style="position:absolute;left:110.08px;top:441.20px" class="cls_011"><span class="cls_011"># All done</span></div>
<div style="position:absolute;left:110.08px;top:460.40px" class="cls_012"><span class="cls_012">print(bigword, bigcount)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:68.97px;top:43.36px" class="cls_006"><span class="cls_006">Breaking Long Statements into Multiple Lines</span></div>
<div style="position:absolute;left:62.51px;top:117.52px" class="cls_013"><span class="cls_013">•</span></div>
<div style="position:absolute;left:89.51px;top:117.52px" class="cls_013"><span class="cls_013">Long statements cannot be viewed on screen without scrolling and cannot be</span></div>
<div style="position:absolute;left:89.51px;top:139.36px" class="cls_013"><span class="cls_013">printed without cutting off.</span></div>
<div style="position:absolute;left:62.51px;top:164.32px" class="cls_013"><span class="cls_013">•</span></div>
<div style="position:absolute;left:89.51px;top:164.32px" class="cls_036"><span class="cls_036">Multiline continuation character (\)</span><span class="cls_014">: </span><span class="cls_013">allows to break a statement into multiple lines</span></div>
<div style="position:absolute;left:180.23px;top:196.32px" class="cls_015"><span class="cls_015">result = var1 * 2 + var2 * 3 + \</span></div>
<div style="position:absolute;left:276.98px;top:218.40px" class="cls_015"><span class="cls_015">var3 * 4 + var4 * 5</span></div>
<div style="position:absolute;left:62.51px;top:265.36px" class="cls_013"><span class="cls_013">•</span></div>
<div style="position:absolute;left:89.51px;top:265.36px" class="cls_013"><span class="cls_013">Any part of a statement that is enclosed in parentheses can be broken without the</span></div>
<div style="position:absolute;left:89.51px;top:287.44px" class="cls_013"><span class="cls_013">line continuation character</span></div>
<div style="position:absolute;left:140.97px;top:320.16px" class="cls_015"><span class="cls_015">print("Monday's sales are", monday,</span></div>
<div style="position:absolute;left:205.47px;top:341.28px" class="cls_015"><span class="cls_015">"and Tuesday's sales are", tuesday,</span></div>
<div style="position:absolute;left:205.47px;top:363.12px" class="cls_015"><span class="cls_015">"and Wednesday's sales are", wednesday)</span></div>
<div style="position:absolute;left:140.97px;top:406.08px" class="cls_015"><span class="cls_015">total = (value1 + value2 +</span></div>
<div style="position:absolute;left:237.72px;top:428.16px" class="cls_015"><span class="cls_015">value3 + value4 +</span></div>
<div style="position:absolute;left:237.72px;top:449.28px" class="cls_015"><span class="cls_015">value5 + value6)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:68.97px;top:43.36px" class="cls_006"><span class="cls_006">Data Output</span></div>
<div style="position:absolute;left:68.97px;top:111.52px" class="cls_013"><span class="cls_013">•</span></div>
<div style="position:absolute;left:95.97px;top:111.52px" class="cls_013"><span class="cls_013">Built-in</span><span class="cls_016"> print </span><span class="cls_013">display output on the screen.</span></div>
<div style="position:absolute;left:104.97px;top:136.52px" class="cls_017"><span class="cls_017">-  Augment: data to be printed on the screen</span></div>
<div style="position:absolute;left:104.97px;top:160.52px" class="cls_017"><span class="cls_017">-  Newline character at end of printed data (‘\n’)</span></div>
<div style="position:absolute;left:104.97px;top:183.56px" class="cls_017"><span class="cls_017">-  Special argument </span><span class="cls_018">end = ‘delimiter’ </span><span class="cls_017">causes print to place delimiter at end of data instead</span></div>
<div style="position:absolute;left:127.47px;top:203.48px" class="cls_017"><span class="cls_017">of newline character</span></div>
<div style="position:absolute;left:104.97px;top:227.48px" class="cls_017"><span class="cls_017">-  Doe not automatically display a space after the prompt</span></div>
<div style="position:absolute;left:68.97px;top:251.40px" class="cls_009"><span class="cls_009">•</span></div>
<div style="position:absolute;left:95.97px;top:251.40px" class="cls_019"><span class="cls_019">print</span><span class="cls_009"> function uses space as item separator</span></div>
<div style="position:absolute;left:104.97px;top:277.40px" class="cls_017"><span class="cls_017">-  Special argument </span><span class="cls_018">sep = ‘delimiter’ </span><span class="cls_017">causes print to use delimiter as item separator</span></div>
<div style="position:absolute;left:68.97px;top:301.56px" class="cls_009"><span class="cls_009">•</span></div>
<div style="position:absolute;left:95.97px;top:301.56px" class="cls_009"><span class="cls_009">Special characters appearing in strings are treated as commands embedded in</span></div>
<div style="position:absolute;left:95.97px;top:325.56px" class="cls_009"><span class="cls_009">string ( newline character \n, horizontal tab \t)</span></div>
<div style="position:absolute;left:104.97px;top:351.56px" class="cls_017"><span class="cls_017">-  Special characters are regular characters (e.g., ‘n’) preceded by the escape character “\”</span></div>
<div style="position:absolute;left:104.97px;top:374.36px" class="cls_017"><span class="cls_017">-  E.g., </span><span class="cls_018">print(‘One\nTwo\nThree’)</span></div>
<div style="position:absolute;left:159.34px;top:398.36px" class="cls_018"><span class="cls_018">print(‘Mon\tTues\tWed’)</span></div>
<div style="position:absolute;left:68.97px;top:422.52px" class="cls_009"><span class="cls_009">•</span></div>
<div style="position:absolute;left:95.97px;top:422.52px" class="cls_009"><span class="cls_009">When +  operator used on two strings, it performs string concatenation</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:68.97px;top:20.08px" class="cls_006"><span class="cls_006">Formatting Numbers</span></div>
<div style="position:absolute;left:47.14px;top:62.16px" class="cls_020"><span class="cls_020">• built-in </span><span class="cls_021">format</span><span class="cls_020"> function</span></div>
<div style="position:absolute;left:83.14px;top:97.12px" class="cls_022"><span class="cls_022">- Two arguments:</span></div>
<div style="position:absolute;left:119.14px;top:125.04px" class="cls_023"><span class="cls_023">• Numeric value to be formatted</span></div>
<div style="position:absolute;left:119.14px;top:151.20px" class="cls_023"><span class="cls_023">• Format specifier</span></div>
<div style="position:absolute;left:83.14px;top:178.24px" class="cls_022"><span class="cls_022">- Returns string containing formatted number</span></div>
<div style="position:absolute;left:83.14px;top:206.08px" class="cls_022"><span class="cls_022">- Format specifier typically includes precision and data type</span></div>
<div style="position:absolute;left:119.14px;top:235.20px" class="cls_023"><span class="cls_023">• Can be used to indicate scientific notation, comma separators, and</span></div>
<div style="position:absolute;left:137.14px;top:256.08px" class="cls_023"><span class="cls_023">the minimum field width used to display the value</span></div>
<div style="position:absolute;left:465.52px;top:294.88px" class="cls_028"><span class="cls_028">The .2f specifies the precision. It</span></div>
<div style="position:absolute;left:47.14px;top:293.20px" class="cls_024"><span class="cls_024">>>>format(</span><span class="cls_025">12345.6789</span><span class="cls_024">, </span><span class="cls_025">‘.2f’</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:465.52px;top:310.96px" class="cls_028"><span class="cls_028">indicates that we want to round the</span></div>
<div style="position:absolute;left:47.14px;top:317.20px" class="cls_026"><span class="cls_026">12345.68</span></div>
<div style="position:absolute;left:465.52px;top:328.00px" class="cls_028"><span class="cls_028">number to two decimal places.</span></div>
<div style="position:absolute;left:47.14px;top:341.20px" class="cls_024"><span class="cls_024">>>>format(</span><span class="cls_025">12345.6789</span><span class="cls_024">, </span><span class="cls_025">‘.2e’</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:465.52px;top:352.96px" class="cls_028"><span class="cls_028">The .2e formats the number in</span></div>
<div style="position:absolute;left:47.14px;top:365.20px" class="cls_027"><span class="cls_027">1.23e+04</span></div>
<div style="position:absolute;left:465.52px;top:369.04px" class="cls_028"><span class="cls_028">scientific notation with two decimal</span></div>
<div style="position:absolute;left:465.52px;top:386.08px" class="cls_028"><span class="cls_028">places.</span></div>
<div style="position:absolute;left:47.14px;top:389.20px" class="cls_024"><span class="cls_024">>>>format(</span><span class="cls_025">127.899</span><span class="cls_024">, </span><span class="cls_025">‘7.2f’</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:465.52px;top:411.28px" class="cls_028"><span class="cls_028">Display the number in a field of 7</span></div>
<div style="position:absolute;left:59.14px;top:413.20px" class="cls_027"><span class="cls_027">127.90</span></div>
<div style="position:absolute;left:465.52px;top:427.12px" class="cls_028"><span class="cls_028">spaces with 2 decimal places .</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.83px;top:43.36px" class="cls_006"><span class="cls_006">Formatting Numbers</span></div>
<div style="position:absolute;left:36.00px;top:104.64px" class="cls_020"><span class="cls_020">• The </span><span class="cls_029">%</span><span class="cls_020"> symbol can be used in the format string of </span><span class="cls_029">format</span></div>
<div style="position:absolute;left:63.00px;top:134.64px" class="cls_020"><span class="cls_020">function to format number as percentage</span></div>
<div style="position:absolute;left:124.56px;top:175.84px" class="cls_024"><span class="cls_024">>>>format(</span><span class="cls_025">0.5</span><span class="cls_024">, </span><span class="cls_025">‘.0%’</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:539.85px;top:181.12px" class="cls_028"><span class="cls_028">0 decimal points.</span></div>
<div style="position:absolute;left:124.56px;top:199.84px" class="cls_027"><span class="cls_027">50%</span></div>
<div style="position:absolute;left:36.00px;top:236.64px" class="cls_020"><span class="cls_020">• To format an integer using </span><span class="cls_029">format</span><span class="cls_020"> function:</span></div>
<div style="position:absolute;left:72.00px;top:270.64px" class="cls_022"><span class="cls_022">- Use </span><span class="cls_030">d</span><span class="cls_022"> as the type designator</span></div>
<div style="position:absolute;left:72.00px;top:300.64px" class="cls_022"><span class="cls_022">- Do not specify precision</span></div>
<div style="position:absolute;left:72.00px;top:328.72px" class="cls_022"><span class="cls_022">- Can still use </span><span class="cls_030">format</span><span class="cls_022"> function to set field width or comma</span></div>
<div style="position:absolute;left:94.50px;top:353.68px" class="cls_022"><span class="cls_022">separator</span></div>
<div style="position:absolute;left:136.56px;top:400.24px" class="cls_024"><span class="cls_024">>>>format(</span><span class="cls_025">123456</span><span class="cls_024">, </span><span class="cls_025">‘10,d’</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:539.85px;top:415.12px" class="cls_028"><span class="cls_028">With a comma separator</span></div>
<div style="position:absolute;left:172.56px;top:424.24px" class="cls_027"><span class="cls_027">123,456</span></div>
<div style="position:absolute;left:539.85px;top:430.96px" class="cls_028"><span class="cls_028">in a filed that is 10 spaces</span></div>
<div style="position:absolute;left:539.85px;top:448.00px" class="cls_028"><span class="cls_028">wide</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.83px;top:43.36px" class="cls_006"><span class="cls_006">Formatter with Multiple Place Holder</span></div>
<div style="position:absolute;left:489.52px;top:83.52px" class="cls_023"><span class="cls_023">Place holder</span></div>
<div style="position:absolute;left:79.97px;top:104.64px" class="cls_023"><span class="cls_023">New version</span></div>
<div style="position:absolute;left:65.03px;top:162.16px" class="cls_024"><span class="cls_024">>>>print(</span><span class="cls_025">‘Hi, My name is</span></div>
<div style="position:absolute;left:473.03px;top:162.16px" class="cls_025"><span class="cls_025">am {} years</span></div>
<div style="position:absolute;left:65.03px;top:186.16px" class="cls_025"><span class="cls_025">old’</span><span class="cls_024">.format(</span><span class="cls_025">“Jane”</span><span class="cls_024">, </span><span class="cls_025">19</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:101.03px;top:210.16px" class="cls_027"><span class="cls_027">My name is Jane and I am 19 years old.</span></div>
<div style="position:absolute;left:90.81px;top:291.60px" class="cls_023"><span class="cls_023">Old version</span></div>
<div style="position:absolute;left:65.03px;top:352.96px" class="cls_024"><span class="cls_024">>>>print(</span><span class="cls_025">‘Hi, My name is %s and I am %d years old’</span></div>
<div style="position:absolute;left:101.03px;top:376.96px" class="cls_024"><span class="cls_024">%(</span><span class="cls_025">“Jane”</span><span class="cls_024">, </span><span class="cls_025">19</span><span class="cls_024">)</span></div>
<div style="position:absolute;left:101.03px;top:400.96px" class="cls_027"><span class="cls_027">My name is Jane and I am 19 years old.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:68.90px;top:44.64px" class="cls_033"><span class="cls_033">Acknowledgements/Contributions</span></div>
<div style="position:absolute;left:106.02px;top:112.08px" class="cls_031"><span class="cls_031">§</span><span class="cls_032">  These slides are adapted from Copyright 2018 Copyright</span></div>
<div style="position:absolute;left:133.02px;top:132.96px" class="cls_032"><span class="cls_032">Pearson Inc -</span><span class="cls_034"> Starting Out with Python (4th Edition) </span><span class="cls_023">by</span></div>
<div style="position:absolute;left:133.02px;top:155.04px" class="cls_023"><span class="cls_023">Tony Gaddis, Pearson, 2014, ISBN 13: 978-0134444321</span></div>
<div style="position:absolute;left:137.37px;top:176.88px" class="cls_032"><span class="cls_032">and Copyright 2010- Charles R. Severance (</span><A HREF="http://www.dr-/">www.dr-</A> </div>
<div style="position:absolute;left:132.42px;top:198.96px" class="cls_032"><span class="cls_032">chuck.com) of the University of Michigan School of</span></div>
<div style="position:absolute;left:132.42px;top:221.04px" class="cls_032"><span class="cls_032">Information and made available under a Creative</span></div>
<div style="position:absolute;left:132.42px;top:241.92px" class="cls_032"><span class="cls_032">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:132.42px;top:264.00px" class="cls_032"><span class="cls_032">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:132.42px;top:284.88px" class="cls_032"><span class="cls_032">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:132.42px;top:306.96px" class="cls_032"><span class="cls_032">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:132.42px;top:329.04px" class="cls_032"><span class="cls_032">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:132.42px;top:349.92px" class="cls_032"><span class="cls_032">materials.</span></div>
<div style="position:absolute;left:106.02px;top:403.92px" class="cls_031"><span class="cls_031">§</span><span class="cls_032">  Updated 2018: Zhuojun Gu, University of Texas at</span></div>
<div style="position:absolute;left:132.97px;top:426.00px" class="cls_032"><span class="cls_032">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7PrintFormat/background10.jpg" width=720 height=540></div>
</div>

</body>
</html>
