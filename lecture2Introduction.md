<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Courier New,serif;font-size:16.0px;color:rgb(128,98,161);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_011{font-family:Courier New,serif;font-size:16.0px;color:rgb(128,98,161);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Calibri",serif;font-size:18.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:"Calibri",serif;font-size:18.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_046{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:18.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:18.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:18.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:18.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Courier New,serif;font-size:16.0px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Courier New,serif;font-size:16.0px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Courier New,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Courier New,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:16.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:16.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:16.0px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:16.0px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:16.0px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:16.0px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:26.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:26.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_037{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_047{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_039{font-family:Courier New,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_039{font-family:Courier New,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_040{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_040{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:16.0px;color:rgb(102,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_045{font-family:Arial,serif;font-size:16.0px;color:rgb(255,152,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_045{font-family:Arial,serif;font-size:16.0px;color:rgb(255,152,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_042{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,248,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,248,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_043{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,145,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,145,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture1Introduction/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:231.69px;top:231.76px" class="cls_002"><span class="cls_002">Introduction</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.62px;top:29.20px" class="cls_003"><span class="cls_003">History of Python</span></div>
<div style="position:absolute;left:61.40px;top:88.00px" class="cls_004"><span class="cls_004">•  Python was conceived in late 1980’s</span></div>
<div style="position:absolute;left:61.40px;top:117.04px" class="cls_004"><span class="cls_004">•  Van Rossum is Python’s principal author</span></div>
<div style="position:absolute;left:61.40px;top:145.84px" class="cls_004"><span class="cls_004">•  Python was named after the BBC TV show Monty</span></div>
<div style="position:absolute;left:88.40px;top:169.84px" class="cls_004"><span class="cls_004">Python’s Flying Circus</span></div>
<div style="position:absolute;left:61.40px;top:198.88px" class="cls_004"><span class="cls_004">•  Python 2.0, released on Oct. 16, 2000</span></div>
<div style="position:absolute;left:61.40px;top:227.92px" class="cls_004"><span class="cls_004">•  Python 3.0, released on Dec. 3, 2008</span></div>
<div style="position:absolute;left:97.40px;top:255.92px" class="cls_005"><span class="cls_005">-  Backwards incompatible release</span></div>
<div style="position:absolute;left:61.40px;top:279.04px" class="cls_004"><span class="cls_004">•  The Python interpreter is written in C</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.62px;top:29.20px" class="cls_003"><span class="cls_003">Why Python</span></div>
<div style="position:absolute;left:61.40px;top:87.84px" class="cls_006"><span class="cls_006">• It’s open source software</span></div>
<div style="position:absolute;left:97.40px;top:121.92px" class="cls_007"><span class="cls_007">- Free to use</span></div>
<div style="position:absolute;left:97.40px;top:147.84px" class="cls_007"><span class="cls_007">- Source code is accessible</span></div>
<div style="position:absolute;left:97.40px;top:173.04px" class="cls_007"><span class="cls_007">- Online community is huge</span></div>
<div style="position:absolute;left:61.40px;top:200.88px" class="cls_006"><span class="cls_006">• Platform independent</span></div>
<div style="position:absolute;left:61.40px;top:234.96px" class="cls_006"><span class="cls_006">• Powerful</span></div>
<div style="position:absolute;left:97.40px;top:269.04px" class="cls_007"><span class="cls_007">- Dynamic typing (check type at run time)</span></div>
<div style="position:absolute;left:97.40px;top:294.00px" class="cls_007"><span class="cls_007">- Automatic memory management</span></div>
<div style="position:absolute;left:97.40px;top:319.92px" class="cls_007"><span class="cls_007">- Library Utilities</span></div>
<div style="position:absolute;left:97.40px;top:345.84px" class="cls_007"><span class="cls_007">- Third party utilities (scikit-learn, Scipy, Numpy, Pandas)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.62px;top:29.20px" class="cls_003"><span class="cls_003">Who Uses Python</span></div>
<div style="position:absolute;left:61.40px;top:87.84px" class="cls_006"><span class="cls_006">• Star Wars special effects team use</span></div>
<div style="position:absolute;left:88.40px;top:116.88px" class="cls_006"><span class="cls_006">Python to glue tools using various</span></div>
<div style="position:absolute;left:88.40px;top:145.92px" class="cls_006"><span class="cls_006">programming languages</span></div>
<div style="position:absolute;left:61.40px;top:283.92px" class="cls_006"><span class="cls_006">• Dropbox, the popular cloud</span></div>
<div style="position:absolute;left:88.40px;top:312.96px" class="cls_006"><span class="cls_006">storage software is written in</span></div>
<div style="position:absolute;left:88.40px;top:342.00px" class="cls_006"><span class="cls_006">Python</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.62px;top:29.20px" class="cls_003"><span class="cls_003">What is Code? Software? A Program?</span></div>
<div style="position:absolute;left:61.40px;top:87.84px" class="cls_006"><span class="cls_006">• A sequence of stored instructions</span></div>
<div style="position:absolute;left:97.40px;top:121.92px" class="cls_007"><span class="cls_007">- The programmers instructs the computer what to do and in</span></div>
<div style="position:absolute;left:119.90px;top:143.04px" class="cls_007"><span class="cls_007">what sequence</span></div>
<div style="position:absolute;left:97.40px;top:168.96px" class="cls_007"><span class="cls_007">- Using a computer program we can automate and perform</span></div>
<div style="position:absolute;left:119.90px;top:191.04px" class="cls_007"><span class="cls_007">tasks faster and more accurately</span></div>
<div style="position:absolute;left:61.40px;top:217.92px" class="cls_006"><span class="cls_006">• A good algorithm is key to robust functionality</span></div>
<div style="position:absolute;left:61.40px;top:252.96px" class="cls_006"><span class="cls_006">• A piece of creative art - particularly when we do</span></div>
<div style="position:absolute;left:88.40px;top:281.04px" class="cls_006"><span class="cls_006">a good job on user experience</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:203.27px;top:17.28px" class="cls_008"><span class="cls_008">Generic Computer</span></div>
<div style="position:absolute;left:281.09px;top:79.36px" class="cls_009"><span class="cls_009">Software</span></div>
<div style="position:absolute;left:427.99px;top:86.96px" class="cls_005"><span class="cls_005">What</span></div>
<div style="position:absolute;left:425.83px;top:106.16px" class="cls_005"><span class="cls_005">Next?</span></div>
<div style="position:absolute;left:153.80px;top:158.16px" class="cls_007"><span class="cls_007">Input</span></div>
<div style="position:absolute;left:316.88px;top:157.68px" class="cls_007"><span class="cls_007">Central</span></div>
<div style="position:absolute;left:130.05px;top:179.28px" class="cls_007"><span class="cls_007">and Output</span></div>
<div style="position:absolute;left:301.55px;top:179.52px" class="cls_007"><span class="cls_007">Processing</span></div>
<div style="position:absolute;left:142.15px;top:201.36px" class="cls_007"><span class="cls_007">Devices</span></div>
<div style="position:absolute;left:329.80px;top:200.64px" class="cls_007"><span class="cls_007">Unit</span></div>
<div style="position:absolute;left:504.56px;top:245.52px" class="cls_007"><span class="cls_007">Secondary</span></div>
<div style="position:absolute;left:515.01px;top:267.60px" class="cls_007"><span class="cls_007">Memory</span></div>
<div style="position:absolute;left:326.68px;top:352.56px" class="cls_007"><span class="cls_007">Main</span></div>
<div style="position:absolute;left:313.88px;top:373.44px" class="cls_007"><span class="cls_007">Memory</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:202.20px;top:16.56px" class="cls_008"><span class="cls_008">Generic Computer</span></div>
<div style="position:absolute;left:281.09px;top:79.36px" class="cls_009"><span class="cls_009">Software</span></div>
<div style="position:absolute;left:427.99px;top:86.96px" class="cls_005"><span class="cls_005">What</span></div>
<div style="position:absolute;left:425.83px;top:106.16px" class="cls_005"><span class="cls_005">Next?</span></div>
<div style="position:absolute;left:153.80px;top:158.16px" class="cls_007"><span class="cls_007">Input</span></div>
<div style="position:absolute;left:316.88px;top:157.68px" class="cls_007"><span class="cls_007">Central</span></div>
<div style="position:absolute;left:130.05px;top:179.28px" class="cls_007"><span class="cls_007">and Output</span></div>
<div style="position:absolute;left:301.55px;top:179.52px" class="cls_007"><span class="cls_007">Processing</span></div>
<div style="position:absolute;left:142.15px;top:201.36px" class="cls_007"><span class="cls_007">Devices</span></div>
<div style="position:absolute;left:329.80px;top:200.64px" class="cls_007"><span class="cls_007">Unit</span></div>
<div style="position:absolute;left:504.56px;top:245.52px" class="cls_007"><span class="cls_007">Secondary</span></div>
<div style="position:absolute;left:515.01px;top:267.60px" class="cls_007"><span class="cls_007">Memory</span></div>
<div style="position:absolute;left:365.11px;top:275.84px" class="cls_010"><span class="cls_010">if x&lt; 3: print</span></div>
<div style="position:absolute;left:326.72px;top:351.60px" class="cls_007"><span class="cls_007">Main</span></div>
<div style="position:absolute;left:313.92px;top:372.48px" class="cls_007"><span class="cls_007">Memory</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:202.20px;top:16.56px" class="cls_008"><span class="cls_008">Generic Computer</span></div>
<div style="position:absolute;left:281.09px;top:79.36px" class="cls_009"><span class="cls_009">Software</span></div>
<div style="position:absolute;left:427.99px;top:86.96px" class="cls_005"><span class="cls_005">What</span></div>
<div style="position:absolute;left:425.83px;top:106.16px" class="cls_005"><span class="cls_005">Next?</span></div>
<div style="position:absolute;left:153.80px;top:158.16px" class="cls_007"><span class="cls_007">Input</span></div>
<div style="position:absolute;left:316.49px;top:156.96px" class="cls_007"><span class="cls_007">Central</span></div>
<div style="position:absolute;left:130.05px;top:179.28px" class="cls_007"><span class="cls_007">and Output</span></div>
<div style="position:absolute;left:301.01px;top:178.56px" class="cls_007"><span class="cls_007">Processing</span></div>
<div style="position:absolute;left:142.15px;top:201.36px" class="cls_007"><span class="cls_007">Devices</span></div>
<div style="position:absolute;left:329.45px;top:200.16px" class="cls_007"><span class="cls_007">Unit</span></div>
<div style="position:absolute;left:504.56px;top:245.52px" class="cls_007"><span class="cls_007">Secondary</span></div>
<div style="position:absolute;left:368.23px;top:263.36px" class="cls_011"><span class="cls_011">01001001</span></div>
<div style="position:absolute;left:515.01px;top:267.60px" class="cls_007"><span class="cls_007">Memory</span></div>
<div style="position:absolute;left:368.23px;top:282.32px" class="cls_011"><span class="cls_011">00111001</span></div>
<div style="position:absolute;left:326.72px;top:351.60px" class="cls_007"><span class="cls_007">Main</span></div>
<div style="position:absolute;left:313.92px;top:372.48px" class="cls_007"><span class="cls_007">Memory</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:254.28px;top:16.56px" class="cls_008"><span class="cls_008">‘C’ Compiler</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:203.16px;top:16.56px" class="cls_008"><span class="cls_008">Python Interpreter</span></div>
<div style="position:absolute;left:173.06px;top:266.16px" class="cls_013"><span class="cls_013">Intermediate code</span></div>
<div style="position:absolute;left:103.61px;top:325.60px" class="cls_012"><span class="cls_012">§  Python code is converted to Intermediate code by the</span></div>
<div style="position:absolute;left:130.61px;top:349.60px" class="cls_012"><span class="cls_012">compiler inside the interpreter</span></div>
<div style="position:absolute;left:103.61px;top:378.64px" class="cls_012"><span class="cls_012">§  The byte code is a low level, platform independent</span></div>
<div style="position:absolute;left:130.61px;top:402.64px" class="cls_012"><span class="cls_012">code</span></div>
<div style="position:absolute;left:103.61px;top:431.68px" class="cls_012"><span class="cls_012">§  Virtual machine is an application that executes the</span></div>
<div style="position:absolute;left:130.61px;top:455.68px" class="cls_012"><span class="cls_012">intermediate code</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:265.05px;top:95.04px" class="cls_014"><span class="cls_014">“.py” file (python scripts)</span></div>
<div style="position:absolute;left:220.75px;top:167.04px" class="cls_015"><span class="cls_015">Python  Interpreter (Winodws/Mac OS)</span></div>
<div style="position:absolute;left:287.87px;top:239.04px" class="cls_014"><span class="cls_014">Operating Systems</span></div>
<div style="position:absolute;left:272.16px;top:311.04px" class="cls_014"><span class="cls_014">Hardware/Machine Code</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:156.22px;top:16.56px" class="cls_008"><span class="cls_008">Compiler vs. Interpreter</span></div>
<div style="position:absolute;left:75.84px;top:117.68px" class="cls_016"><span class="cls_016">Compiler characteristics:</span></div>
<div style="position:absolute;left:403.32px;top:117.68px" class="cls_016"><span class="cls_016">Interpreter characteristics:</span></div>
<div style="position:absolute;left:75.84px;top:149.60px" class="cls_016"><span class="cls_016">§ spends a lot of time</span></div>
<div style="position:absolute;left:403.32px;top:149.60px" class="cls_016"><span class="cls_016">§ relatively little time is spent</span></div>
<div style="position:absolute;left:102.84px;top:175.52px" class="cls_016"><span class="cls_016">analyzing and processing</span></div>
<div style="position:absolute;left:430.32px;top:175.52px" class="cls_016"><span class="cls_016">analyzing and processing</span></div>
<div style="position:absolute;left:102.84px;top:202.64px" class="cls_016"><span class="cls_016">the program</span></div>
<div style="position:absolute;left:430.32px;top:202.64px" class="cls_016"><span class="cls_016">the program</span></div>
<div style="position:absolute;left:75.84px;top:233.60px" class="cls_016"><span class="cls_016">§ the resulting executable is</span></div>
<div style="position:absolute;left:403.32px;top:233.60px" class="cls_016"><span class="cls_016">§ the resulting code is some</span></div>
<div style="position:absolute;left:102.84px;top:259.52px" class="cls_016"><span class="cls_016">some form of machine-</span></div>
<div style="position:absolute;left:430.32px;top:259.52px" class="cls_016"><span class="cls_016">sort of intermediate code</span></div>
<div style="position:absolute;left:102.84px;top:286.64px" class="cls_016"><span class="cls_016">specific binary code</span></div>
<div style="position:absolute;left:403.32px;top:291.68px" class="cls_016"><span class="cls_016">§ the resulting code is</span></div>
<div style="position:absolute;left:75.84px;top:317.60px" class="cls_016"><span class="cls_016">§ the computer hardware</span></div>
<div style="position:absolute;left:430.32px;top:317.60px" class="cls_016"><span class="cls_016">interpreted by another</span></div>
<div style="position:absolute;left:102.84px;top:344.72px" class="cls_016"><span class="cls_016">interprets (executes) the</span></div>
<div style="position:absolute;left:430.32px;top:344.72px" class="cls_016"><span class="cls_016">program (virtual machine)</span></div>
<div style="position:absolute;left:102.84px;top:370.64px" class="cls_016"><span class="cls_016">resulting code</span></div>
<div style="position:absolute;left:403.32px;top:375.68px" class="cls_016"><span class="cls_016">§ program execution is slow</span></div>
<div style="position:absolute;left:75.84px;top:401.60px" class="cls_016"><span class="cls_016">§ program execution is fast</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:184.11px;top:16.56px" class="cls_008"><span class="cls_008">Anaconda & Jupyter</span></div>
<div style="position:absolute;left:276.16px;top:59.76px" class="cls_008"><span class="cls_008">Notebook</span></div>
<div style="position:absolute;left:107.95px;top:144.88px" class="cls_017"><span class="cls_017">§ Anaconda is the most popular</span></div>
<div style="position:absolute;left:134.95px;top:190.96px" class="cls_017"><span class="cls_017">Python Data Science platform</span></div>
<div style="position:absolute;left:107.95px;top:244.96px" class="cls_017"><span class="cls_017">§ Anaconda is downloadable at:</span></div>
<div style="position:absolute;left:134.95px;top:290.80px" class="cls_046"><span class="cls_046"> </span><A HREF="https://www.anaconda.com/">https://www.anaconda.com</A> </div>
<div style="position:absolute;left:107.95px;top:344.80px" class="cls_017"><span class="cls_017">§ Jupyter Notebook is a web based</span></div>
<div style="position:absolute;left:134.95px;top:390.88px" class="cls_017"><span class="cls_017">Python IDE</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:225.24px;top:16.56px" class="cls_008"><span class="cls_008">Starting Python</span></div>
<div style="position:absolute;left:94.90px;top:122.88px" class="cls_019"><span class="cls_019">csev$</span><span class="cls_020"> python3</span></div>
<div style="position:absolute;left:94.90px;top:143.76px" class="cls_019"><span class="cls_019">Python 3.5.1 (v3.5.1:37a07cee5969, Dec  5 2015, 21:12:44)</span></div>
<div style="position:absolute;left:101.61px;top:165.84px" class="cls_019"><span class="cls_019">[GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwinType</span></div>
<div style="position:absolute;left:94.90px;top:186.72px" class="cls_019"><span class="cls_019">"help", "copyright", "credits" or "license" for more information.</span></div>
<div style="position:absolute;left:94.90px;top:208.80px" class="cls_019"><span class="cls_019">>>> </span><span class="cls_020">x = 1</span></div>
<div style="position:absolute;left:94.90px;top:230.88px" class="cls_021"><span class="cls_021">>>></span><span class="cls_020"> print(x)</span></div>
<div style="position:absolute;left:94.90px;top:251.76px" class="cls_021"><span class="cls_021">1</span></div>
<div style="position:absolute;left:94.90px;top:273.84px" class="cls_021"><span class="cls_021">>>> </span><span class="cls_020">x = x + 1</span></div>
<div style="position:absolute;left:94.90px;top:294.72px" class="cls_021"><span class="cls_021">>>></span><span class="cls_020"> print(x)</span></div>
<div style="position:absolute;left:94.90px;top:316.80px" class="cls_021"><span class="cls_021">2</span></div>
<div style="position:absolute;left:94.90px;top:338.88px" class="cls_021"><span class="cls_021">>>></span><span class="cls_020"> exit()</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:164.88px;top:16.56px" class="cls_008"><span class="cls_008">A simple Python script</span></div>
<div style="position:absolute;left:26.93px;top:99.44px" class="cls_022"><span class="cls_022">name = input('Enter file:')</span></div>
<div style="position:absolute;left:26.93px;top:118.40px" class="cls_022"><span class="cls_022">handle = open(name)</span></div>
<div style="position:absolute;left:26.93px;top:157.52px" class="cls_023"><span class="cls_023">counts = dict()</span></div>
<div style="position:absolute;left:26.93px;top:177.44px" class="cls_023"><span class="cls_023">for line in handle:</span></div>
<div style="position:absolute;left:65.33px;top:196.40px" class="cls_023"><span class="cls_023">words = line.split()</span></div>
<div style="position:absolute;left:65.33px;top:215.36px" class="cls_023"><span class="cls_023">for word in words:</span></div>
<div style="position:absolute;left:103.83px;top:234.32px" class="cls_023"><span class="cls_023">counts[word] = counts.get(word,0) + 1</span></div>
<div style="position:absolute;left:26.93px;top:273.44px" class="cls_024"><span class="cls_024">bigcount = None</span></div>
<div style="position:absolute;left:26.93px;top:292.40px" class="cls_024"><span class="cls_024">bigword = None</span></div>
<div style="position:absolute;left:26.93px;top:312.32px" class="cls_024"><span class="cls_024">for word,count in counts.items():</span></div>
<div style="position:absolute;left:65.33px;top:331.52px" class="cls_024"><span class="cls_024">if bigcount is None or count > bigcount:</span></div>
<div style="position:absolute;left:103.83px;top:350.48px" class="cls_024"><span class="cls_024">bigword = word</span></div>
<div style="position:absolute;left:103.83px;top:369.44px" class="cls_024"><span class="cls_024">bigcount = count</span></div>
<div style="position:absolute;left:26.93px;top:408.32px" class="cls_025"><span class="cls_025">print(bigword, bigcount)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:217.56px;top:16.56px" class="cls_008"><span class="cls_008">Reserved Words</span></div>
<div style="position:absolute;left:149.66px;top:124.16px" class="cls_026"><span class="cls_026">False</span></div>
<div style="position:absolute;left:221.61px;top:124.16px" class="cls_026"><span class="cls_026">class</span></div>
<div style="position:absolute;left:293.66px;top:124.16px" class="cls_026"><span class="cls_026">return  is</span></div>
<div style="position:absolute;left:437.66px;top:124.16px" class="cls_026"><span class="cls_026">finally</span></div>
<div style="position:absolute;left:149.66px;top:142.88px" class="cls_026"><span class="cls_026">None</span></div>
<div style="position:absolute;left:221.61px;top:142.88px" class="cls_026"><span class="cls_026">if</span></div>
<div style="position:absolute;left:293.66px;top:142.88px" class="cls_026"><span class="cls_026">for</span></div>
<div style="position:absolute;left:365.61px;top:142.88px" class="cls_026"><span class="cls_026">lambda  continue</span></div>
<div style="position:absolute;left:149.66px;top:162.08px" class="cls_026"><span class="cls_026">True</span></div>
<div style="position:absolute;left:221.61px;top:162.08px" class="cls_026"><span class="cls_026">def</span></div>
<div style="position:absolute;left:293.66px;top:162.08px" class="cls_026"><span class="cls_026">from</span></div>
<div style="position:absolute;left:365.61px;top:162.08px" class="cls_026"><span class="cls_026">while</span></div>
<div style="position:absolute;left:437.66px;top:162.08px" class="cls_026"><span class="cls_026">nonlocal</span></div>
<div style="position:absolute;left:149.66px;top:181.28px" class="cls_026"><span class="cls_026">and</span></div>
<div style="position:absolute;left:221.61px;top:181.28px" class="cls_026"><span class="cls_026">del</span></div>
<div style="position:absolute;left:293.66px;top:181.28px" class="cls_026"><span class="cls_026">global</span></div>
<div style="position:absolute;left:365.61px;top:181.28px" class="cls_026"><span class="cls_026">not</span></div>
<div style="position:absolute;left:437.66px;top:181.28px" class="cls_026"><span class="cls_026">with</span></div>
<div style="position:absolute;left:149.66px;top:200.48px" class="cls_026"><span class="cls_026">as</span></div>
<div style="position:absolute;left:221.61px;top:200.48px" class="cls_026"><span class="cls_026">elif</span></div>
<div style="position:absolute;left:293.66px;top:200.48px" class="cls_026"><span class="cls_026">try</span></div>
<div style="position:absolute;left:365.61px;top:200.48px" class="cls_026"><span class="cls_026">or</span></div>
<div style="position:absolute;left:437.66px;top:200.48px" class="cls_026"><span class="cls_026">yield</span></div>
<div style="position:absolute;left:149.66px;top:219.68px" class="cls_026"><span class="cls_026">assert</span></div>
<div style="position:absolute;left:221.61px;top:219.68px" class="cls_026"><span class="cls_026">else</span></div>
<div style="position:absolute;left:293.66px;top:219.68px" class="cls_026"><span class="cls_026">import</span></div>
<div style="position:absolute;left:365.61px;top:219.68px" class="cls_026"><span class="cls_026">pass</span></div>
<div style="position:absolute;left:149.66px;top:238.88px" class="cls_026"><span class="cls_026">break</span></div>
<div style="position:absolute;left:221.61px;top:238.88px" class="cls_026"><span class="cls_026">except</span></div>
<div style="position:absolute;left:293.66px;top:238.88px" class="cls_026"><span class="cls_026">in</span></div>
<div style="position:absolute;left:365.61px;top:238.88px" class="cls_026"><span class="cls_026">raise</span></div>
<div style="position:absolute;left:82.66px;top:295.12px" class="cls_017"><span class="cls_017">§ Cannot use reserved words as</span></div>
<div style="position:absolute;left:109.66px;top:341.20px" class="cls_017"><span class="cls_017">variable names or identifiers</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:194.28px;top:16.56px" class="cls_008"><span class="cls_008">Sentences or Lines</span></div>
<div style="position:absolute;left:100.73px;top:176.00px" class="cls_027"><span class="cls_027">x </span><span class="cls_026">=</span><span class="cls_024"> 2</span></div>
<div style="position:absolute;left:287.50px;top:175.76px" class="cls_005"><span class="cls_005">Assignment Statement</span></div>
<div style="position:absolute;left:100.73px;top:194.96px" class="cls_027"><span class="cls_027">x </span><span class="cls_026">= </span><span class="cls_027">x </span><span class="cls_026">+</span></div>
<div style="position:absolute;left:287.50px;top:195.92px" class="cls_005"><span class="cls_005">Assignment with expression</span></div>
<div style="position:absolute;left:100.73px;top:214.88px" class="cls_028"><span class="cls_028">print(</span><span class="cls_027">x</span></div>
<div style="position:absolute;left:287.50px;top:216.80px" class="cls_005"><span class="cls_005">Print Statement</span></div>
<div style="position:absolute;left:84.46px;top:287.84px" class="cls_029"><span class="cls_029">Variable</span></div>
<div style="position:absolute;left:173.98px;top:288.80px" class="cls_030"><span class="cls_030">Operator</span></div>
<div style="position:absolute;left:264.50px;top:288.80px" class="cls_031"><span class="cls_031">Constant</span></div>
<div style="position:absolute;left:365.90px;top:287.36px" class="cls_032"><span class="cls_032">Function</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:183.24px;top:16.56px" class="cls_008"><span class="cls_008">Interactive vs. Script</span></div>
<div style="position:absolute;left:101.35px;top:107.52px" class="cls_033"><span class="cls_033">§ Interactive</span></div>
<div style="position:absolute;left:137.35px;top:151.36px" class="cls_034"><span class="cls_034">§</span><span class="cls_035">  You type directly to Python one line at</span></div>
<div style="position:absolute;left:159.90px;top:185.44px" class="cls_035"><span class="cls_035">a time and it responds</span></div>
<div style="position:absolute;left:101.35px;top:229.44px" class="cls_033"><span class="cls_033">§ Script</span></div>
<div style="position:absolute;left:137.35px;top:272.56px" class="cls_034"><span class="cls_034">§</span><span class="cls_035">  You enter a sequence of statements</span></div>
<div style="position:absolute;left:159.90px;top:307.36px" class="cls_035"><span class="cls_035">(lines) into a file using a text editor</span></div>
<div style="position:absolute;left:159.90px;top:341.44px" class="cls_035"><span class="cls_035">and tell Python to execute the</span></div>
<div style="position:absolute;left:159.90px;top:375.52px" class="cls_035"><span class="cls_035">statements in the file</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:232.20px;top:16.56px" class="cls_008"><span class="cls_008">Python Scripts</span></div>
<div style="position:absolute;left:107.95px;top:114.92px" class="cls_036"><span class="cls_036">§ Interactive Python is good for experiments</span></div>
<div style="position:absolute;left:134.95px;top:144.92px" class="cls_036"><span class="cls_036">and programs of 3-4 lines long.</span></div>
<div style="position:absolute;left:107.95px;top:180.92px" class="cls_036"><span class="cls_036">§ Most programs are much longer, so we</span></div>
<div style="position:absolute;left:134.95px;top:210.92px" class="cls_036"><span class="cls_036">type them into a file and tell Python to run</span></div>
<div style="position:absolute;left:134.95px;top:240.92px" class="cls_036"><span class="cls_036">the commands in the file.</span></div>
<div style="position:absolute;left:107.95px;top:276.92px" class="cls_036"><span class="cls_036">§ In a sense, we are “giving Python a script”.</span></div>
<div style="position:absolute;left:107.95px;top:312.92px" class="cls_036"><span class="cls_036">§ As a convention, we add “.py” as the suffix</span></div>
<div style="position:absolute;left:134.95px;top:342.92px" class="cls_036"><span class="cls_036">on the end of these files to indicate they</span></div>
<div style="position:absolute;left:134.95px;top:372.92px" class="cls_036"><span class="cls_036">contain Python code.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:239.28px;top:16.56px" class="cls_008"><span class="cls_008">Program Flow</span></div>
<div style="position:absolute;left:112.73px;top:104.00px" class="cls_016"><span class="cls_016">§ Like a recipe or installation instructions, a</span></div>
<div style="position:absolute;left:139.73px;top:131.12px" class="cls_016"><span class="cls_016">program is a </span><span class="cls_037">sequence</span><span class="cls_016"> of steps to be done in</span></div>
<div style="position:absolute;left:139.73px;top:157.04px" class="cls_016"><span class="cls_016">order.</span></div>
<div style="position:absolute;left:112.73px;top:189.20px" class="cls_016"><span class="cls_016">§ Some steps are </span><span class="cls_037">conditional</span><span class="cls_016"> - they may be</span></div>
<div style="position:absolute;left:139.73px;top:215.12px" class="cls_016"><span class="cls_016">skipped.</span></div>
<div style="position:absolute;left:112.73px;top:246.08px" class="cls_016"><span class="cls_016">§ Sometimes a step or group of steps is to be</span></div>
<div style="position:absolute;left:139.68px;top:273.20px" class="cls_037"><span class="cls_037">repeated</span><span class="cls_016">.</span></div>
<div style="position:absolute;left:112.73px;top:304.16px" class="cls_016"><span class="cls_016">§ Sometimes we store a set of steps to be used</span></div>
<div style="position:absolute;left:139.73px;top:331.04px" class="cls_016"><span class="cls_016">over and over as needed several places</span></div>
<div style="position:absolute;left:139.73px;top:357.20px" class="cls_016"><span class="cls_016">throughout the program called a</span><span class="cls_037"> function</span><span class="cls_016">.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:214.20px;top:16.56px" class="cls_008"><span class="cls_008">Sequential Steps</span></div>
<div style="position:absolute;left:129.46px;top:115.52px" class="cls_038"><span class="cls_038">x = 2</span></div>
<div style="position:absolute;left:306.12px;top:154.88px" class="cls_005"><span class="cls_005">Program:</span></div>
<div style="position:absolute;left:453.60px;top:155.12px" class="cls_005"><span class="cls_005">Output:</span></div>
<div style="position:absolute;left:122.38px;top:184.40px" class="cls_038"><span class="cls_038">print(x)</span></div>
<div style="position:absolute;left:306.12px;top:191.12px" class="cls_022"><span class="cls_022">x = 2</span></div>
<div style="position:absolute;left:306.12px;top:210.08px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">x</span><span class="cls_028">)</span></div>
<div style="position:absolute;left:306.12px;top:230.00px" class="cls_022"><span class="cls_022">x = x +</span></div>
<div style="position:absolute;left:116.26px;top:247.28px" class="cls_038"><span class="cls_038">x = x + 2</span></div>
<div style="position:absolute;left:306.12px;top:248.96px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">x</span><span class="cls_028">)</span></div>
<div style="position:absolute;left:120.36px;top:308.96px" class="cls_038"><span class="cls_038">print(x)</span></div>
<div style="position:absolute;left:107.95px;top:390.16px" class="cls_012"><span class="cls_012">§  When a program is running, it flows from one step to</span></div>
<div style="position:absolute;left:134.95px;top:414.16px" class="cls_012"><span class="cls_012">the next. As programmers, we set up “paths” for the</span></div>
<div style="position:absolute;left:134.95px;top:438.16px" class="cls_012"><span class="cls_012">program to follow.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.28px;top:16.56px" class="cls_008"><span class="cls_008">Conditional Steps</span></div>
<div style="position:absolute;left:93.58px;top:46.40px" class="cls_005"><span class="cls_005">x = 5</span></div>
<div style="position:absolute;left:79.82px;top:131.36px" class="cls_005"><span class="cls_005">x &lt; 10 ?</span></div>
<div style="position:absolute;left:171.89px;top:148.64px" class="cls_005"><span class="cls_005">Yes</span></div>
<div style="position:absolute;left:371.98px;top:153.92px" class="cls_047"><span class="cls_047">Program</span></div>
<div style="position:absolute;left:371.98px;top:171.92px" class="cls_022"><span class="cls_022">x = 5</span></div>
<div style="position:absolute;left:76.49px;top:176.24px" class="cls_005"><span class="cls_005">No</span></div>
<div style="position:absolute;left:371.93px;top:190.88px" class="cls_028"><span class="cls_028">if </span><span class="cls_022">x &lt; 10:</span></div>
<div style="position:absolute;left:189.63px;top:204.56px" class="cls_005"><span class="cls_005">print('Smaller')</span></div>
<div style="position:absolute;left:400.73px;top:210.08px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">'Smaller’</span><span class="cls_028">)</span></div>
<div style="position:absolute;left:371.98px;top:230.00px" class="cls_028"><span class="cls_028">if </span><span class="cls_022">x > 20:</span></div>
<div style="position:absolute;left:400.78px;top:248.96px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">'Bigger’</span><span class="cls_028">)</span></div>
<div style="position:absolute;left:371.98px;top:267.92px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">'Finished'</span><span class="cls_028">)</span></div>
<div style="position:absolute;left:173.74px;top:285.92px" class="cls_005"><span class="cls_005">Yes</span></div>
<div style="position:absolute;left:79.99px;top:302.72px" class="cls_005"><span class="cls_005">x > 20 ?</span></div>
<div style="position:absolute;left:78.38px;top:349.04px" class="cls_005"><span class="cls_005">No</span></div>
<div style="position:absolute;left:197.04px;top:351.92px" class="cls_005"><span class="cls_005">print('Bigger')</span></div>
<div style="position:absolute;left:54.23px;top:432.08px" class="cls_005"><span class="cls_005">print(’Finished')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:224.28px;top:16.56px" class="cls_008"><span class="cls_008">Repeated Steps</span></div>
<div style="position:absolute;left:320.06px;top:97.52px" class="cls_005"><span class="cls_005">Program:</span></div>
<div style="position:absolute;left:151.47px;top:101.12px" class="cls_005"><span class="cls_005">n = 5</span></div>
<div style="position:absolute;left:570.31px;top:104.00px" class="cls_005"><span class="cls_005">Output:</span></div>
<div style="position:absolute;left:320.06px;top:133.76px" class="cls_022"><span class="cls_022">n = 5</span></div>
<div style="position:absolute;left:320.06px;top:152.96px" class="cls_028"><span class="cls_028">while </span><span class="cls_022">n > 0</span></div>
<div style="position:absolute;left:358.46px;top:172.88px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">n</span><span class="cls_028">)</span></div>
<div style="position:absolute;left:81.48px;top:177.44px" class="cls_005"><span class="cls_005">No</span></div>
<div style="position:absolute;left:251.47px;top:175.76px" class="cls_005"><span class="cls_005">Yes</span></div>
<div style="position:absolute;left:141.72px;top:192.08px" class="cls_005"><span class="cls_005">n > 0 ?</span></div>
<div style="position:absolute;left:358.46px;top:191.84px" class="cls_022"><span class="cls_022">n = n - 1</span></div>
<div style="position:absolute;left:570.31px;top:199.52px" class="cls_005"><span class="cls_005">2</span></div>
<div style="position:absolute;left:320.06px;top:210.80px" class="cls_028"><span class="cls_028">print(</span><span class="cls_022">'Blastoff!'</span><span class="cls_039">)</span></div>
<div style="position:absolute;left:570.31px;top:218.48px" class="cls_005"><span class="cls_005">1</span></div>
<div style="position:absolute;left:570.31px;top:238.40px" class="cls_005"><span class="cls_005">Blastoff!</span></div>
<div style="position:absolute;left:268.62px;top:270.32px" class="cls_005"><span class="cls_005">print(n)</span></div>
<div style="position:absolute;left:268.77px;top:344.72px" class="cls_005"><span class="cls_005">n = n -1</span></div>
<div style="position:absolute;left:66.94px;top:385.76px" class="cls_005"><span class="cls_005">print('Blastoff')</span></div>
<div style="position:absolute;left:276.67px;top:395.20px" class="cls_012"><span class="cls_012">§  Loops (repeated steps) have </span><span class="cls_040">iteration</span></div>
<div style="position:absolute;left:303.67px;top:419.20px" class="cls_040"><span class="cls_040">variables</span><span class="cls_012"> that change each time through</span></div>
<div style="position:absolute;left:303.67px;top:444.16px" class="cls_012"><span class="cls_012">a loop.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:51.31px;top:22.56px" class="cls_008"><span class="cls_008">Sequential, Conditional & Repeated</span></div>
<div style="position:absolute;left:30.84px;top:101.84px" class="cls_028"><span class="cls_028">name = input('Enter file:')</span></div>
<div style="position:absolute;left:515.38px;top:105.68px" class="cls_044"><span class="cls_044">Sequential</span></div>
<div style="position:absolute;left:30.84px;top:121.04px" class="cls_028"><span class="cls_028">handle = open(name, 'r')</span></div>
<div style="position:absolute;left:515.38px;top:133.52px" class="cls_041"><span class="cls_041">Repeated</span></div>
<div style="position:absolute;left:30.84px;top:159.68px" class="cls_028"><span class="cls_028">counts = dict()</span></div>
<div style="position:absolute;left:515.38px;top:162.56px" class="cls_045"><span class="cls_045">Conditional</span></div>
<div style="position:absolute;left:30.84px;top:178.64px" class="cls_042"><span class="cls_042">for line in handle:</span></div>
<div style="position:absolute;left:69.24px;top:198.56px" class="cls_042"><span class="cls_042">words = line.split(</span></div>
<div style="position:absolute;left:69.24px;top:217.52px" class="cls_042"><span class="cls_042">for word in words:</span></div>
<div style="position:absolute;left:107.74px;top:236.48px" class="cls_042"><span class="cls_042">counts[word] = counts.get(word,0</span></div>
<div style="position:absolute;left:30.84px;top:274.88px" class="cls_028"><span class="cls_028">bigcount = None</span></div>
<div style="position:absolute;left:30.84px;top:293.84px" class="cls_028"><span class="cls_028">bigword = None</span></div>
<div style="position:absolute;left:30.84px;top:313.76px" class="cls_042"><span class="cls_042">for word,count in counts.items():</span></div>
<div style="position:absolute;left:69.19px;top:332.72px" class="cls_043"><span class="cls_043">if bigcount is None or count > bigco</span></div>
<div style="position:absolute;left:107.74px;top:351.68px" class="cls_043"><span class="cls_043">bigword = word</span></div>
<div style="position:absolute;left:108.61px;top:370.88px" class="cls_043"><span class="cls_043">bigcount = count</span></div>
<div style="position:absolute;left:30.84px;top:409.76px" class="cls_028"><span class="cls_028">print(bigword, bigcount)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.20px;top:16.56px" class="cls_008"><span class="cls_008">Summary</span></div>
<div style="position:absolute;left:107.95px;top:101.20px" class="cls_017"><span class="cls_017">§ History of Python</span></div>
<div style="position:absolute;left:107.95px;top:155.20px" class="cls_017"><span class="cls_017">§ Importance of Python</span></div>
<div style="position:absolute;left:107.95px;top:208.24px" class="cls_017"><span class="cls_017">§ HardwareArchitecture</span></div>
<div style="position:absolute;left:107.95px;top:261.04px" class="cls_017"><span class="cls_017">§ Python Interpreter</span></div>
<div style="position:absolute;left:107.95px;top:315.04px" class="cls_017"><span class="cls_017">§ Program Examples</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:147.82px;top:16.56px" class="cls_008"><span class="cls_008">To Do List for Next Week</span></div>
<div style="position:absolute;left:102.19px;top:94.00px" class="cls_017"><span class="cls_017">§ Read Chapters 1 & 2</span></div>
<div style="position:absolute;left:102.19px;top:143.92px" class="cls_017"><span class="cls_017">§ Bring a laptop with Jupyter</span></div>
<div style="position:absolute;left:129.19px;top:190.00px" class="cls_017"><span class="cls_017">Notebook already installed</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.21px;top:16.56px" class="cls_008"><span class="cls_008">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.31px;top:59.76px" class="cls_008"><span class="cls_008">tions</span></div>
<div style="position:absolute;left:107.95px;top:127.44px" class="cls_034"><span class="cls_034">§  These slides are Copyright 2010-  Charles R. Severance</span></div>
<div style="position:absolute;left:134.95px;top:148.56px" class="cls_034"><span class="cls_034">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.95px;top:170.40px" class="cls_034"><span class="cls_034">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.95px;top:191.52px" class="cls_034"><span class="cls_034">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.95px;top:213.36px" class="cls_034"><span class="cls_034">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.95px;top:235.44px" class="cls_034"><span class="cls_034">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.95px;top:256.56px" class="cls_034"><span class="cls_034">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.95px;top:278.40px" class="cls_034"><span class="cls_034">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.95px;top:299.52px" class="cls_034"><span class="cls_034">materials.</span></div>
<div style="position:absolute;left:107.95px;top:352.56px" class="cls_034"><span class="cls_034">§  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.90px;top:374.40px" class="cls_034"><span class="cls_034">Michigan School of Information</span></div>
<div style="position:absolute;left:107.95px;top:427.44px" class="cls_034"><span class="cls_034">§  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.90px;top:448.56px" class="cls_034"><span class="cls_034">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1Introduction/background28.jpg" width=720 height=540></div>
</div>

</body>
</html>
