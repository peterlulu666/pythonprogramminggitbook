<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:20.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:20.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_032{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_036{font-family:Courier New,serif;font-size:18.1px;color:rgb(227,107,8);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Courier New,serif;font-size:18.1px;color:rgb(227,107,8);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_037{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_037{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_038{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_038{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_039{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_039{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_040{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_040{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_043{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture8Iteration/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.57px;top:185.68px" class="cls_002"><span class="cls_002">Chapter 5:</span></div>
<div style="position:absolute;left:44.57px;top:222.64px" class="cls_002"><span class="cls_002">Iteration</span></div>
<div style="position:absolute;left:44.93px;top:353.52px" class="cls_003"><span class="cls_003">INSY 5336: Python Programming</span></div>
<div style="position:absolute;left:44.57px;top:407.28px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:44.57px;top:434.32px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:174.12px;top:222.40px" class="cls_007"><span class="cls_007">Chapter 5: Iteration</span></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:200.64px;top:16.56px" class="cls_009"><span class="cls_009">Types of Iterations</span></div>
<div style="position:absolute;left:31.99px;top:102.96px" class="cls_008"><span class="cls_008">Two kinds of iterative loops in Python:</span></div>
<div style="position:absolute;left:31.99px;top:183.84px" class="cls_008"><span class="cls_008">• Indefinite loops</span></div>
<div style="position:absolute;left:67.99px;top:222.88px" class="cls_010"><span class="cls_010">- Loop is executed as long as some condition is met</span></div>
<div style="position:absolute;left:67.99px;top:256.96px" class="cls_010"><span class="cls_010">- E.g. execute the loop as long as n>0</span></div>
<div style="position:absolute;left:31.99px;top:291.84px" class="cls_008"><span class="cls_008">• Definite loops</span></div>
<div style="position:absolute;left:67.99px;top:330.88px" class="cls_010"><span class="cls_010">- Loop is executed for a finite number of times</span></div>
<div style="position:absolute;left:67.99px;top:364.96px" class="cls_010"><span class="cls_010">- E.g. execute the loop 10 times</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.32px;top:16.56px" class="cls_009"><span class="cls_009">Indefinite loops</span></div>
<div style="position:absolute;left:31.99px;top:106.48px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> While </span><span class="cls_011">statements implement indefinite</span></div>
<div style="position:absolute;left:58.99px;top:145.60px" class="cls_011"><span class="cls_011">loops in python</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:191.38px;top:11.52px" class="cls_009"><span class="cls_009">Indefinite loops - While</span></div>
<div style="position:absolute;left:74.13px;top:46.80px" class="cls_013"><span class="cls_013">n = 5</span></div>
<div style="position:absolute;left:307.20px;top:54.72px" class="cls_009"><span class="cls_009">statement</span></div>
<div style="position:absolute;left:392.32px;top:100.00px" class="cls_014"><span class="cls_014">Program:</span></div>
<div style="position:absolute;left:12.44px;top:144.00px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:151.64px;top:144.00px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:392.32px;top:146.88px" class="cls_017"><span class="cls_017">n </span><span class="cls_018">=</span><span class="cls_019"> 5</span></div>
<div style="position:absolute;left:64.75px;top:161.76px" class="cls_013"><span class="cls_013">n > 0 ?</span></div>
<div style="position:absolute;left:392.32px;top:168.96px" class="cls_015"><span class="cls_015">while </span><span class="cls_017">n </span><span class="cls_018">> </span><span class="cls_019">0</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:435.42px;top:191.04px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">n</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:435.42px;top:211.92px" class="cls_017"><span class="cls_017">n </span><span class="cls_018">= </span><span class="cls_017">n </span><span class="cls_018">-</span><span class="cls_015"> 1</span></div>
<div style="position:absolute;left:151.46px;top:228.00px" class="cls_013"><span class="cls_013">print(n)</span></div>
<div style="position:absolute;left:392.32px;top:234.00px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_019">'Blastoff!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:392.32px;top:254.88px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">n</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:151.71px;top:292.80px" class="cls_013"><span class="cls_013">n = n -1</span></div>
<div style="position:absolute;left:234.29px;top:284.32px" class="cls_011"><span class="cls_011">• Loops are repeated steps</span></div>
<div style="position:absolute;left:234.29px;top:327.28px" class="cls_011"><span class="cls_011">• Iteration variables change each</span></div>
<div style="position:absolute;left:261.29px;top:362.32px" class="cls_011"><span class="cls_011">time through a loop</span></div>
<div style="position:absolute;left:76.22px;top:396.00px" class="cls_013"><span class="cls_013">print('Blastoff')</span></div>
<div style="position:absolute;left:234.29px;top:405.28px" class="cls_011"><span class="cls_011">• Iteration variables could be a</span></div>
<div style="position:absolute;left:261.29px;top:440.32px" class="cls_011"><span class="cls_011">sequence of numbers</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.28px;top:11.52px" class="cls_009"><span class="cls_009">Infinite Loops</span></div>
<div style="position:absolute;left:74.13px;top:46.80px" class="cls_013"><span class="cls_013">n = 5</span></div>
<div style="position:absolute;left:392.32px;top:100.00px" class="cls_014"><span class="cls_014">Program:</span></div>
<div style="position:absolute;left:12.44px;top:144.00px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:151.64px;top:144.00px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:392.32px;top:146.88px" class="cls_017"><span class="cls_017">n </span><span class="cls_018">=</span><span class="cls_019"> 5</span></div>
<div style="position:absolute;left:64.75px;top:161.76px" class="cls_013"><span class="cls_013">n > 0 ?</span></div>
<div style="position:absolute;left:392.32px;top:168.96px" class="cls_015"><span class="cls_015">while </span><span class="cls_017">n </span><span class="cls_018">> </span><span class="cls_019">0</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:435.42px;top:191.04px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">‘Hello’</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:435.42px;top:211.92px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">‘World’</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:136.46px;top:228.00px" class="cls_013"><span class="cls_013">print(‘Hello’)</span></div>
<div style="position:absolute;left:392.32px;top:234.00px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_019">'Done!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:392.32px;top:254.88px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">n</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:128.27px;top:292.80px" class="cls_013"><span class="cls_013">Print(‘World’)</span></div>
<div style="position:absolute;left:234.29px;top:288.16px" class="cls_011"><span class="cls_011">• What is wrong with this loop?</span></div>
<div style="position:absolute;left:234.29px;top:334.24px" class="cls_011"><span class="cls_011">• Every loop has to have an</span></div>
<div style="position:absolute;left:261.29px;top:373.12px" class="cls_011"><span class="cls_011">“escape hatch”</span></div>
<div style="position:absolute;left:84.62px;top:396.00px" class="cls_013"><span class="cls_013">print('Done')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:298.32px;top:11.52px" class="cls_009"><span class="cls_009">Dead Code</span></div>
<div style="position:absolute;left:74.13px;top:46.80px" class="cls_013"><span class="cls_013">n = 0</span></div>
<div style="position:absolute;left:392.32px;top:100.00px" class="cls_014"><span class="cls_014">Program:</span></div>
<div style="position:absolute;left:12.44px;top:144.00px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:151.64px;top:144.00px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:392.32px;top:146.88px" class="cls_017"><span class="cls_017">n </span><span class="cls_018">=</span><span class="cls_019"> 0</span></div>
<div style="position:absolute;left:64.75px;top:161.76px" class="cls_013"><span class="cls_013">n > 0 ?</span></div>
<div style="position:absolute;left:392.32px;top:168.96px" class="cls_015"><span class="cls_015">while </span><span class="cls_017">n </span><span class="cls_018">> </span><span class="cls_019">0</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:435.42px;top:191.04px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">‘Hello’</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:435.42px;top:211.92px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">‘World’</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:136.46px;top:228.00px" class="cls_013"><span class="cls_013">print(‘Hello’)</span></div>
<div style="position:absolute;left:392.32px;top:234.00px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_019">'Done!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:392.32px;top:254.88px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">n</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:128.27px;top:292.80px" class="cls_013"><span class="cls_013">Print(‘World’)</span></div>
<div style="position:absolute;left:244.66px;top:288.40px" class="cls_011"><span class="cls_011">• What is wrong with this loop?</span></div>
<div style="position:absolute;left:244.66px;top:331.36px" class="cls_011"><span class="cls_011">• Dead code is not desirable:</span></div>
<div style="position:absolute;left:280.66px;top:373.28px" class="cls_020"><span class="cls_020">- Uses memory</span></div>
<div style="position:absolute;left:84.62px;top:396.00px" class="cls_013"><span class="cls_013">print('Done')</span></div>
<div style="position:absolute;left:280.66px;top:410.24px" class="cls_020"><span class="cls_020">- Document & maintenance costs</span></div>
<div style="position:absolute;left:303.21px;top:440.24px" class="cls_020"><span class="cls_020">are wasted</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.28px;top:11.52px" class="cls_009"><span class="cls_009">Infinite Loops</span></div>
<div style="position:absolute;left:74.13px;top:46.80px" class="cls_013"><span class="cls_013">n = 5</span></div>
<div style="position:absolute;left:392.32px;top:100.00px" class="cls_014"><span class="cls_014">Program:</span></div>
<div style="position:absolute;left:12.44px;top:144.00px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:151.64px;top:144.00px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:392.32px;top:146.88px" class="cls_017"><span class="cls_017">n </span><span class="cls_018">=</span><span class="cls_019"> 5</span></div>
<div style="position:absolute;left:64.75px;top:161.76px" class="cls_013"><span class="cls_013">n > 0 ?</span></div>
<div style="position:absolute;left:392.32px;top:168.96px" class="cls_015"><span class="cls_015">while </span><span class="cls_017">n </span><span class="cls_018">> </span><span class="cls_019">0</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:435.42px;top:191.04px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">‘Hello’</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:435.42px;top:211.92px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">‘World’</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:136.46px;top:228.00px" class="cls_013"><span class="cls_013">print(‘Hello’)</span></div>
<div style="position:absolute;left:392.32px;top:234.00px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_019">'Done!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:392.32px;top:254.88px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">n</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:128.27px;top:292.80px" class="cls_013"><span class="cls_013">Print(‘World’)</span></div>
<div style="position:absolute;left:234.29px;top:288.16px" class="cls_011"><span class="cls_011">• What is wrong with this loop?</span></div>
<div style="position:absolute;left:234.29px;top:334.24px" class="cls_011"><span class="cls_011">• Every loop has to have an</span></div>
<div style="position:absolute;left:261.29px;top:373.12px" class="cls_011"><span class="cls_011">“escape hatch”</span></div>
<div style="position:absolute;left:84.62px;top:396.00px" class="cls_013"><span class="cls_013">print('Done')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:149.38px;top:11.52px" class="cls_009"><span class="cls_009">Breaking out of a While loop</span></div>
<div style="position:absolute;left:136.47px;top:79.68px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:64.20px;top:86.88px" class="cls_013"><span class="cls_013">Is it</span></div>
<div style="position:absolute;left:370.97px;top:103.68px" class="cls_015"><span class="cls_015">while</span><span class="cls_019"> True</span><span class="cls_015">:</span></div>
<div style="position:absolute;left:48.63px;top:107.76px" class="cls_013"><span class="cls_013">TRUE?</span></div>
<div style="position:absolute;left:414.02px;top:124.80px" class="cls_017"><span class="cls_017">line </span><span class="cls_018">= </span><span class="cls_019">input(</span><span class="cls_016">'> '</span><span class="cls_019">)</span></div>
<div style="position:absolute;left:414.02px;top:146.88px" class="cls_015"><span class="cls_015">if </span><span class="cls_017">line </span><span class="cls_018">== </span><span class="cls_016">'done'</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:120.56px;top:167.52px" class="cls_013"><span class="cls_013">Line=input()</span></div>
<div style="position:absolute;left:456.97px;top:168.72px" class="cls_015"><span class="cls_015">break</span></div>
<div style="position:absolute;left:414.02px;top:189.84px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">line</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:37.56px;top:196.56px" class="cls_013"><span class="cls_013">print(line)</span></div>
<div style="position:absolute;left:370.97px;top:211.68px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">('Done!')</span></div>
<div style="position:absolute;left:87.51px;top:240.48px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:140.88px;top:247.92px" class="cls_013"><span class="cls_013">line==‘</span></div>
<div style="position:absolute;left:145.43px;top:268.80px" class="cls_013"><span class="cls_013">done’</span></div>
<div style="position:absolute;left:246.67px;top:287.44px" class="cls_011"><span class="cls_011">• There is no false condition for</span></div>
<div style="position:absolute;left:124.23px;top:308.40px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:273.67px;top:326.56px" class="cls_011"><span class="cls_011">the while loop</span></div>
<div style="position:absolute;left:146.50px;top:358.32px" class="cls_013"><span class="cls_013">break</span></div>
<div style="position:absolute;left:246.67px;top:372.64px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> break </span><span class="cls_011">provides the escape</span></div>
<div style="position:absolute;left:118.89px;top:422.64px" class="cls_013"><span class="cls_013">Print(‘Done’)</span></div>
<div style="position:absolute;left:273.67px;top:410.56px" class="cls_011"><span class="cls_011">hatch</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:182.38px;top:11.52px" class="cls_009"><span class="cls_009">Continue in a While loop</span></div>
<div style="position:absolute;left:136.47px;top:79.68px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:64.20px;top:86.88px" class="cls_013"><span class="cls_013">Is it</span></div>
<div style="position:absolute;left:392.33px;top:92.16px" class="cls_015"><span class="cls_015">while</span><span class="cls_019"> True</span><span class="cls_015">:</span></div>
<div style="position:absolute;left:48.63px;top:107.76px" class="cls_013"><span class="cls_013">TRUE?</span></div>
<div style="position:absolute;left:435.43px;top:113.28px" class="cls_017"><span class="cls_017">line </span><span class="cls_018">= </span><span class="cls_019">input(</span><span class="cls_016">'> '</span><span class="cls_019">)</span></div>
<div style="position:absolute;left:435.38px;top:135.12px" class="cls_015"><span class="cls_015">if </span><span class="cls_017">line[0] </span><span class="cls_018">== </span><span class="cls_016">'#'</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:478.38px;top:157.20px" class="cls_015"><span class="cls_015">continue</span></div>
<div style="position:absolute;left:123.58px;top:170.64px" class="cls_013"><span class="cls_013">Line=input()</span></div>
<div style="position:absolute;left:435.38px;top:178.32px" class="cls_015"><span class="cls_015">if </span><span class="cls_017">line </span><span class="cls_018">== </span><span class="cls_016">'done'</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:478.38px;top:200.16px" class="cls_015"><span class="cls_015">break</span></div>
<div style="position:absolute;left:435.43px;top:221.28px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">line</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:87.51px;top:240.48px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:392.33px;top:243.12px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">('Done!')</span></div>
<div style="position:absolute;left:143.40px;top:247.92px" class="cls_013"><span class="cls_013">line[0]</span></div>
<div style="position:absolute;left:147.80px;top:268.80px" class="cls_013"><span class="cls_013">==‘#’</span></div>
<div style="position:absolute;left:246.67px;top:287.44px" class="cls_011"><span class="cls_011">• The</span><span class="cls_012"> continue</span><span class="cls_011"> statement</span></div>
<div style="position:absolute;left:24.41px;top:306.48px" class="cls_013"><span class="cls_013">print(line)</span></div>
<div style="position:absolute;left:124.23px;top:308.40px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:273.67px;top:326.56px" class="cls_011"><span class="cls_011">sends control back to the</span></div>
<div style="position:absolute;left:82.18px;top:361.20px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:144.10px;top:368.64px" class="cls_013"><span class="cls_013">line==</span></div>
<div style="position:absolute;left:273.67px;top:364.48px" class="cls_011"><span class="cls_011">while test</span></div>
<div style="position:absolute;left:148.65px;top:389.52px" class="cls_013"><span class="cls_013">done</span></div>
<div style="position:absolute;left:186.99px;top:429.12px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:151.08px;top:461.76px" class="cls_013"><span class="cls_013">break</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:240.36px;top:16.56px" class="cls_009"><span class="cls_009">Definite loops</span></div>
<div style="position:absolute;left:31.99px;top:106.48px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> for </span><span class="cls_011">statements implement definite loops in</span></div>
<div style="position:absolute;left:58.99px;top:145.60px" class="cls_011"><span class="cls_011">python</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:325.44px;top:11.52px" class="cls_009"><span class="cls_009">for loop</span></div>
<div style="position:absolute;left:101.29px;top:62.16px" class="cls_013"><span class="cls_013">Is an</span></div>
<div style="position:absolute;left:205.23px;top:65.04px" class="cls_013"><span class="cls_013">Yes</span></div>
<div style="position:absolute;left:17.14px;top:76.80px" class="cls_013"><span class="cls_013">No</span></div>
<div style="position:absolute;left:88.94px;top:83.28px" class="cls_013"><span class="cls_013">element</span></div>
<div style="position:absolute;left:103.84px;top:105.12px" class="cls_013"><span class="cls_013">left?</span></div>
<div style="position:absolute;left:360.72px;top:133.68px" class="cls_015"><span class="cls_015">for </span><span class="cls_017">i </span><span class="cls_015">in </span><span class="cls_021">[5, 4, 3, 2, 1]</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:403.82px;top:154.80px" class="cls_015"><span class="cls_015">print(</span><span class="cls_017">i</span><span class="cls_015">)</span></div>
<div style="position:absolute;left:181.77px;top:160.32px" class="cls_013"><span class="cls_013">Move i by</span></div>
<div style="position:absolute;left:360.72px;top:176.64px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Blastoff!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:185.62px;top:182.40px" class="cls_013"><span class="cls_013">one step</span></div>
<div style="position:absolute;left:194.67px;top:239.76px" class="cls_013"><span class="cls_013">print(i)</span></div>
<div style="position:absolute;left:246.67px;top:285.04px" class="cls_011"><span class="cls_011">• The loop is executed for each</span></div>
<div style="position:absolute;left:25.07px;top:332.16px" class="cls_013"><span class="cls_013">Print(‘blastoff’)</span></div>
<div style="position:absolute;left:273.67px;top:320.08px" class="cls_011"><span class="cls_011">item in the list</span></div>
<div style="position:absolute;left:246.67px;top:361.12px" class="cls_011"><span class="cls_011">• i is called the iteration variable</span></div>
<div style="position:absolute;left:246.67px;top:404.08px" class="cls_011"><span class="cls_011">• The value of variable i takes</span></div>
<div style="position:absolute;left:273.67px;top:439.12px" class="cls_011"><span class="cls_011">one element at a time</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.38px;top:11.52px" class="cls_009"><span class="cls_009">Anatomy of a for loop</span></div>
<div style="position:absolute;left:347.93px;top:73.84px" class="cls_023"><span class="cls_023">Five-element sequence</span></div>
<div style="position:absolute;left:117.31px;top:85.12px" class="cls_022"><span class="cls_022">Iteration variable</span></div>
<div style="position:absolute;left:215.14px;top:148.56px" class="cls_015"><span class="cls_015">for </span><span class="cls_017">i </span><span class="cls_015">in </span><span class="cls_021">[5, 4, 3, 2, 1]</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:258.24px;top:169.68px" class="cls_015"><span class="cls_015">print(</span><span class="cls_017">i</span><span class="cls_015">)</span></div>
<div style="position:absolute;left:215.14px;top:191.52px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Blastoff!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:54.55px;top:280.32px" class="cls_008"><span class="cls_008">• The iteration variable “iterates” through the</span></div>
<div style="position:absolute;left:81.55px;top:309.36px" class="cls_008"><span class="cls_008">sequence (ordered set)</span></div>
<div style="position:absolute;left:54.55px;top:345.36px" class="cls_008"><span class="cls_008">• The block (body) of code is executed once for</span></div>
<div style="position:absolute;left:81.55px;top:374.40px" class="cls_008"><span class="cls_008">each value in the sequence</span></div>
<div style="position:absolute;left:54.55px;top:410.40px" class="cls_008"><span class="cls_008">• The iteration variable moves through all of the</span></div>
<div style="position:absolute;left:81.55px;top:439.44px" class="cls_008"><span class="cls_008">values in the sequence</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:194.28px;top:16.56px" class="cls_009"><span class="cls_009">break and continue</span></div>
<div style="position:absolute;left:35.90px;top:140.08px" class="cls_011"><span class="cls_011">• Can we use break and continue in for</span></div>
<div style="position:absolute;left:62.90px;top:178.96px" class="cls_011"><span class="cls_011">loops?</span></div>
<div style="position:absolute;left:56.04px;top:269.52px" class="cls_015"><span class="cls_015">for </span><span class="cls_017">i </span><span class="cls_015">in </span><span class="cls_021">[5, 4, 3, 2, 1]</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:380.45px;top:270.48px" class="cls_015"><span class="cls_015">for </span><span class="cls_017">i </span><span class="cls_015">in </span><span class="cls_021">[5, 4, 3, 2, 1]</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:99.14px;top:290.40px" class="cls_015"><span class="cls_015">print(</span><span class="cls_017">i</span><span class="cls_015">)</span></div>
<div style="position:absolute;left:412.70px;top:291.60px" class="cls_015"><span class="cls_015">continue</span></div>
<div style="position:absolute;left:102.89px;top:311.52px" class="cls_015"><span class="cls_015">break</span></div>
<div style="position:absolute;left:416.45px;top:312.48px" class="cls_015"><span class="cls_015">print(</span><span class="cls_017">i</span><span class="cls_015">)</span></div>
<div style="position:absolute;left:56.04px;top:333.60px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Blastoff!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:380.45px;top:334.56px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Blastoff!'</span><span class="cls_016">)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:173.14px;top:16.56px" class="cls_009"><span class="cls_009">The range expression</span></div>
<div style="position:absolute;left:31.99px;top:100.52px" class="cls_024"><span class="cls_024">• It is often cumbersome and impractical to list out all the</span></div>
<div style="position:absolute;left:58.99px;top:124.52px" class="cls_024"><span class="cls_024">elements in a sequence to iterate through</span></div>
<div style="position:absolute;left:31.99px;top:154.52px" class="cls_024"><span class="cls_024">• We therefore use the </span><span class="cls_025">range</span><span class="cls_024"> expression</span></div>
<div style="position:absolute;left:31.99px;top:184.52px" class="cls_024"><span class="cls_024">•</span><span class="cls_025"> range(begin, end, step)</span></div>
<div style="position:absolute;left:67.99px;top:214.40px" class="cls_026"><span class="cls_026">-</span><span class="cls_027"> begin </span><span class="cls_026">is the first value in the range; default value is 0</span></div>
<div style="position:absolute;left:67.99px;top:241.52px" class="cls_026"><span class="cls_026">-</span><span class="cls_027"> end </span><span class="cls_026">is the one past the last value in the range; this value is</span></div>
<div style="position:absolute;left:90.54px;top:262.40px" class="cls_026"><span class="cls_026">mandatory (no default value)</span></div>
<div style="position:absolute;left:67.99px;top:286.40px" class="cls_026"><span class="cls_026">-</span><span class="cls_027"> step </span><span class="cls_026">is the amount to increment (if positive) or decrement (if</span></div>
<div style="position:absolute;left:90.54px;top:310.40px" class="cls_026"><span class="cls_026">negative); default value is 1</span></div>
<div style="position:absolute;left:31.99px;top:336.44px" class="cls_024"><span class="cls_024">• The arguments may be integer variables, expressions or</span></div>
<div style="position:absolute;left:58.99px;top:360.44px" class="cls_024"><span class="cls_024">literals</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:196.20px;top:16.56px" class="cls_009"><span class="cls_009">The range example</span></div>
<div style="position:absolute;left:211.20px;top:117.12px" class="cls_015"><span class="cls_015">for </span><span class="cls_017">i </span><span class="cls_015">in</span><span class="cls_021"> range(1,6)</span><span class="cls_015">:</span></div>
<div style="position:absolute;left:254.25px;top:138.00px" class="cls_015"><span class="cls_015">print(</span><span class="cls_017">i</span><span class="cls_015">)</span></div>
<div style="position:absolute;left:211.20px;top:160.08px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Blastoff!'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:43.51px;top:269.68px" class="cls_011"><span class="cls_011">• In the for loop, i takes values from 1 to (6-1)</span></div>
<div style="position:absolute;left:43.51px;top:316.48px" class="cls_011"><span class="cls_011">• The default increment is 1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:108.34px;top:16.56px" class="cls_009"><span class="cls_009">Infinite execution in for loops</span></div>
<div style="position:absolute;left:35.90px;top:103.12px" class="cls_011"><span class="cls_011">• Can we create an infinite loop using the</span><span class="cls_012"> for</span></div>
<div style="position:absolute;left:62.90px;top:142.00px" class="cls_011"><span class="cls_011">construct?</span></div>
<div style="position:absolute;left:56.04px;top:223.20px" class="cls_015"><span class="cls_015">for i in range(1,4):</span></div>
<div style="position:absolute;left:380.45px;top:224.40px" class="cls_015"><span class="cls_015">a = [1,2,3]</span></div>
<div style="position:absolute;left:99.19px;top:244.32px" class="cls_015"><span class="cls_015">print(i)</span></div>
<div style="position:absolute;left:99.19px;top:266.16px" class="cls_015"><span class="cls_015">i=i-1</span></div>
<div style="position:absolute;left:380.45px;top:267.36px" class="cls_015"><span class="cls_015">for i in a:</span></div>
<div style="position:absolute;left:99.19px;top:288.24px" class="cls_015"><span class="cls_015">print(i)</span></div>
<div style="position:absolute;left:423.50px;top:288.48px" class="cls_015"><span class="cls_015">a.append(i)</span></div>
<div style="position:absolute;left:56.09px;top:309.12px" class="cls_015"><span class="cls_015">print('Done')</span></div>
<div style="position:absolute;left:423.50px;top:310.56px" class="cls_015"><span class="cls_015">print(i)</span></div>
<div style="position:absolute;left:35.90px;top:368.56px" class="cls_011"><span class="cls_011">• Which code segment works to create an</span></div>
<div style="position:absolute;left:62.90px;top:407.68px" class="cls_011"><span class="cls_011">infinite loop and why?</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:143.38px;top:16.56px" class="cls_009"><span class="cls_009">How to construct a loop?</span></div>
<div style="position:absolute;left:35.90px;top:111.28px" class="cls_011"><span class="cls_011">• Most loops are constructed by the following</span></div>
<div style="position:absolute;left:62.90px;top:146.32px" class="cls_011"><span class="cls_011">structure:</span></div>
<div style="position:absolute;left:230.09px;top:194.88px" class="cls_028"><span class="cls_028">Set some variables to initial</span></div>
<div style="position:absolute;left:313.04px;top:216.00px" class="cls_028"><span class="cls_028">values</span></div>
<div style="position:absolute;left:236.14px;top:251.52px" class="cls_029"><span class="cls_029">for </span><span class="cls_030">thing </span><span class="cls_029">in data:</span></div>
<div style="position:absolute;left:262.13px;top:282.96px" class="cls_028"><span class="cls_028">Look for something or do</span></div>
<div style="position:absolute;left:266.65px;top:305.04px" class="cls_028"><span class="cls_028">something to each entry</span></div>
<div style="position:absolute;left:275.80px;top:325.92px" class="cls_028"><span class="cls_028">separately, updating a</span></div>
<div style="position:absolute;left:330.68px;top:348.00px" class="cls_028"><span class="cls_028">variable</span></div>
<div style="position:absolute;left:255.94px;top:386.64px" class="cls_028"><span class="cls_028">Look at the variables</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:169.18px;top:16.56px" class="cls_009"><span class="cls_009">Looping through a set</span></div>
<div style="position:absolute;left:157.74px;top:130.32px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.74px;top:151.20px" class="cls_015"><span class="cls_015">for </span><span class="cls_018">thing </span><span class="cls_015">in </span><span class="cls_018">[9, 41, 12, 3, 74, 15] </span><span class="cls_015">:</span></div>
<div style="position:absolute;left:211.54px;top:173.28px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_018">thing</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.74px;top:195.36px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:35.90px;top:283.36px" class="cls_011"><span class="cls_011">• The for construct loops through each</span></div>
<div style="position:absolute;left:62.90px;top:322.48px" class="cls_011"><span class="cls_011">element in the list, one at a time</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:227.28px;top:16.56px" class="cls_009"><span class="cls_009">Counting loops</span></div>
<div style="position:absolute;left:157.74px;top:111.84px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.74px;top:132.72px" class="cls_016"><span class="cls_016">count=0</span></div>
<div style="position:absolute;left:157.74px;top:154.80px" class="cls_015"><span class="cls_015">for </span><span class="cls_018">thing </span><span class="cls_015">in </span><span class="cls_018">[9, 41, 12, 3, 74, 15]</span><span class="cls_015"> :</span></div>
<div style="position:absolute;left:211.54px;top:176.88px" class="cls_031"><span class="cls_031">count=count+1</span></div>
<div style="position:absolute;left:215.34px;top:197.76px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_018">thing</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.74px;top:219.84px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.74px;top:240.72px" class="cls_016"><span class="cls_016">Print(‘count=‘,count)</span></div>
<div style="position:absolute;left:52.30px;top:306.88px" class="cls_011"><span class="cls_011">• count is a counter variable</span></div>
<div style="position:absolute;left:52.30px;top:353.92px" class="cls_011"><span class="cls_011">• Counter variable starts at 0 and increments</span></div>
<div style="position:absolute;left:79.30px;top:391.84px" class="cls_011"><span class="cls_011">every time the loop iterates</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:224.28px;top:16.56px" class="cls_009"><span class="cls_009">Summing loops</span></div>
<div style="position:absolute;left:140.56px;top:100.32px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:140.56px;top:121.44px" class="cls_016"><span class="cls_016">total=0</span></div>
<div style="position:absolute;left:140.51px;top:143.52px" class="cls_015"><span class="cls_015">for </span><span class="cls_018">thing </span><span class="cls_015">in </span><span class="cls_018">[9, 41, 12, 3, 74, 15] </span><span class="cls_015">:</span></div>
<div style="position:absolute;left:194.31px;top:165.36px" class="cls_031"><span class="cls_031">total=total+thing</span></div>
<div style="position:absolute;left:198.16px;top:186.48px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_018">thing</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:140.56px;top:208.32px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:140.56px;top:229.44px" class="cls_016"><span class="cls_016">Print(‘Total=‘,total)</span></div>
<div style="position:absolute;left:52.30px;top:280.24px" class="cls_011"><span class="cls_011">• total is a summing variable</span></div>
<div style="position:absolute;left:52.30px;top:327.28px" class="cls_011"><span class="cls_011">• The summing variable starts at zero and</span></div>
<div style="position:absolute;left:79.30px;top:365.44px" class="cls_011"><span class="cls_011">accumulates the total in every loop iteration</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:189.12px;top:16.56px" class="cls_009"><span class="cls_009">Finding the average</span></div>
<div style="position:absolute;left:157.00px;top:90.72px" class="cls_018"><span class="cls_018">count = 0</span></div>
<div style="position:absolute;left:157.00px;top:111.84px" class="cls_017"><span class="cls_017">sum = 0</span></div>
<div style="position:absolute;left:157.00px;top:133.68px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before', </span><span class="cls_018">count,</span><span class="cls_017"> sum</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.00px;top:155.76px" class="cls_015"><span class="cls_015">for </span><span class="cls_031">value </span><span class="cls_015">in </span><span class="cls_031">[9, 41, 12, 3, 74, 15] :</span></div>
<div style="position:absolute;left:200.10px;top:176.88px" class="cls_016"><span class="cls_016">count = count + 1</span></div>
<div style="position:absolute;left:200.10px;top:198.72px" class="cls_016"><span class="cls_016">sum = sum + value</span></div>
<div style="position:absolute;left:200.10px;top:219.84px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_018">count</span><span class="cls_016">, </span><span class="cls_017">sum, </span><span class="cls_031">value</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:157.00px;top:241.68px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After', </span><span class="cls_018">count, </span><span class="cls_017">sum,</span><span class="cls_015"> sum/count</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:40.82px;top:299.20px" class="cls_011"><span class="cls_011">• Average calculation combines the counting</span></div>
<div style="position:absolute;left:67.82px;top:338.08px" class="cls_011"><span class="cls_011">and summing patterns</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:127.18px;top:16.56px" class="cls_009"><span class="cls_009">Finding the largest number</span></div>
<div style="position:absolute;left:145.62px;top:126.24px" class="cls_017"><span class="cls_017">largest_so_far = -1</span></div>
<div style="position:absolute;left:145.62px;top:147.36px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before',</span><span class="cls_017"> largest_so_far</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:145.62px;top:169.44px" class="cls_015"><span class="cls_015">for </span><span class="cls_031">the_num </span><span class="cls_015">in </span><span class="cls_031">[9, 41, 12, 3, 74, 15] :</span></div>
<div style="position:absolute;left:177.92px;top:190.32px" class="cls_031"><span class="cls_031">if the_num > </span><span class="cls_017">largest_so_far</span><span class="cls_031"> :</span></div>
<div style="position:absolute;left:211.80px;top:212.40px" class="cls_017"><span class="cls_017">largest_so_far =</span><span class="cls_031"> the_num</span></div>
<div style="position:absolute;left:179.80px;top:234.24px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">largest_so_far,</span><span class="cls_031"> the_num</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:145.62px;top:277.44px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After',</span><span class="cls_017"> largest_so_far</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:34.25px;top:319.68px" class="cls_008"><span class="cls_008">• Start with an arbitrary value as the</span></div>
<div style="position:absolute;left:61.25px;top:348.72px" class="cls_008"><span class="cls_008">largest_so_far (-1 in this case)</span></div>
<div style="position:absolute;left:34.25px;top:384.72px" class="cls_008"><span class="cls_008">• If the current number in the loop is larger than</span></div>
<div style="position:absolute;left:61.25px;top:413.76px" class="cls_008"><span class="cls_008">the largest_so_far, make the new number the</span></div>
<div style="position:absolute;left:61.25px;top:442.80px" class="cls_008"><span class="cls_008">largest</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:114.22px;top:16.56px" class="cls_009"><span class="cls_009">Finding the smallest number</span></div>
<div style="position:absolute;left:138.23px;top:109.44px" class="cls_017"><span class="cls_017">smallest_so_far = -1</span></div>
<div style="position:absolute;left:138.23px;top:130.56px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before',</span><span class="cls_017"> smallest_so_far</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:138.23px;top:153.60px" class="cls_015"><span class="cls_015">for </span><span class="cls_031">the_num </span><span class="cls_015">in </span><span class="cls_031">[9, 41, 12, 3, 74, 15] :</span></div>
<div style="position:absolute;left:170.53px;top:175.44px" class="cls_031"><span class="cls_031">if the_num </span><span class="cls_032">&lt; </span><span class="cls_017">smallest_so_far</span><span class="cls_031"> :</span></div>
<div style="position:absolute;left:204.43px;top:197.52px" class="cls_017"><span class="cls_017">smallest_so_far =</span><span class="cls_031"> the_num</span></div>
<div style="position:absolute;left:172.43px;top:218.40px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">smallest_so_far,</span><span class="cls_031"> the_num</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:138.23px;top:262.56px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After',</span><span class="cls_017"> smallest_so_far</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:34.25px;top:326.56px" class="cls_011"><span class="cls_011">• What is wrong with the above code?</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:114.22px;top:16.56px" class="cls_009"><span class="cls_009">Finding the smallest number</span></div>
<div style="position:absolute;left:151.79px;top:97.68px" class="cls_017"><span class="cls_017">smallest =</span><span class="cls_015"> None</span></div>
<div style="position:absolute;left:151.79px;top:118.80px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:151.79px;top:140.88px" class="cls_015"><span class="cls_015">for </span><span class="cls_031">value </span><span class="cls_016">in </span><span class="cls_031">[9, 41, 12, 3, 74, 15] :</span></div>
<div style="position:absolute;left:194.89px;top:162.72px" class="cls_015"><span class="cls_015">if </span><span class="cls_017">smallest </span><span class="cls_015">is </span><span class="cls_017">None</span><span class="cls_031"> :</span></div>
<div style="position:absolute;left:237.84px;top:183.84px" class="cls_017"><span class="cls_017">smallest </span><span class="cls_031">= value</span></div>
<div style="position:absolute;left:194.84px;top:205.68px" class="cls_015"><span class="cls_015">elif </span><span class="cls_031">value &lt; </span><span class="cls_017">smallest</span><span class="cls_031"> :</span></div>
<div style="position:absolute;left:237.84px;top:226.80px" class="cls_017"><span class="cls_017">smallest </span><span class="cls_031">= value</span></div>
<div style="position:absolute;left:194.89px;top:248.88px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">smallest,</span><span class="cls_031"> value</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:151.79px;top:270.72px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After',</span><span class="cls_017"> smallest</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:34.25px;top:308.28px" class="cls_033"><span class="cls_033">• None is a special class in Python to initialize a</span></div>
<div style="position:absolute;left:61.25px;top:334.20px" class="cls_033"><span class="cls_033">variable but put nothing into it</span></div>
<div style="position:absolute;left:34.25px;top:366.36px" class="cls_033"><span class="cls_033">• None value is generally checked with the </span><span class="cls_034">if x is</span></div>
<div style="position:absolute;left:61.25px;top:392.28px" class="cls_034"><span class="cls_034">None:</span><span class="cls_033"> statement</span></div>
<div style="position:absolute;left:34.25px;top:425.16px" class="cls_033"><span class="cls_033">• The </span><span class="cls_034">is </span><span class="cls_033">operator checks if the two objects are exactly</span></div>
<div style="position:absolute;left:61.25px;top:451.32px" class="cls_033"><span class="cls_033">the same (similar to == operator)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:288.24px;top:16.56px" class="cls_009"><span class="cls_009">==</span></div>
<div style="position:absolute;left:340.22px;top:16.56px" class="cls_009"><span class="cls_009">vs. is</span></div>
<div style="position:absolute;left:40.00px;top:79.20px" class="cls_017"><span class="cls_017">a = 5</span></div>
<div style="position:absolute;left:40.00px;top:100.32px" class="cls_017"><span class="cls_017">b = 5.0</span></div>
<div style="position:absolute;left:40.00px;top:122.16px" class="cls_015"><span class="cls_015">if </span><span class="cls_017">a </span><span class="cls_015">== </span><span class="cls_017">b</span><span class="cls_031"> :</span></div>
<div style="position:absolute;left:76.00px;top:144.24px" class="cls_017"><span class="cls_017">print(</span><span class="cls_021">'a and b are equal')</span></div>
<div style="position:absolute;left:40.00px;top:165.36px" class="cls_015"><span class="cls_015">else</span><span class="cls_031">:</span></div>
<div style="position:absolute;left:76.00px;top:187.20px" class="cls_017"><span class="cls_017">print(</span><span class="cls_021">'a and b are NOT equal')</span></div>
<div style="position:absolute;left:315.58px;top:237.12px" class="cls_017"><span class="cls_017">a = 5</span></div>
<div style="position:absolute;left:34.20px;top:244.96px" class="cls_011"><span class="cls_011">•</span></div>
<div style="position:absolute;left:61.25px;top:244.96px" class="cls_012"><span class="cls_012">==</span></div>
<div style="position:absolute;left:107.62px;top:244.96px" class="cls_011"><span class="cls_011">checks for</span></div>
<div style="position:absolute;left:315.58px;top:258.00px" class="cls_017"><span class="cls_017">b = 5.0</span></div>
<div style="position:absolute;left:315.58px;top:280.08px" class="cls_015"><span class="cls_015">if </span><span class="cls_017">a </span><span class="cls_015">is </span><span class="cls_017">b</span><span class="cls_031"> :</span></div>
<div style="position:absolute;left:61.25px;top:284.08px" class="cls_011"><span class="cls_011">equivalence</span></div>
<div style="position:absolute;left:351.58px;top:302.16px" class="cls_017"><span class="cls_017">print(</span><span class="cls_021">'a and b are the same')</span></div>
<div style="position:absolute;left:315.58px;top:323.04px" class="cls_015"><span class="cls_015">else</span><span class="cls_031">:</span></div>
<div style="position:absolute;left:34.25px;top:330.16px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> is</span><span class="cls_011"> checks if the</span></div>
<div style="position:absolute;left:351.58px;top:345.12px" class="cls_017"><span class="cls_017">print(</span><span class="cls_021">'a and b are NOT the same')</span></div>
<div style="position:absolute;left:61.25px;top:368.08px" class="cls_011"><span class="cls_011">two objects are</span></div>
<div style="position:absolute;left:61.25px;top:406.96px" class="cls_011"><span class="cls_011">the same</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:113.26px;top:16.56px" class="cls_009"><span class="cls_009">Best Practice for using None</span></div>
<div style="position:absolute;left:45.11px;top:105.84px" class="cls_036"><span class="cls_036">number =</span><span class="cls_017"> None</span></div>
<div style="position:absolute;left:45.11px;top:126.72px" class="cls_017"><span class="cls_017">if </span><span class="cls_036">number ==</span><span class="cls_017"> None:</span></div>
<div style="position:absolute;left:88.21px;top:148.80px" class="cls_036"><span class="cls_036">print("This works, but not preferred ")</span></div>
<div style="position:absolute;left:230.93px;top:225.60px" class="cls_036"><span class="cls_036">number =</span><span class="cls_017"> None</span></div>
<div style="position:absolute;left:230.93px;top:246.48px" class="cls_017"><span class="cls_017">if </span><span class="cls_036">number </span><span class="cls_017">is None:</span></div>
<div style="position:absolute;left:273.98px;top:268.56px" class="cls_036"><span class="cls_036">print("This works, and is preferred ")</span></div>
<div style="position:absolute;left:34.25px;top:310.56px" class="cls_008"><span class="cls_008">• The </span><span class="cls_035">is </span><span class="cls_008">comparison operator checks if both the</span></div>
<div style="position:absolute;left:61.25px;top:342.72px" class="cls_008"><span class="cls_008">variables are pointing to the same object</span></div>
<div style="position:absolute;left:34.20px;top:381.60px" class="cls_008"><span class="cls_008">• == checks for equivalence and depending on</span></div>
<div style="position:absolute;left:61.25px;top:413.52px" class="cls_008"><span class="cls_008">how the equivalence is implemented, may or</span></div>
<div style="position:absolute;left:61.25px;top:445.68px" class="cls_008"><span class="cls_008">may not return the correct answer</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:209.28px;top:16.56px" class="cls_009"><span class="cls_009">Filtering in a loop</span></div>
<div style="position:absolute;left:152.99px;top:107.52px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:152.94px;top:128.40px" class="cls_015"><span class="cls_015">for </span><span class="cls_031">value </span><span class="cls_015">in </span><span class="cls_031">[9, 41, 12, 3, 74, 15] :</span></div>
<div style="position:absolute;left:196.04px;top:150.48px" class="cls_015"><span class="cls_015">if </span><span class="cls_031">value </span><span class="cls_018">> 20:</span></div>
<div style="position:absolute;left:232.09px;top:172.56px" class="cls_018"><span class="cls_018">print('Large number',value)</span></div>
<div style="position:absolute;left:152.99px;top:193.44px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'After'</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:34.25px;top:265.60px" class="cls_011"><span class="cls_011">• If statement inside the loop is used to filter</span></div>
<div style="position:absolute;left:61.25px;top:304.72px" class="cls_011"><span class="cls_011">what we are looking for</span></div>
<div style="position:absolute;left:34.25px;top:350.80px" class="cls_011"><span class="cls_011">• In this case we are looking for the largest</span></div>
<div style="position:absolute;left:61.25px;top:388.72px" class="cls_011"><span class="cls_011">value greater than 20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:77.33px;top:28.08px" class="cls_009"><span class="cls_009">Search using a Boolean variable</span></div>
<div style="position:absolute;left:152.99px;top:98.16px" class="cls_017"><span class="cls_017">found =</span><span class="cls_015"> False</span></div>
<div style="position:absolute;left:152.99px;top:119.28px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_021">'Before',</span><span class="cls_017"> found</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:152.94px;top:141.12px" class="cls_015"><span class="cls_015">for </span><span class="cls_031">value </span><span class="cls_015">in </span><span class="cls_031">[9, 41, 12, 3, 74, 15] :</span></div>
<div style="position:absolute;left:185.24px;top:163.20px" class="cls_015"><span class="cls_015">if </span><span class="cls_031">value == 3 :</span></div>
<div style="position:absolute;left:228.34px;top:184.08px" class="cls_017"><span class="cls_017">found =</span><span class="cls_015"> True</span></div>
<div style="position:absolute;left:185.24px;top:206.16px" class="cls_015"><span class="cls_015">print</span><span class="cls_016">(</span><span class="cls_017">found</span><span class="cls_031">, value</span><span class="cls_016">)</span></div>
<div style="position:absolute;left:152.99px;top:228.24px" class="cls_037"><span class="cls_037">print</span><span class="cls_038">(</span><span class="cls_039">'After',</span><span class="cls_040"> found</span><span class="cls_038">)</span></div>
<div style="position:absolute;left:34.25px;top:265.60px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> found </span><span class="cls_011">is a Boolean variable set to False to</span></div>
<div style="position:absolute;left:61.25px;top:304.72px" class="cls_011"><span class="cls_011">begin with</span></div>
<div style="position:absolute;left:34.25px;top:350.80px" class="cls_011"><span class="cls_011">• If the value is found the variable </span><span class="cls_012">found</span><span class="cls_011"> is</span></div>
<div style="position:absolute;left:61.25px;top:388.72px" class="cls_011"><span class="cls_011">updated to True</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.20px;top:16.56px" class="cls_009"><span class="cls_009">Summary</span></div>
<div style="position:absolute;left:81.82px;top:67.88px" class="cls_041"><span class="cls_041">§ Types of Loops</span></div>
<div style="position:absolute;left:117.82px;top:102.80px" class="cls_042"><span class="cls_042">§ Indefinite loops</span></div>
<div style="position:absolute;left:117.82px;top:134.96px" class="cls_042"><span class="cls_042">§ Definite loops</span></div>
<div style="position:absolute;left:81.82px;top:167.00px" class="cls_041"><span class="cls_041">§ While loops</span></div>
<div style="position:absolute;left:117.82px;top:201.92px" class="cls_042"><span class="cls_042">§ Infinite loops</span></div>
<div style="position:absolute;left:117.82px;top:233.84px" class="cls_042"><span class="cls_042">§ Dead code</span></div>
<div style="position:absolute;left:117.82px;top:264.80px" class="cls_042"><span class="cls_042">§ Break and continue statements</span></div>
<div style="position:absolute;left:81.82px;top:296.84px" class="cls_041"><span class="cls_041">§ For loops</span></div>
<div style="position:absolute;left:117.82px;top:332.00px" class="cls_042"><span class="cls_042">§ Break and continue statements</span></div>
<div style="position:absolute;left:117.82px;top:363.92px" class="cls_042"><span class="cls_042">§ Infinite execution</span></div>
<div style="position:absolute;left:81.82px;top:395.96px" class="cls_041"><span class="cls_041">§ How to write a loop</span></div>
<div style="position:absolute;left:81.82px;top:431.96px" class="cls_041"><span class="cls_041">§ Example loop patterns</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.50px;top:16.56px" class="cls_009"><span class="cls_009">To Do List for Next Class</span></div>
<div style="position:absolute;left:107.95px;top:96.16px" class="cls_043"><span class="cls_043">• Read Chapters 5 and 6</span></div>
<div style="position:absolute;left:107.95px;top:146.08px" class="cls_043"><span class="cls_043">• Execute code snippets and</span></div>
<div style="position:absolute;left:134.95px;top:192.16px" class="cls_043"><span class="cls_043">chapter exercises in the book</span></div>
<div style="position:absolute;left:107.95px;top:250.24px" class="cls_043"><span class="cls_043">• Study for Exam1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.21px;top:16.56px" class="cls_009"><span class="cls_009">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.31px;top:59.76px" class="cls_009"><span class="cls_009">tions</span></div>
<div style="position:absolute;left:107.95px;top:127.44px" class="cls_044"><span class="cls_044">§  These slides are Copyright 2010-  Charles R. Severance</span></div>
<div style="position:absolute;left:134.95px;top:148.56px" class="cls_044"><span class="cls_044">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.95px;top:170.40px" class="cls_044"><span class="cls_044">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.95px;top:191.52px" class="cls_044"><span class="cls_044">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.95px;top:213.36px" class="cls_044"><span class="cls_044">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.95px;top:235.44px" class="cls_044"><span class="cls_044">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.95px;top:256.56px" class="cls_044"><span class="cls_044">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.95px;top:278.40px" class="cls_044"><span class="cls_044">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.95px;top:299.52px" class="cls_044"><span class="cls_044">materials.</span></div>
<div style="position:absolute;left:107.95px;top:352.56px" class="cls_044"><span class="cls_044">§  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.90px;top:374.40px" class="cls_044"><span class="cls_044">Michigan School of Information</span></div>
<div style="position:absolute;left:107.95px;top:427.44px" class="cls_044"><span class="cls_044">§  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.90px;top:448.56px" class="cls_044"><span class="cls_044">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8Iteration/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>

</body>
</html>
