<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_026{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_027{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_028{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter7file/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.57px;top:185.68px" class="cls_002"><span class="cls_002">Chapter 7:</span></div>
<div style="position:absolute;left:44.57px;top:222.64px" class="cls_002"><span class="cls_002">Files</span></div>
<div style="position:absolute;left:44.93px;top:353.52px" class="cls_003"><span class="cls_003">INSY 5336: Python Programming</span></div>
<div style="position:absolute;left:44.57px;top:407.28px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:44.57px;top:432.16px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:210.74px;top:222.40px" class="cls_007"><span class="cls_007">Chapter 7: Files</span></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.20px;top:16.56px" class="cls_009"><span class="cls_009">File Processing</span></div>
<div style="position:absolute;left:55.70px;top:100.00px" class="cls_008"><span class="cls_008">• Frequently we need to analyze text files</span></div>
<div style="position:absolute;left:82.70px;top:138.88px" class="cls_008"><span class="cls_008">for data analysis</span></div>
<div style="position:absolute;left:55.70px;top:184.96px" class="cls_008"><span class="cls_008">• A text file is stored in secondary memory</span></div>
<div style="position:absolute;left:55.70px;top:230.80px" class="cls_008"><span class="cls_008">• A text file is a sequence of lines.</span></div>
<div style="position:absolute;left:55.70px;top:277.84px" class="cls_008"><span class="cls_008">• In contrast a string is a sequence of</span></div>
<div style="position:absolute;left:82.70px;top:316.00px" class="cls_008"><span class="cls_008">characters</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.20px;top:16.56px" class="cls_009"><span class="cls_009">File Processing</span></div>
<div style="position:absolute;left:39.91px;top:74.16px" class="cls_010"><span class="cls_010">From </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span><span class="cls_027"> </span><span class="cls_010">Sat Jan</span></div>
<div style="position:absolute;left:480.56px;top:74.16px" class="cls_010"><span class="cls_010">5 09:14:16 2008</span></div>
<div style="position:absolute;left:39.91px;top:95.04px" class="cls_010"><span class="cls_010">Return-Path: &lt;</span><A HREF="mailto:postmaster@collab.sakaiproject.org">postmaster@collab.sakaiproject.org</A> </span><span class="cls_010">></span></div>
<div style="position:absolute;left:39.91px;top:117.12px" class="cls_010"><span class="cls_010">Date: Sat, 5 Jan 2008 09:12:18 -0500</span></div>
<div style="position:absolute;left:39.91px;top:139.20px" class="cls_010"><span class="cls_010">To: </span><A HREF="mailto:source@collab.sakaiproject.org">source@collab.sakaiproject.org</A> </span></div>
<div style="position:absolute;left:39.91px;top:160.08px" class="cls_010"><span class="cls_010">From:</span><span class="cls_011"> </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span></div>
<div style="position:absolute;left:39.91px;top:182.16px" class="cls_010"><span class="cls_010">Subject: [sakai] svn commit: r39772 - content/branches/</span></div>
<div style="position:absolute;left:39.91px;top:226.08px" class="cls_010"><span class="cls_010">Details:</span></div>
<div style="position:absolute;left:39.91px;top:247.20px" class="cls_026"><span class="cls_026"> </span><A HREF="http://source.sakaiproject.org/viewsvn/?view=rev&amp;rev=39772">http://source.sakaiproject.org/viewsvn/?view=rev&rev=39772</A> </div>
<div style="position:absolute;left:47.11px;top:345.52px" class="cls_008"><span class="cls_008">• A text file is a sequence of lines.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:152.14px;top:16.56px" class="cls_009"><span class="cls_009">The “newline” character</span></div>
<div style="position:absolute;left:7.20px;top:100.00px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:34.20px;top:100.00px" class="cls_008"><span class="cls_008">We use a special</span></div>
<div style="position:absolute;left:395.40px;top:137.76px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_014">stuff </span><span class="cls_012">= 'Hello</span><span class="cls_015">\n</span><span class="cls_012">World!'</span></div>
<div style="position:absolute;left:34.20px;top:138.88px" class="cls_008"><span class="cls_008">character called the</span></div>
<div style="position:absolute;left:395.40px;top:158.64px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_014">stuff</span></div>
<div style="position:absolute;left:395.40px;top:180.72px" class="cls_012"><span class="cls_012">'Hello</span><span class="cls_015">\n</span><span class="cls_012">World!'</span></div>
<div style="position:absolute;left:34.20px;top:176.80px" class="cls_008"><span class="cls_008">“newline” to indicate</span></div>
<div style="position:absolute;left:395.40px;top:202.56px" class="cls_012"><span class="cls_012">>>></span><span class="cls_013"> print</span><span class="cls_012">(</span><span class="cls_014">stuff</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:395.40px;top:223.68px" class="cls_012"><span class="cls_012">Hello</span></div>
<div style="position:absolute;left:34.20px;top:214.96px" class="cls_008"><span class="cls_008">when a line ends</span></div>
<div style="position:absolute;left:395.40px;top:245.76px" class="cls_012"><span class="cls_012">World!</span></div>
<div style="position:absolute;left:395.40px;top:266.64px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_014">stuff </span><span class="cls_012">= 'X</span><span class="cls_015">\n</span><span class="cls_012">Y'</span></div>
<div style="position:absolute;left:7.20px;top:262.00px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:34.20px;top:262.00px" class="cls_008"><span class="cls_008">We represent it as \n in</span></div>
<div style="position:absolute;left:395.40px;top:288.72px" class="cls_012"><span class="cls_012">>>></span><span class="cls_013"> print</span><span class="cls_012">(</span><span class="cls_014">stuff</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:395.40px;top:310.56px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:34.20px;top:299.92px" class="cls_008"><span class="cls_008">strings</span></div>
<div style="position:absolute;left:395.40px;top:331.68px" class="cls_012"><span class="cls_012">Y</span></div>
<div style="position:absolute;left:395.40px;top:353.76px" class="cls_012"><span class="cls_012">>>></span><span class="cls_010"> len</span><span class="cls_012">(</span><span class="cls_014">stuff</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:7.20px;top:346.96px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:34.20px;top:346.96px" class="cls_008"><span class="cls_008">Newline is still one</span></div>
<div style="position:absolute;left:395.40px;top:374.64px" class="cls_012"><span class="cls_012">3</span></div>
<div style="position:absolute;left:34.20px;top:384.88px" class="cls_008"><span class="cls_008">character - not two</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.20px;top:16.56px" class="cls_009"><span class="cls_009">File Processing</span></div>
<div style="position:absolute;left:17.95px;top:80.16px" class="cls_010"><span class="cls_010">From </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span><span class="cls_027"> </span><span class="cls_010">Sat Jan</span></div>
<div style="position:absolute;left:458.60px;top:80.16px" class="cls_010"><span class="cls_010">5 09:14:16 2008</span><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:101.04px" class="cls_010"><span class="cls_010">Return-Path: &lt;</span><A HREF="mailto:postmaster@collab.sakaiproject.org">postmaster@collab.sakaiproject.org</A> </span><span class="cls_010">></span><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:123.12px" class="cls_010"><span class="cls_010">Date: Sat, 5 Jan 2008 09:12:18 -0500</span><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:145.20px" class="cls_010"><span class="cls_010">To: source@collab.sakaiproject.org</span><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:166.08px" class="cls_010"><span class="cls_010">From: stephen.marquard@uct.ac.za</span><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:188.16px" class="cls_010"><span class="cls_010">Subject: [sakai] svn commit: r39772 - content/branches/</span><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:209.04px" class="cls_015"><span class="cls_015">\n</span></div>
<div style="position:absolute;left:17.95px;top:231.12px" class="cls_010"><span class="cls_010">Details:</span></div>
<div style="position:absolute;left:17.95px;top:253.20px" class="cls_026"><span class="cls_026"> </span><A HREF="http://source.sakaiproject.org/viewsvn/?view=rev&amp;rev=39772%5Cn">http://source.sakaiproject.org/viewsvn/?view=rev&rev=39772\n</A> </div>
<div style="position:absolute;left:47.11px;top:345.52px" class="cls_008"><span class="cls_008">• A text file is a sequence of lines ending</span></div>
<div style="position:absolute;left:74.11px;top:384.64px" class="cls_008"><span class="cls_008">with the newline character (\n).</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:235.32px;top:16.56px" class="cls_009"><span class="cls_009">Opening a File</span></div>
<div style="position:absolute;left:55.70px;top:95.92px" class="cls_008"><span class="cls_008">• Before we can read the contents of the</span></div>
<div style="position:absolute;left:82.70px;top:130.96px" class="cls_008"><span class="cls_008">file, we must tell Python which file we are</span></div>
<div style="position:absolute;left:82.70px;top:164.80px" class="cls_008"><span class="cls_008">going to work with and what we will be</span></div>
<div style="position:absolute;left:82.70px;top:199.84px" class="cls_008"><span class="cls_008">doing with the file</span></div>
<div style="position:absolute;left:55.70px;top:241.84px" class="cls_008"><span class="cls_008">• This is done with the open() function</span></div>
<div style="position:absolute;left:55.70px;top:284.80px" class="cls_008"><span class="cls_008">• open() returns a “file handle” - a variable</span></div>
<div style="position:absolute;left:82.70px;top:319.84px" class="cls_008"><span class="cls_008">used to perform operations on the file</span></div>
<div style="position:absolute;left:55.70px;top:362.80px" class="cls_008"><span class="cls_008">• Similar to “File -> Open” in a Word</span></div>
<div style="position:absolute;left:82.70px;top:397.84px" class="cls_008"><span class="cls_008">Processor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:249.24px;top:16.56px" class="cls_009"><span class="cls_009">Using open()</span></div>
<div style="position:absolute;left:55.70px;top:71.76px" class="cls_016"><span class="cls_016">• handle = open(filename, mode)</span></div>
<div style="position:absolute;left:91.70px;top:122.80px" class="cls_008"><span class="cls_008">-Example: fhandle = open('mbox.txt', 'r')</span></div>
<div style="position:absolute;left:55.70px;top:170.88px" class="cls_016"><span class="cls_016">• returns a handle to manipulate the file</span></div>
<div style="position:absolute;left:55.70px;top:222.72px" class="cls_016"><span class="cls_016">• filename is a string</span></div>
<div style="position:absolute;left:55.70px;top:274.80px" class="cls_016"><span class="cls_016">• mode is optional and</span></div>
<div style="position:absolute;left:91.70px;top:325.84px" class="cls_008"><span class="cls_008">- should be 'r' if read only access is</span></div>
<div style="position:absolute;left:114.25px;top:364.72px" class="cls_008"><span class="cls_008">needed</span></div>
<div style="position:absolute;left:91.70px;top:410.80px" class="cls_008"><span class="cls_008">- should be 'w' if read and write access is</span></div>
<div style="position:absolute;left:114.25px;top:449.68px" class="cls_008"><span class="cls_008">needed</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.28px;top:16.56px" class="cls_009"><span class="cls_009">What is a handle?</span></div>
<div style="position:absolute;left:16.20px;top:84.72px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_014">fhand </span><span class="cls_012">=</span><span class="cls_017"> open</span><span class="cls_012">(</span><span class="cls_017">'mbox.txt'</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:16.20px;top:105.60px" class="cls_012"><span class="cls_012">>>></span><span class="cls_013"> print(</span><span class="cls_010">fhand</span><span class="cls_013">)</span></div>
<div style="position:absolute;left:16.20px;top:127.68px" class="cls_012"><span class="cls_012">&lt;_io.TextIOWrapper name='mbox.txt' mode='r' encoding='UTF-8'></span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:133.18px;top:16.56px" class="cls_009"><span class="cls_009">File Handle as a Sequence</span></div>
<div style="position:absolute;left:7.20px;top:82.72px" class="cls_008"><span class="cls_008">• A file handle open for read</span></div>
<div style="position:absolute;left:34.20px;top:117.76px" class="cls_008"><span class="cls_008">can be treated as a</span></div>
<div style="position:absolute;left:34.20px;top:151.84px" class="cls_008"><span class="cls_008">sequence of strings where</span></div>
<div style="position:absolute;left:34.20px;top:186.88px" class="cls_008"><span class="cls_008">each line in the file is a</span></div>
<div style="position:absolute;left:412.97px;top:199.44px" class="cls_017"><span class="cls_017">xfile </span><span class="cls_012">=</span><span class="cls_010"> open</span><span class="cls_012">('mbox.txt')</span></div>
<div style="position:absolute;left:412.97px;top:221.52px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">cheese </span><span class="cls_013">in</span><span class="cls_017"> xfile</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:34.20px;top:221.68px" class="cls_008"><span class="cls_008">string in the sequence</span></div>
<div style="position:absolute;left:456.02px;top:242.40px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">(</span><span class="cls_014">cheese</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:7.20px;top:263.68px" class="cls_008"><span class="cls_008">• We can use the</span><span class="cls_018"> for</span></div>
<div style="position:absolute;left:34.20px;top:298.72px" class="cls_008"><span class="cls_008">statement to iterate</span></div>
<div style="position:absolute;left:34.20px;top:332.80px" class="cls_008"><span class="cls_008">through a sequence</span></div>
<div style="position:absolute;left:7.20px;top:375.76px" class="cls_008"><span class="cls_008">• Remember - a sequence</span></div>
<div style="position:absolute;left:34.20px;top:410.80px" class="cls_008"><span class="cls_008">is an ordered set</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:171.22px;top:16.56px" class="cls_009"><span class="cls_009">When a file is missing</span></div>
<div style="position:absolute;left:90.48px;top:124.32px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_014">fhand </span><span class="cls_012">=</span><span class="cls_010"> open</span><span class="cls_012">('</span><span class="cls_017">stuff.txt'</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:90.48px;top:145.20px" class="cls_012"><span class="cls_012">Traceback (most recent call last):</span></div>
<div style="position:absolute;left:112.08px;top:167.28px" class="cls_012"><span class="cls_012">File "&lt;stdin>", line 1, in &lt;module></span></div>
<div style="position:absolute;left:90.48px;top:189.36px" class="cls_012"><span class="cls_012">FileNotFoundError: [Errno 2] </span><span class="cls_017">No such file or</span></div>
<div style="position:absolute;left:90.48px;top:210.24px" class="cls_017"><span class="cls_017">directory: 'stuff.txt'</span></div>
<div style="position:absolute;left:40.99px;top:278.88px" class="cls_019"><span class="cls_019">A file open error occurs if:</span></div>
<div style="position:absolute;left:40.99px;top:318.96px" class="cls_019"><span class="cls_019">• The file does not exist as specified in the</span></div>
<div style="position:absolute;left:67.99px;top:350.88px" class="cls_019"><span class="cls_019">argument</span></div>
<div style="position:absolute;left:40.99px;top:389.76px" class="cls_019"><span class="cls_019">• The file is located in a different directory from</span></div>
<div style="position:absolute;left:67.99px;top:421.92px" class="cls_019"><span class="cls_019">which python is being run</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:156.22px;top:16.56px" class="cls_009"><span class="cls_009">Counting Lines in a File</span></div>
<div style="position:absolute;left:21.89px;top:153.04px" class="cls_008"><span class="cls_008">• Open a file read-only</span></div>
<div style="position:absolute;left:412.94px;top:169.20px" class="cls_014"><span class="cls_014">fhand </span><span class="cls_012">= </span><span class="cls_010">open</span><span class="cls_012">('mbox.txt')</span></div>
<div style="position:absolute;left:412.94px;top:190.32px" class="cls_017"><span class="cls_017">count </span><span class="cls_012">= 0</span></div>
<div style="position:absolute;left:21.89px;top:199.84px" class="cls_008"><span class="cls_008">• Use a for loop to read</span></div>
<div style="position:absolute;left:412.89px;top:212.16px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">line </span><span class="cls_013">in </span><span class="cls_014">fhand</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:455.99px;top:234.24px" class="cls_017"><span class="cls_017">count </span><span class="cls_012">= </span><span class="cls_017">count </span><span class="cls_012">+ 1</span></div>
<div style="position:absolute;left:48.89px;top:238.00px" class="cls_008"><span class="cls_008">each line</span></div>
<div style="position:absolute;left:412.94px;top:255.12px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">('Line Count:',</span><span class="cls_017"> count</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:21.89px;top:283.84px" class="cls_008"><span class="cls_008">• Count the lines and print</span></div>
<div style="position:absolute;left:412.94px;top:322.32px" class="cls_012"><span class="cls_012">$ </span><span class="cls_014">python open.py</span></div>
<div style="position:absolute;left:48.89px;top:322.96px" class="cls_008"><span class="cls_008">out the number of lines</span></div>
<div style="position:absolute;left:412.94px;top:343.20px" class="cls_012"><span class="cls_012">Line Count: 132045</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:193.32px;top:16.56px" class="cls_009"><span class="cls_009">Read the Whole file</span></div>
<div style="position:absolute;left:64.51px;top:107.44px" class="cls_008"><span class="cls_008">• We can read the whole file including</span></div>
<div style="position:absolute;left:91.51px;top:146.32px" class="cls_008"><span class="cls_008">newline characters into a single string</span></div>
<div style="position:absolute;left:173.52px;top:262.80px" class="cls_020"><span class="cls_020">></span><span class="cls_012">>> </span><span class="cls_014">fhand </span><span class="cls_012">=</span><span class="cls_010"> open</span><span class="cls_012">('mbox-short.txt')</span></div>
<div style="position:absolute;left:173.52px;top:283.92px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_015">inp </span><span class="cls_012">=</span><span class="cls_014"> fhand</span><span class="cls_017">.read</span><span class="cls_012">()</span></div>
<div style="position:absolute;left:173.52px;top:305.76px" class="cls_012"><span class="cls_012">>>></span><span class="cls_013"> print</span><span class="cls_012">(</span><span class="cls_010">len</span><span class="cls_012">(</span><span class="cls_015">inp</span><span class="cls_012">))</span></div>
<div style="position:absolute;left:173.52px;top:327.84px" class="cls_012"><span class="cls_012">94626</span></div>
<div style="position:absolute;left:173.52px;top:348.72px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_013">print</span><span class="cls_012">(</span><span class="cls_015">inp</span><span class="cls_012">[:20])</span></div>
<div style="position:absolute;left:173.52px;top:370.80px" class="cls_012"><span class="cls_012">From stephen.marquar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:153.22px;top:16.56px" class="cls_009"><span class="cls_009">Searching through a file</span></div>
<div style="position:absolute;left:25.42px;top:109.68px" class="cls_019"><span class="cls_019">• We can put an if</span></div>
<div style="position:absolute;left:393.65px;top:125.28px" class="cls_010"><span class="cls_010">From:</span></div>
<div style="position:absolute;left:393.65px;top:146.16px" class="cls_010"><span class="cls_010">stephen.marquard@uct.ac.za</span><span class="cls_014">\n</span></div>
<div style="position:absolute;left:52.42px;top:141.60px" class="cls_019"><span class="cls_019">statement in our for</span></div>
<div style="position:absolute;left:393.65px;top:168.24px" class="cls_013"><span class="cls_013">\n</span></div>
<div style="position:absolute;left:52.42px;top:173.52px" class="cls_019"><span class="cls_019">loop to only print lines</span></div>
<div style="position:absolute;left:393.65px;top:190.08px" class="cls_010"><span class="cls_010">From:</span></div>
<div style="position:absolute;left:393.65px;top:211.20px" class="cls_010"><span class="cls_010">louis@media.berkeley.edu</span><span class="cls_014">\n</span></div>
<div style="position:absolute;left:52.42px;top:206.64px" class="cls_019"><span class="cls_019">that meet some criteria</span></div>
<div style="position:absolute;left:393.65px;top:233.28px" class="cls_013"><span class="cls_013">\n</span></div>
<div style="position:absolute;left:393.65px;top:254.16px" class="cls_010"><span class="cls_010">From: zqian@umich.edu</span><span class="cls_014">\n</span></div>
<div style="position:absolute;left:4.37px;top:271.20px" class="cls_014"><span class="cls_014">fhand </span><span class="cls_012">= </span><span class="cls_010">open</span><span class="cls_012">('mbox-short.txt')</span></div>
<div style="position:absolute;left:393.65px;top:276.24px" class="cls_013"><span class="cls_013">\n</span></div>
<div style="position:absolute;left:4.37px;top:292.32px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">line </span><span class="cls_013">in</span><span class="cls_014"> fhand</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:393.65px;top:298.08px" class="cls_010"><span class="cls_010">From: rjlowe@iupui.edu</span><span class="cls_014">\n</span></div>
<div style="position:absolute;left:47.42px;top:314.40px" class="cls_013"><span class="cls_013">if </span><span class="cls_014">line</span><span class="cls_010">.startswith</span><span class="cls_012">('From:') :</span></div>
<div style="position:absolute;left:393.65px;top:319.20px" class="cls_013"><span class="cls_013">\n</span></div>
<div style="position:absolute;left:90.42px;top:336.24px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">(</span><span class="cls_014">line</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:393.65px;top:356.08px" class="cls_008"><span class="cls_008">• Output contains</span></div>
<div style="position:absolute;left:420.65px;top:394.96px" class="cls_008"><span class="cls_008">additional newline</span></div>
<div style="position:absolute;left:420.65px;top:433.12px" class="cls_008"><span class="cls_008">characters</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:153.22px;top:16.56px" class="cls_009"><span class="cls_009">Searching through a file</span></div>
<div style="position:absolute;left:25.42px;top:123.28px" class="cls_008"><span class="cls_008">• We can strip the</span></div>
<div style="position:absolute;left:52.42px;top:162.40px" class="cls_008"><span class="cls_008">newline character at</span></div>
<div style="position:absolute;left:393.41px;top:191.04px" class="cls_021"><span class="cls_021">From:</span><span class="cls_022"> </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span></div>
<div style="position:absolute;left:393.41px;top:211.92px" class="cls_021"><span class="cls_021">From: </span><A HREF="mailto:louis@media.berkeley.edu">louis@media.berkeley.edu</A> </span></div>
<div style="position:absolute;left:52.42px;top:200.32px" class="cls_008"><span class="cls_008">the end before printing</span></div>
<div style="position:absolute;left:393.41px;top:234.00px" class="cls_021"><span class="cls_021">From:</span><span class="cls_022"> </span><A HREF="mailto:zqian@umich.edu">zqian@umich.edu</A> </span></div>
<div style="position:absolute;left:393.41px;top:255.12px" class="cls_021"><span class="cls_021">From:</span><span class="cls_022"> </span><A HREF="mailto:rjlowe@iupui.edu">rjlowe@iupui.edu</A> </span></div>
<div style="position:absolute;left:9.72px;top:282.96px" class="cls_014"><span class="cls_014">fhand </span><span class="cls_012">=</span><span class="cls_010"> open</span><span class="cls_012">('mbox-short.txt')</span></div>
<div style="position:absolute;left:9.72px;top:303.84px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">line </span><span class="cls_013">in</span><span class="cls_014"> fhand</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:52.77px;top:325.92px" class="cls_014"><span class="cls_014">line </span><span class="cls_012">=</span><span class="cls_014"> line</span><span class="cls_017">.rstrip</span><span class="cls_012">()</span></div>
<div style="position:absolute;left:52.72px;top:348.00px" class="cls_013"><span class="cls_013">if </span><span class="cls_014">line</span><span class="cls_010">.startswith</span><span class="cls_012">('From:') :</span></div>
<div style="position:absolute;left:393.65px;top:338.08px" class="cls_008"><span class="cls_008">• Output contains no</span></div>
<div style="position:absolute;left:95.72px;top:368.88px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">(</span><span class="cls_014">line</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:420.65px;top:376.96px" class="cls_008"><span class="cls_008">additional newline</span></div>
<div style="position:absolute;left:420.65px;top:415.12px" class="cls_008"><span class="cls_008">characters</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:162.34px;top:16.56px" class="cls_009"><span class="cls_009">Skipping with continue</span></div>
<div style="position:absolute;left:65.98px;top:119.20px" class="cls_008"><span class="cls_008">• We can skip a line by using the continue</span></div>
<div style="position:absolute;left:92.98px;top:158.08px" class="cls_008"><span class="cls_008">statement</span></div>
<div style="position:absolute;left:158.40px;top:277.44px" class="cls_014"><span class="cls_014">fhand </span><span class="cls_012">=</span><span class="cls_014"> open</span><span class="cls_012">('mbox-short.txt')</span></div>
<div style="position:absolute;left:158.40px;top:298.56px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">line </span><span class="cls_013">in</span><span class="cls_014"> fhand</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:201.45px;top:320.64px" class="cls_014"><span class="cls_014">line </span><span class="cls_012">=</span><span class="cls_014"> line</span><span class="cls_010">.rstrip</span><span class="cls_012">()</span></div>
<div style="position:absolute;left:201.40px;top:342.48px" class="cls_013"><span class="cls_013">if not </span><span class="cls_014">line</span><span class="cls_010">.startswith</span><span class="cls_012">('From:') :</span></div>
<div style="position:absolute;left:244.40px;top:363.60px" class="cls_013"><span class="cls_013">continue</span></div>
<div style="position:absolute;left:201.45px;top:385.44px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">(</span><span class="cls_014">line</span><span class="cls_012">)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:164.26px;top:16.56px" class="cls_009"><span class="cls_009">Using in to select lines</span></div>
<div style="position:absolute;left:342.41px;top:101.04px" class="cls_014"><span class="cls_014">fhand </span><span class="cls_012">= </span><span class="cls_010">open</span><span class="cls_012">('mbox-short.txt')</span></div>
<div style="position:absolute;left:7.20px;top:91.84px" class="cls_008"><span class="cls_008">• We can look for a</span></div>
<div style="position:absolute;left:342.41px;top:121.92px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">line </span><span class="cls_013">in</span><span class="cls_014"> fhand</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:34.20px;top:130.72px" class="cls_008"><span class="cls_008">string anywhere in a</span></div>
<div style="position:absolute;left:385.46px;top:144.00px" class="cls_014"><span class="cls_014">line </span><span class="cls_012">=</span><span class="cls_014"> line</span><span class="cls_010">.rstrip</span><span class="cls_012">()</span></div>
<div style="position:absolute;left:385.46px;top:166.08px" class="cls_013"><span class="cls_013">if not </span><span class="cls_012">'</span><span class="cls_015">@uct.ac.za</span><span class="cls_012">' </span><span class="cls_013">in </span><span class="cls_015">line </span><span class="cls_012">:</span></div>
<div style="position:absolute;left:34.20px;top:168.88px" class="cls_008"><span class="cls_008">line as our selection</span></div>
<div style="position:absolute;left:428.46px;top:186.96px" class="cls_013"><span class="cls_013">continue</span></div>
<div style="position:absolute;left:385.46px;top:209.04px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">(</span><span class="cls_014">line</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:34.20px;top:206.80px" class="cls_008"><span class="cls_008">criteria</span></div>
<div style="position:absolute;left:29.18px;top:296.40px" class="cls_010"><span class="cls_010">From </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span><span class="cls_010">Sat Jan</span></div>
<div style="position:absolute;left:469.84px;top:296.40px" class="cls_010"><span class="cls_010">5 09:14:16 2008</span></div>
<div style="position:absolute;left:29.18px;top:317.52px" class="cls_010"><span class="cls_010">X-Authentication-Warning: set sender to</span></div>
<div style="position:absolute;left:29.18px;top:339.36px" class="cls_026"><span class="cls_026"> </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> <span class="cls_010">using -f</span></div>
<div style="position:absolute;left:29.18px;top:361.44px" class="cls_010"><span class="cls_010">From: </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span></div>
<div style="position:absolute;left:29.18px;top:382.32px" class="cls_010"><span class="cls_010">Author:</span><span class="cls_011"> </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span></div>
<div style="position:absolute;left:29.18px;top:404.40px" class="cls_010"><span class="cls_010">From </span><A HREF="mailto:david.horwitz@uct.ac.za">david.horwitz@uct.ac.za</A> </span><span class="cls_027"> </span><span class="cls_010">Fri Jan</span></div>
<div style="position:absolute;left:437.53px;top:404.40px" class="cls_010"><span class="cls_010">4 07:02:32 2008</span></div>
<div style="position:absolute;left:29.18px;top:425.52px" class="cls_010"><span class="cls_010">X-Authentication-Warning: set sender to</span></div>
<div style="position:absolute;left:29.18px;top:447.36px" class="cls_026"><span class="cls_026"> </span><A HREF="mailto:david.horwitz@uct.ac.za">david.horwitz@uct.ac.za</A> <span class="cls_010">using -f...</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:124.30px;top:16.56px" class="cls_009"><span class="cls_009">Robustness in file handling</span></div>
<div style="position:absolute;left:29.18px;top:77.20px" class="cls_008"><span class="cls_008">• Good practice to enclose file opening</span></div>
<div style="position:absolute;left:56.18px;top:116.32px" class="cls_008"><span class="cls_008">statements with try and except block</span></div>
<div style="position:absolute;left:29.18px;top:162.16px" class="cls_008"><span class="cls_008">• quit() command exits the program</span></div>
<div style="position:absolute;left:84.00px;top:214.56px" class="cls_014"><span class="cls_014">fname </span><span class="cls_012">= </span><span class="cls_010">input</span><span class="cls_012">('Enter the file name:   ')</span></div>
<div style="position:absolute;left:84.00px;top:235.68px" class="cls_013"><span class="cls_013">try</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:126.95px;top:257.52px" class="cls_014"><span class="cls_014">fhand </span><span class="cls_012">=</span><span class="cls_010"> open</span><span class="cls_012">(</span><span class="cls_014">fname</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:84.00px;top:279.60px" class="cls_013"><span class="cls_013">except</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:126.95px;top:300.48px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">('File cannot be opened:', </span><span class="cls_014">fname</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:126.95px;top:322.56px" class="cls_010"><span class="cls_010">quit</span><span class="cls_012">()</span></div>
<div style="position:absolute;left:84.00px;top:366.48px" class="cls_014"><span class="cls_014">count </span><span class="cls_012">= 0</span></div>
<div style="position:absolute;left:84.00px;top:387.60px" class="cls_013"><span class="cls_013">for </span><span class="cls_014">line </span><span class="cls_013">in</span><span class="cls_014"> fhand</span><span class="cls_012">:</span></div>
<div style="position:absolute;left:126.95px;top:409.68px" class="cls_013"><span class="cls_013">if </span><span class="cls_014">line</span><span class="cls_010">.startswith</span><span class="cls_012">('Subject:') :</span></div>
<div style="position:absolute;left:169.90px;top:430.56px" class="cls_014"><span class="cls_014">count </span><span class="cls_012">= </span><span class="cls_014">count </span><span class="cls_012">+ 1</span></div>
<div style="position:absolute;left:84.00px;top:452.64px" class="cls_013"><span class="cls_013">print</span><span class="cls_012">('There were', </span><span class="cls_014">count</span><span class="cls_012">, 'subject lines in', fname)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:234.60px;top:16.56px" class="cls_009"><span class="cls_009">Writing to files</span></div>
<div style="position:absolute;left:136.61px;top:108.96px" class="cls_012"><span class="cls_012">x = ‘example.txt’</span></div>
<div style="position:absolute;left:136.61px;top:130.08px" class="cls_012"><span class="cls_012">filehandle = open(x,’w’)</span></div>
<div style="position:absolute;left:136.61px;top:152.16px" class="cls_012"><span class="cls_012">Line1 = “INSY5336 Intro to Python”</span></div>
<div style="position:absolute;left:136.61px;top:173.04px" class="cls_012"><span class="cls_012">filehandle.write(line1)</span></div>
<div style="position:absolute;left:136.61px;top:195.12px" class="cls_012"><span class="cls_012">filehandle.close()</span></div>
<div style="position:absolute;left:30.67px;top:276.00px" class="cls_019"><span class="cls_019">• write() method writes the line to a open,</span></div>
<div style="position:absolute;left:57.67px;top:307.92px" class="cls_019"><span class="cls_019">writeable file</span></div>
<div style="position:absolute;left:30.67px;top:347.04px" class="cls_019"><span class="cls_019">• close() should be used to close the file</span></div>
<div style="position:absolute;left:30.67px;top:387.12px" class="cls_019"><span class="cls_019">• The existing contents of the file will be lost if</span></div>
<div style="position:absolute;left:57.67px;top:419.04px" class="cls_019"><span class="cls_019">the file is opened in write mode</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:164.26px;top:16.56px" class="cls_009"><span class="cls_009">Inserting a line to a file</span></div>
<div style="position:absolute;left:40.97px;top:61.44px" class="cls_012"><span class="cls_012">x='marytext.txt'</span></div>
<div style="position:absolute;left:40.97px;top:82.32px" class="cls_012"><span class="cls_012">filehandle = open(x,'r')</span></div>
<div style="position:absolute;left:40.97px;top:104.40px" class="cls_012"><span class="cls_012">#read all the lines in the file into a list called lines</span></div>
<div style="position:absolute;left:40.97px;top:126.48px" class="cls_012"><span class="cls_012">lines = filehandle.readlines()</span></div>
<div style="position:absolute;left:40.97px;top:147.36px" class="cls_012"><span class="cls_012">filehandle.close()</span></div>
<div style="position:absolute;left:40.97px;top:169.44px" class="cls_012"><span class="cls_012">print(lines)</span></div>
<div style="position:absolute;left:40.97px;top:190.32px" class="cls_012"><span class="cls_012">#change line 5 in the list to the new text</span></div>
<div style="position:absolute;left:40.97px;top:212.40px" class="cls_012"><span class="cls_012">lines[5]='INSY5336\n'</span></div>
<div style="position:absolute;left:40.97px;top:234.48px" class="cls_012"><span class="cls_012">print(lines)</span></div>
<div style="position:absolute;left:40.97px;top:255.36px" class="cls_012"><span class="cls_012">filehandle = open(x,'w')</span></div>
<div style="position:absolute;left:40.97px;top:277.44px" class="cls_012"><span class="cls_012">#write all the lines into the file</span></div>
<div style="position:absolute;left:40.97px;top:298.32px" class="cls_012"><span class="cls_012">filehandle.writelines(lines)</span></div>
<div style="position:absolute;left:40.97px;top:320.40px" class="cls_012"><span class="cls_012">filehandle.close()</span></div>
<div style="position:absolute;left:30.67px;top:356.16px" class="cls_019"><span class="cls_019">• Read the file into a list</span></div>
<div style="position:absolute;left:30.67px;top:396.24px" class="cls_019"><span class="cls_019">• Update the list</span></div>
<div style="position:absolute;left:30.67px;top:436.08px" class="cls_019"><span class="cls_019">• Write the list back into the file</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.20px;top:16.56px" class="cls_009"><span class="cls_009">Summary</span></div>
<div style="position:absolute;left:27.77px;top:68.40px" class="cls_023"><span class="cls_023">§ File Processing</span></div>
<div style="position:absolute;left:27.77px;top:115.20px" class="cls_023"><span class="cls_023">§ Newline character</span></div>
<div style="position:absolute;left:27.77px;top:162.24px" class="cls_023"><span class="cls_023">§ File Handle</span></div>
<div style="position:absolute;left:27.77px;top:209.28px" class="cls_023"><span class="cls_023">§ Opening a file</span></div>
<div style="position:absolute;left:27.77px;top:256.32px" class="cls_023"><span class="cls_023">§ Counting lines in a file</span></div>
<div style="position:absolute;left:27.77px;top:303.36px" class="cls_023"><span class="cls_023">§ Read the whole file</span></div>
<div style="position:absolute;left:27.77px;top:350.40px" class="cls_023"><span class="cls_023">§ Searching & selecting through a file</span></div>
<div style="position:absolute;left:27.77px;top:397.20px" class="cls_023"><span class="cls_023">§ Robustness in file processing</span></div>
<div style="position:absolute;left:27.77px;top:444.24px" class="cls_023"><span class="cls_023">§ Writing & Inserting into files</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.50px;top:16.56px" class="cls_009"><span class="cls_009">To Do List for Next Class</span></div>
<div style="position:absolute;left:107.95px;top:96.16px" class="cls_024"><span class="cls_024">• Read Chapters 7 & 8</span></div>
<div style="position:absolute;left:107.95px;top:146.08px" class="cls_024"><span class="cls_024">• Execute code snippets and</span></div>
<div style="position:absolute;left:134.95px;top:192.16px" class="cls_024"><span class="cls_024">chapter exercises in the book</span></div>
<div style="position:absolute;left:107.95px;top:250.24px" class="cls_024"><span class="cls_024">• Start working on HW2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.21px;top:16.56px" class="cls_009"><span class="cls_009">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.31px;top:59.76px" class="cls_009"><span class="cls_009">tions</span></div>
<div style="position:absolute;left:107.95px;top:127.44px" class="cls_025"><span class="cls_025">§  These slides are Copyright 2010-  Charles R. Severance</span></div>
<div style="position:absolute;left:134.95px;top:148.56px" class="cls_025"><span class="cls_025">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.95px;top:170.40px" class="cls_025"><span class="cls_025">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.95px;top:191.52px" class="cls_025"><span class="cls_025">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.95px;top:213.36px" class="cls_025"><span class="cls_025">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.95px;top:235.44px" class="cls_025"><span class="cls_025">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.95px;top:256.56px" class="cls_025"><span class="cls_025">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.95px;top:278.40px" class="cls_025"><span class="cls_025">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.95px;top:299.52px" class="cls_025"><span class="cls_025">materials.</span></div>
<div style="position:absolute;left:107.95px;top:352.56px" class="cls_025"><span class="cls_025">§  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.90px;top:374.40px" class="cls_025"><span class="cls_025">Michigan School of Information</span></div>
<div style="position:absolute;left:107.95px;top:427.44px" class="cls_025"><span class="cls_025">§  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.90px;top:448.56px" class="cls_025"><span class="cls_025">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter7file/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>

</body>
</html>
