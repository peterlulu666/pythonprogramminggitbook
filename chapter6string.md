<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(223,101,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(223,101,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_017{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:40.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:40.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:27.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:27.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_040{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:24.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:24.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_041{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_041{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_042{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_042{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_037{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_043{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_043{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_044{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_039{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_039{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter6string/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.57px;top:185.68px" class="cls_002"><span class="cls_002">Chapter 6:</span></div>
<div style="position:absolute;left:44.57px;top:222.64px" class="cls_002"><span class="cls_002">Strings</span></div>
<div style="position:absolute;left:44.93px;top:353.52px" class="cls_003"><span class="cls_003">INSY 5336: Python Programming</span></div>
<div style="position:absolute;left:45.82px;top:408.72px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:45.82px;top:433.60px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:185.04px;top:222.40px" class="cls_007"><span class="cls_007">Chapter 6: Strings</span></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:180.60px;top:16.56px" class="cls_009"><span class="cls_009">The String Data Type</span></div>
<div style="position:absolute;left:7.20px;top:96.48px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:34.20px;top:96.48px" class="cls_008"><span class="cls_008">A string is a sequence </span><span class="cls_010">>>> </span><span class="cls_011">str1 = "Hello"</span></div>
<div style="position:absolute;left:336.12px;top:130.32px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_011">str2 = 'there'</span></div>
<div style="position:absolute;left:34.20px;top:128.88px" class="cls_008"><span class="cls_008">of characters</span></div>
<div style="position:absolute;left:336.12px;top:151.44px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">bob = str1 + str2</span></div>
<div style="position:absolute;left:336.12px;top:173.52px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">bob</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:7.20px;top:168.72px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:34.20px;top:168.72px" class="cls_008"><span class="cls_008">A string literal uses</span></div>
<div style="position:absolute;left:336.12px;top:195.36px" class="cls_012"><span class="cls_012">Hellothere</span></div>
<div style="position:absolute;left:34.20px;top:200.88px" class="cls_008"><span class="cls_008">quote (unlike int or</span></div>
<div style="position:absolute;left:336.12px;top:216.48px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">str3 = '123'</span></div>
<div style="position:absolute;left:336.12px;top:238.32px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">str3 = str3 + 1</span></div>
<div style="position:absolute;left:34.20px;top:232.80px" class="cls_008"><span class="cls_008">float). E.g. ‘Hello’ or</span></div>
<div style="position:absolute;left:336.12px;top:259.44px" class="cls_015"><span class="cls_015">Traceback (most recent call</span></div>
<div style="position:absolute;left:34.20px;top:265.92px" class="cls_008"><span class="cls_008">“World”</span></div>
<div style="position:absolute;left:336.12px;top:281.76px" class="cls_015"><span class="cls_015">last):  File "&lt;stdin>", line 1,</span></div>
<div style="position:absolute;left:336.12px;top:302.64px" class="cls_015"><span class="cls_015">in &lt;module></span></div>
<div style="position:absolute;left:7.20px;top:304.80px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:34.20px;top:304.80px" class="cls_008"><span class="cls_008">The + operator is</span></div>
<div style="position:absolute;left:336.12px;top:324.72px" class="cls_015"><span class="cls_015">TypeError: cannot concatenate</span></div>
<div style="position:absolute;left:336.12px;top:346.56px" class="cls_015"><span class="cls_015">'str' and 'int' objects</span></div>
<div style="position:absolute;left:34.20px;top:337.92px" class="cls_008"><span class="cls_008">interpreted as</span></div>
<div style="position:absolute;left:34.20px;top:369.84px" class="cls_008"><span class="cls_008">concatenate when</span></div>
<div style="position:absolute;left:34.20px;top:401.76px" class="cls_008"><span class="cls_008">used with strings</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:112.66px;top:16.56px" class="cls_009"><span class="cls_009">Type Conversion from String</span></div>
<div style="position:absolute;left:390.43px;top:102.00px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">name </span><span class="cls_010">=</span><span class="cls_011"> input</span><span class="cls_010">(</span><span class="cls_014">'Enter:'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:31.99px;top:99.60px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:58.99px;top:99.60px" class="cls_016"><span class="cls_016">input() </span><span class="cls_008">statement</span></div>
<div style="position:absolute;left:390.43px;top:123.12px" class="cls_010"><span class="cls_010">Enter:</span><span class="cls_012">Chuck</span></div>
<div style="position:absolute;left:58.99px;top:128.40px" class="cls_008"><span class="cls_008">accepts all input as</span></div>
<div style="position:absolute;left:390.43px;top:144.96px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_014">name</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:58.99px;top:157.44px" class="cls_008"><span class="cls_008">strings</span></div>
<div style="position:absolute;left:390.43px;top:167.04px" class="cls_010"><span class="cls_010">Chuck</span></div>
<div style="position:absolute;left:390.43px;top:187.92px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">apple </span><span class="cls_010">=</span><span class="cls_011"> input</span><span class="cls_010">(</span><span class="cls_014">'Enter:'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:31.99px;top:193.44px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:58.99px;top:193.44px" class="cls_008"><span class="cls_008">Numbers are also</span></div>
<div style="position:absolute;left:390.43px;top:210.00px" class="cls_010"><span class="cls_010">Enter:</span><span class="cls_012">100</span></div>
<div style="position:absolute;left:390.43px;top:231.12px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">x </span><span class="cls_010">= </span><span class="cls_012">apple </span><span class="cls_017">-</span><span class="cls_014"> 10</span></div>
<div style="position:absolute;left:58.99px;top:222.48px" class="cls_008"><span class="cls_008">strings</span></div>
<div style="position:absolute;left:390.43px;top:252.96px" class="cls_015"><span class="cls_015">Traceback (most recent call</span></div>
<div style="position:absolute;left:31.99px;top:258.48px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:58.99px;top:258.48px" class="cls_008"><span class="cls_008">Input strings are</span></div>
<div style="position:absolute;left:390.43px;top:275.04px" class="cls_015"><span class="cls_015">last):  File "&lt;stdin>", line</span></div>
<div style="position:absolute;left:390.43px;top:295.92px" class="cls_015"><span class="cls_015">1, in &lt;module></span></div>
<div style="position:absolute;left:58.99px;top:287.52px" class="cls_008"><span class="cls_008">parsed and converted</span></div>
<div style="position:absolute;left:390.43px;top:318.00px" class="cls_015"><span class="cls_015">TypeError: unsupported</span></div>
<div style="position:absolute;left:58.99px;top:316.56px" class="cls_008"><span class="cls_008">to other data types as</span></div>
<div style="position:absolute;left:390.43px;top:339.12px" class="cls_015"><span class="cls_015">operand type(s) for -: 'str'</span></div>
<div style="position:absolute;left:58.99px;top:345.60px" class="cls_008"><span class="cls_008">needed</span></div>
<div style="position:absolute;left:390.43px;top:360.96px" class="cls_015"><span class="cls_015">and 'int'</span></div>
<div style="position:absolute;left:390.43px;top:383.04px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">x </span><span class="cls_010">= </span><span class="cls_011">int</span><span class="cls_014">(</span><span class="cls_012">apple</span><span class="cls_014">) </span><span class="cls_017">-</span><span class="cls_014"> 10</span></div>
<div style="position:absolute;left:31.99px;top:381.60px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:58.99px;top:381.60px" class="cls_008"><span class="cls_008">This gives more control</span></div>
<div style="position:absolute;left:390.43px;top:403.92px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">x</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:58.99px;top:410.40px" class="cls_008"><span class="cls_008">over error situations</span></div>
<div style="position:absolute;left:390.43px;top:426.00px" class="cls_010"><span class="cls_010">90</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:198.34px;top:11.52px" class="cls_009"><span class="cls_009">Looking Inside Strings</span></div>
<div style="position:absolute;left:208.69px;top:94.40px" class="cls_018"><span class="cls_018">b</span></div>
<div style="position:absolute;left:267.69px;top:94.40px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:328.69px;top:94.40px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:387.69px;top:94.40px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:444.69px;top:94.40px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:503.74px;top:94.40px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:208.69px;top:152.24px" class="cls_019"><span class="cls_019">0</span></div>
<div style="position:absolute;left:267.69px;top:152.24px" class="cls_019"><span class="cls_019">1</span></div>
<div style="position:absolute;left:328.69px;top:152.24px" class="cls_019"><span class="cls_019">2</span></div>
<div style="position:absolute;left:387.69px;top:152.24px" class="cls_019"><span class="cls_019">3</span></div>
<div style="position:absolute;left:444.69px;top:152.24px" class="cls_019"><span class="cls_019">4</span></div>
<div style="position:absolute;left:503.74px;top:152.24px" class="cls_019"><span class="cls_019">5</span></div>
<div style="position:absolute;left:72.84px;top:244.32px" class="cls_008"><span class="cls_008">• We can access any single character in a</span></div>
<div style="position:absolute;left:99.84px;top:276.24px" class="cls_008"><span class="cls_008">string using square brackets [ ]</span></div>
<div style="position:absolute;left:72.84px;top:315.36px" class="cls_008"><span class="cls_008">• The index value must be an integer and</span></div>
<div style="position:absolute;left:99.84px;top:347.28px" class="cls_008"><span class="cls_008">starts with zero [0]</span></div>
<div style="position:absolute;left:72.84px;top:386.40px" class="cls_008"><span class="cls_008">• Any expression that results in an integer</span></div>
<div style="position:absolute;left:99.84px;top:418.32px" class="cls_008"><span class="cls_008">can be used as the index value</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:181.94px;top:11.52px" class="cls_009"><span class="cls_009">Looking Inside Strings</span></div>
<div style="position:absolute;left:208.70px;top:94.40px" class="cls_018"><span class="cls_018">b</span></div>
<div style="position:absolute;left:267.70px;top:94.40px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:328.70px;top:94.40px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:387.70px;top:94.40px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:444.70px;top:94.40px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:503.75px;top:94.40px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:208.70px;top:152.24px" class="cls_019"><span class="cls_019">0</span></div>
<div style="position:absolute;left:267.70px;top:152.24px" class="cls_019"><span class="cls_019">1</span></div>
<div style="position:absolute;left:328.70px;top:152.24px" class="cls_019"><span class="cls_019">2</span></div>
<div style="position:absolute;left:387.70px;top:152.24px" class="cls_019"><span class="cls_019">3</span></div>
<div style="position:absolute;left:444.70px;top:152.24px" class="cls_019"><span class="cls_019">4</span></div>
<div style="position:absolute;left:503.75px;top:152.24px" class="cls_019"><span class="cls_019">5</span></div>
<div style="position:absolute;left:176.75px;top:240.24px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">fruit </span><span class="cls_010">=</span><span class="cls_014"> 'banana'</span></div>
<div style="position:absolute;left:176.75px;top:262.08px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">letter </span><span class="cls_010">=</span><span class="cls_012"> fruit</span><span class="cls_017">[</span><span class="cls_014">1</span><span class="cls_017">]</span></div>
<div style="position:absolute;left:176.75px;top:283.20px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">letter</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:176.75px;top:305.28px" class="cls_010"><span class="cls_010">a</span></div>
<div style="position:absolute;left:176.75px;top:327.12px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">x </span><span class="cls_010">=</span><span class="cls_014"> 3</span></div>
<div style="position:absolute;left:176.75px;top:348.24px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">w </span><span class="cls_010">= </span><span class="cls_012">fruit</span><span class="cls_017">[</span><span class="cls_012">x </span><span class="cls_017">-</span><span class="cls_014"> 1</span><span class="cls_017">]</span></div>
<div style="position:absolute;left:176.75px;top:370.08px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">w</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:176.75px;top:391.20px" class="cls_010"><span class="cls_010">n</span></div>
<div style="position:absolute;left:83.52px;top:447.76px" class="cls_020"><span class="cls_020">• Can we access fruit[7] ?</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:275.40px;top:11.52px" class="cls_009"><span class="cls_009">String Length</span></div>
<div style="position:absolute;left:208.70px;top:99.20px" class="cls_018"><span class="cls_018">b</span></div>
<div style="position:absolute;left:267.70px;top:99.20px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:328.70px;top:99.20px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:387.70px;top:99.20px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:444.70px;top:99.20px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:503.75px;top:99.20px" class="cls_018"><span class="cls_018">a</span></div>
<div style="position:absolute;left:208.65px;top:157.28px" class="cls_019"><span class="cls_019">0</span></div>
<div style="position:absolute;left:267.70px;top:157.28px" class="cls_019"><span class="cls_019">1</span></div>
<div style="position:absolute;left:328.70px;top:157.28px" class="cls_019"><span class="cls_019">2</span></div>
<div style="position:absolute;left:387.70px;top:157.28px" class="cls_019"><span class="cls_019">3</span></div>
<div style="position:absolute;left:444.70px;top:157.28px" class="cls_019"><span class="cls_019">4</span></div>
<div style="position:absolute;left:33.50px;top:262.24px" class="cls_020"><span class="cls_020">• Built in function len()</span></div>
<div style="position:absolute;left:431.56px;top:288.96px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">fruit </span><span class="cls_010">=</span><span class="cls_014"> 'banana'</span></div>
<div style="position:absolute;left:431.56px;top:309.84px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">x </span><span class="cls_010">=</span><span class="cls_011"> len(</span><span class="cls_012">fruit</span><span class="cls_011">)</span></div>
<div style="position:absolute;left:60.50px;top:301.36px" class="cls_020"><span class="cls_020">gives the length of a</span></div>
<div style="position:absolute;left:431.56px;top:331.92px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">x</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:60.50px;top:339.28px" class="cls_020"><span class="cls_020">string as an integer</span></div>
<div style="position:absolute;left:431.56px;top:353.76px" class="cls_010"><span class="cls_010">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:156.34px;top:11.52px" class="cls_009"><span class="cls_009">Traversing through a String</span></div>
<div style="position:absolute;left:72.84px;top:87.76px" class="cls_020"><span class="cls_020">• Several data</span></div>
<div style="position:absolute;left:99.84px;top:126.88px" class="cls_020"><span class="cls_020">analytics</span></div>
<div style="position:absolute;left:421.72px;top:171.36px" class="cls_012"><span class="cls_012">fruit = 'banana'</span></div>
<div style="position:absolute;left:99.84px;top:164.80px" class="cls_020"><span class="cls_020">applications require</span></div>
<div style="position:absolute;left:421.72px;top:193.44px" class="cls_012"><span class="cls_012">index </span><span class="cls_010">=</span><span class="cls_014"> 0</span></div>
<div style="position:absolute;left:99.84px;top:202.72px" class="cls_020"><span class="cls_020">processing a string,</span></div>
<div style="position:absolute;left:421.72px;top:215.52px" class="cls_013"><span class="cls_013">while </span><span class="cls_012">index </span><span class="cls_010">&lt; </span><span class="cls_011">len</span><span class="cls_010">(</span><span class="cls_012">fruit</span><span class="cls_010">):</span></div>
<div style="position:absolute;left:464.82px;top:236.40px" class="cls_012"><span class="cls_012">letter </span><span class="cls_010">=</span><span class="cls_012"> fruit</span><span class="cls_017">[</span><span class="cls_012">index</span><span class="cls_017">]</span></div>
<div style="position:absolute;left:99.84px;top:241.84px" class="cls_020"><span class="cls_020">one character at a</span></div>
<div style="position:absolute;left:464.82px;top:258.48px" class="cls_013"><span class="cls_013">print</span><span class="cls_010">(index, </span><span class="cls_012">letter</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:464.82px;top:279.36px" class="cls_012"><span class="cls_012">index </span><span class="cls_010">= </span><span class="cls_012">index </span><span class="cls_017">+</span><span class="cls_014"> 1</span></div>
<div style="position:absolute;left:99.84px;top:279.76px" class="cls_020"><span class="cls_020">time</span></div>
<div style="position:absolute;left:72.84px;top:326.80px" class="cls_020"><span class="cls_020">• A </span><span class="cls_021">while </span><span class="cls_020">statement</span></div>
<div style="position:absolute;left:99.84px;top:364.72px" class="cls_020"><span class="cls_020">can be used to loop</span></div>
<div style="position:absolute;left:99.84px;top:402.88px" class="cls_020"><span class="cls_020">through a string</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:156.34px;top:11.52px" class="cls_009"><span class="cls_009">Traversing through a String</span></div>
<div style="position:absolute;left:53.18px;top:90.16px" class="cls_020"><span class="cls_020">• A </span><span class="cls_021">for </span><span class="cls_020">loop can be</span></div>
<div style="position:absolute;left:421.00px;top:131.76px" class="cls_013"><span class="cls_013">fruit = 'banana'</span></div>
<div style="position:absolute;left:80.18px;top:129.28px" class="cls_020"><span class="cls_020">used to traverse</span></div>
<div style="position:absolute;left:421.00px;top:152.64px" class="cls_013"><span class="cls_013">for </span><span class="cls_012">letter </span><span class="cls_013">in</span><span class="cls_012"> fruit</span><span class="cls_010">:</span></div>
<div style="position:absolute;left:464.10px;top:174.72px" class="cls_013"><span class="cls_013">print</span><span class="cls_010">(</span><span class="cls_012">letter</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:80.18px;top:167.20px" class="cls_020"><span class="cls_020">through a string</span></div>
<div style="position:absolute;left:53.18px;top:213.28px" class="cls_020"><span class="cls_020">• It is a more elegant</span></div>
<div style="position:absolute;left:80.18px;top:252.16px" class="cls_020"><span class="cls_020">way of traversing a</span></div>
<div style="position:absolute;left:80.18px;top:290.32px" class="cls_020"><span class="cls_020">string</span></div>
<div style="position:absolute;left:421.72px;top:310.08px" class="cls_012"><span class="cls_012">fruit = 'banana'</span></div>
<div style="position:absolute;left:421.72px;top:331.92px" class="cls_012"><span class="cls_012">index </span><span class="cls_010">=</span><span class="cls_014"> 0</span></div>
<div style="position:absolute;left:53.18px;top:337.36px" class="cls_020"><span class="cls_020">• The iteration variable</span></div>
<div style="position:absolute;left:421.72px;top:354.00px" class="cls_013"><span class="cls_013">while </span><span class="cls_012">index </span><span class="cls_010">&lt; </span><span class="cls_011">len</span><span class="cls_010">(</span><span class="cls_012">fruit</span><span class="cls_010">):</span></div>
<div style="position:absolute;left:464.82px;top:375.12px" class="cls_012"><span class="cls_012">letter </span><span class="cls_010">=</span><span class="cls_012"> fruit</span><span class="cls_017">[</span><span class="cls_012">index</span><span class="cls_017">]</span></div>
<div style="position:absolute;left:80.18px;top:375.28px" class="cls_020"><span class="cls_020">is use implicitly in a</span></div>
<div style="position:absolute;left:464.82px;top:396.96px" class="cls_013"><span class="cls_013">print</span><span class="cls_010">(index, </span><span class="cls_012">letter</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:464.82px;top:418.08px" class="cls_012"><span class="cls_012">index </span><span class="cls_010">= </span><span class="cls_012">index </span><span class="cls_017">+</span><span class="cls_014"> 1</span></div>
<div style="position:absolute;left:80.18px;top:413.20px" class="cls_020"><span class="cls_020">for loop</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:200.38px;top:11.52px" class="cls_009"><span class="cls_009">Looping and Counting</span></div>
<div style="position:absolute;left:49.08px;top:102.64px" class="cls_020"><span class="cls_020">• A simple loop to count</span></div>
<div style="position:absolute;left:76.08px;top:141.52px" class="cls_020"><span class="cls_020">the number of a</span></div>
<div style="position:absolute;left:422.99px;top:179.76px" class="cls_012"><span class="cls_012">word </span><span class="cls_010">=</span><span class="cls_014"> 'banana'</span></div>
<div style="position:absolute;left:76.08px;top:179.44px" class="cls_020"><span class="cls_020">certain character (a)</span></div>
<div style="position:absolute;left:422.99px;top:200.88px" class="cls_012"><span class="cls_012">count </span><span class="cls_010">=</span><span class="cls_014"> 0</span></div>
<div style="position:absolute;left:422.94px;top:222.96px" class="cls_013"><span class="cls_013">for </span><span class="cls_012">letter </span><span class="cls_013">in </span><span class="cls_012">word </span><span class="cls_010">:</span></div>
<div style="position:absolute;left:49.08px;top:225.52px" class="cls_020"><span class="cls_020">• You can execute any</span></div>
<div style="position:absolute;left:466.09px;top:244.80px" class="cls_013"><span class="cls_013">if </span><span class="cls_012">letter </span><span class="cls_017">== </span><span class="cls_014">'a'</span><span class="cls_010"> :</span></div>
<div style="position:absolute;left:498.39px;top:265.92px" class="cls_012"><span class="cls_012">count </span><span class="cls_010">= </span><span class="cls_012">count </span><span class="cls_017">+</span><span class="cls_014"> 1</span></div>
<div style="position:absolute;left:76.08px;top:264.64px" class="cls_020"><span class="cls_020">code block within the</span></div>
<div style="position:absolute;left:422.99px;top:287.76px" class="cls_013"><span class="cls_013">print</span><span class="cls_010">(</span><span class="cls_012">count</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:76.08px;top:302.56px" class="cls_020"><span class="cls_020">context of the current</span></div>
<div style="position:absolute;left:76.08px;top:341.44px" class="cls_020"><span class="cls_020">character</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:283.32px;top:11.52px" class="cls_009"><span class="cls_009">String Slices</span></div>
<div style="position:absolute;left:554.51px;top:101.12px" class="cls_018"><span class="cls_018">h</span></div>
<div style="position:absolute;left:611.51px;top:101.12px" class="cls_018"><span class="cls_018">o</span></div>
<div style="position:absolute;left:670.51px;top:101.12px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:19.89px;top:106.16px" class="cls_018"><span class="cls_018">M</span></div>
<div style="position:absolute;left:84.44px;top:106.16px" class="cls_018"><span class="cls_018">o</span></div>
<div style="position:absolute;left:145.44px;top:106.16px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:209.99px;top:106.16px" class="cls_018"><span class="cls_018">t</span></div>
<div style="position:absolute;left:262.54px;top:106.16px" class="cls_018"><span class="cls_018">y</span></div>
<div style="position:absolute;left:373.34px;top:106.16px" class="cls_018"><span class="cls_018">P</span></div>
<div style="position:absolute;left:435.59px;top:106.16px" class="cls_018"><span class="cls_018">y</span></div>
<div style="position:absolute;left:501.04px;top:106.16px" class="cls_018"><span class="cls_018">t</span></div>
<div style="position:absolute;left:25.46px;top:159.20px" class="cls_019"><span class="cls_019">0</span></div>
<div style="position:absolute;left:84.41px;top:159.20px" class="cls_019"><span class="cls_019">1</span></div>
<div style="position:absolute;left:145.41px;top:159.20px" class="cls_019"><span class="cls_019">2</span></div>
<div style="position:absolute;left:204.46px;top:159.20px" class="cls_019"><span class="cls_019">3</span></div>
<div style="position:absolute;left:261.46px;top:159.20px" class="cls_019"><span class="cls_019">4</span></div>
<div style="position:absolute;left:320.46px;top:159.20px" class="cls_019"><span class="cls_019">5</span></div>
<div style="position:absolute;left:375.46px;top:159.20px" class="cls_019"><span class="cls_019">6</span></div>
<div style="position:absolute;left:434.51px;top:159.20px" class="cls_019"><span class="cls_019">7</span></div>
<div style="position:absolute;left:495.51px;top:159.20px" class="cls_019"><span class="cls_019">8</span></div>
<div style="position:absolute;left:554.51px;top:159.20px" class="cls_019"><span class="cls_019">9 10 11</span></div>
<div style="position:absolute;left:72.81px;top:225.24px" class="cls_022"><span class="cls_022">• A segment of a string</span></div>
<div style="position:absolute;left:99.86px;top:254.28px" class="cls_022"><span class="cls_022">is called a </span><span class="cls_023">slice</span></div>
<div style="position:absolute;left:407.87px;top:273.36px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">s </span><span class="cls_010">= </span><span class="cls_014">'Monty Python'</span></div>
<div style="position:absolute;left:407.87px;top:294.24px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[</span><span class="cls_014">0</span><span class="cls_010">:</span><span class="cls_014">4</span><span class="cls_017">]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:72.81px;top:289.08px" class="cls_022"><span class="cls_022">• The : operator on</span></div>
<div style="position:absolute;left:407.87px;top:316.32px" class="cls_010"><span class="cls_010">Mont</span></div>
<div style="position:absolute;left:99.86px;top:318.12px" class="cls_022"><span class="cls_022">indices return a string</span></div>
<div style="position:absolute;left:407.87px;top:337.20px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[</span><span class="cls_014">6:7</span><span class="cls_017">]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:99.86px;top:347.16px" class="cls_022"><span class="cls_022">slice</span></div>
<div style="position:absolute;left:407.87px;top:359.28px" class="cls_010"><span class="cls_010">P</span></div>
<div style="position:absolute;left:407.87px;top:381.36px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[</span><span class="cls_014">6</span><span class="cls_017">:</span><span class="cls_014">20</span><span class="cls_017">]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:72.81px;top:383.16px" class="cls_022"><span class="cls_022">•</span></div>
<div style="position:absolute;left:99.86px;top:383.16px" class="cls_022"><span class="cls_022">[m:n] returns the m</span><span class="cls_024"><sup>th</sup></span></div>
<div style="position:absolute;left:407.87px;top:402.24px" class="cls_010"><span class="cls_010">Python</span></div>
<div style="position:absolute;left:99.86px;top:412.20px" class="cls_022"><span class="cls_022">to the n-1</span><span class="cls_024"><sup>th</sup></span><span class="cls_022"> character</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:235.30px;top:11.52px" class="cls_009"><span class="cls_009">More String Slices</span></div>
<div style="position:absolute;left:554.51px;top:101.12px" class="cls_018"><span class="cls_018">h</span></div>
<div style="position:absolute;left:611.51px;top:101.12px" class="cls_018"><span class="cls_018">o</span></div>
<div style="position:absolute;left:670.51px;top:101.12px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:19.89px;top:106.16px" class="cls_018"><span class="cls_018">M</span></div>
<div style="position:absolute;left:84.44px;top:106.16px" class="cls_018"><span class="cls_018">o</span></div>
<div style="position:absolute;left:145.44px;top:106.16px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:209.99px;top:106.16px" class="cls_018"><span class="cls_018">t</span></div>
<div style="position:absolute;left:262.56px;top:106.16px" class="cls_018"><span class="cls_018">y</span></div>
<div style="position:absolute;left:373.31px;top:106.16px" class="cls_018"><span class="cls_018">P</span></div>
<div style="position:absolute;left:435.56px;top:106.16px" class="cls_018"><span class="cls_018">y</span></div>
<div style="position:absolute;left:501.01px;top:106.16px" class="cls_018"><span class="cls_018">t</span></div>
<div style="position:absolute;left:25.46px;top:159.20px" class="cls_019"><span class="cls_019">0</span></div>
<div style="position:absolute;left:84.41px;top:159.20px" class="cls_019"><span class="cls_019">1</span></div>
<div style="position:absolute;left:145.41px;top:159.20px" class="cls_019"><span class="cls_019">2</span></div>
<div style="position:absolute;left:204.46px;top:159.20px" class="cls_019"><span class="cls_019">3</span></div>
<div style="position:absolute;left:261.46px;top:159.20px" class="cls_019"><span class="cls_019">4</span></div>
<div style="position:absolute;left:320.46px;top:159.20px" class="cls_019"><span class="cls_019">5</span></div>
<div style="position:absolute;left:375.46px;top:159.20px" class="cls_019"><span class="cls_019">6</span></div>
<div style="position:absolute;left:434.51px;top:159.20px" class="cls_019"><span class="cls_019">7</span></div>
<div style="position:absolute;left:495.51px;top:159.20px" class="cls_019"><span class="cls_019">8</span></div>
<div style="position:absolute;left:554.51px;top:159.20px" class="cls_019"><span class="cls_019">9 10 11</span></div>
<div style="position:absolute;left:35.76px;top:221.28px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:62.76px;top:221.28px" class="cls_008"><span class="cls_008">[m:n] returns the m</span><span class="cls_025"><sup>th </sup></span><span class="cls_008">to</span></div>
<div style="position:absolute;left:62.76px;top:250.08px" class="cls_008"><span class="cls_008">the n-1</span><span class="cls_025"><sup>th</sup></span><span class="cls_008"> character</span></div>
<div style="position:absolute;left:416.68px;top:263.52px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">s </span><span class="cls_010">= </span><span class="cls_014">'Monty Python'</span></div>
<div style="position:absolute;left:416.68px;top:284.64px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[:</span><span class="cls_014">2</span><span class="cls_017">]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:35.76px;top:286.08px" class="cls_008"><span class="cls_008">• If the value of m is left</span></div>
<div style="position:absolute;left:416.68px;top:306.72px" class="cls_010"><span class="cls_010">Mo</span></div>
<div style="position:absolute;left:62.76px;top:315.12px" class="cls_008"><span class="cls_008">off, it is assumed to be</span></div>
<div style="position:absolute;left:416.68px;top:328.56px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[</span><span class="cls_014">8</span><span class="cls_017">:]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:416.68px;top:349.68px" class="cls_010"><span class="cls_010">thon</span></div>
<div style="position:absolute;left:62.76px;top:343.20px" class="cls_008"><span class="cls_008">from the beginning</span></div>
<div style="position:absolute;left:416.68px;top:371.52px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[:]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:35.76px;top:379.20px" class="cls_008"><span class="cls_008">• If the value of n is left</span></div>
<div style="position:absolute;left:416.68px;top:392.64px" class="cls_010"><span class="cls_010">Monty Python</span></div>
<div style="position:absolute;left:62.76px;top:408.24px" class="cls_008"><span class="cls_008">off, it is assumed to be</span></div>
<div style="position:absolute;left:62.76px;top:437.28px" class="cls_008"><span class="cls_008">the end of the string</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:235.30px;top:11.52px" class="cls_009"><span class="cls_009">More String Slices</span></div>
<div style="position:absolute;left:19.93px;top:106.16px" class="cls_018"><span class="cls_018">M</span></div>
<div style="position:absolute;left:84.48px;top:106.16px" class="cls_018"><span class="cls_018">o</span></div>
<div style="position:absolute;left:145.48px;top:106.16px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:209.98px;top:106.16px" class="cls_018"><span class="cls_018">t</span></div>
<div style="position:absolute;left:262.58px;top:106.16px" class="cls_018"><span class="cls_018">y</span></div>
<div style="position:absolute;left:373.33px;top:106.16px" class="cls_018"><span class="cls_018">P</span></div>
<div style="position:absolute;left:435.58px;top:106.16px" class="cls_018"><span class="cls_018">y</span></div>
<div style="position:absolute;left:501.03px;top:106.16px" class="cls_018"><span class="cls_018">t</span></div>
<div style="position:absolute;left:554.53px;top:106.16px" class="cls_018"><span class="cls_018">h</span></div>
<div style="position:absolute;left:611.53px;top:106.16px" class="cls_018"><span class="cls_018">o</span></div>
<div style="position:absolute;left:670.58px;top:106.16px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:7.72px;top:164.24px" class="cls_019"><span class="cls_019">-12-11 -10 -9</span></div>
<div style="position:absolute;left:254.87px;top:164.24px" class="cls_019"><span class="cls_019">-8</span></div>
<div style="position:absolute;left:313.92px;top:164.24px" class="cls_019"><span class="cls_019">-7 -6</span></div>
<div style="position:absolute;left:427.92px;top:164.24px" class="cls_019"><span class="cls_019">-5</span></div>
<div style="position:absolute;left:488.92px;top:164.24px" class="cls_019"><span class="cls_019">-4</span></div>
<div style="position:absolute;left:547.92px;top:164.24px" class="cls_019"><span class="cls_019">-3 -2</span></div>
<div style="position:absolute;left:663.97px;top:164.24px" class="cls_019"><span class="cls_019">-1</span></div>
<div style="position:absolute;left:35.78px;top:227.92px" class="cls_020"><span class="cls_020">• Negative indices</span></div>
<div style="position:absolute;left:416.68px;top:263.52px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">s </span><span class="cls_010">= </span><span class="cls_014">'Monty Python'</span></div>
<div style="position:absolute;left:62.78px;top:267.04px" class="cls_020"><span class="cls_020">count spaces from</span></div>
<div style="position:absolute;left:416.68px;top:284.64px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[-12:</span><span class="cls_014">4</span><span class="cls_017">]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:416.68px;top:306.72px" class="cls_010"><span class="cls_010">Mont</span></div>
<div style="position:absolute;left:62.78px;top:304.96px" class="cls_020"><span class="cls_020">the right</span></div>
<div style="position:absolute;left:416.68px;top:328.56px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[</span><span class="cls_014">-1</span><span class="cls_017">:]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:416.68px;top:349.68px" class="cls_010"><span class="cls_010">n</span></div>
<div style="position:absolute;left:35.78px;top:351.04px" class="cls_020"><span class="cls_020">• One can mix and</span></div>
<div style="position:absolute;left:416.68px;top:371.52px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">s</span><span class="cls_017">[:-1]</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:416.68px;top:392.64px" class="cls_010"><span class="cls_010">Monty Pytho</span></div>
<div style="position:absolute;left:62.78px;top:389.92px" class="cls_020"><span class="cls_020">match negative and</span></div>
<div style="position:absolute;left:62.78px;top:428.08px" class="cls_020"><span class="cls_020">positive indices</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:170.26px;top:16.56px" class="cls_009"><span class="cls_009">Strings are Immutable</span></div>
<div style="position:absolute;left:81.82px;top:104.84px" class="cls_026"><span class="cls_026">§ Once assigned, the contents of a string variable</span></div>
<div style="position:absolute;left:108.82px;top:134.84px" class="cls_026"><span class="cls_026">cannot be changed</span></div>
<div style="position:absolute;left:81.82px;top:170.84px" class="cls_026"><span class="cls_026">§ The above is defined as an immutable type</span></div>
<div style="position:absolute;left:81.82px;top:206.84px" class="cls_026"><span class="cls_026">§ A partial list of Python types that are immutable:</span></div>
<div style="position:absolute;left:117.82px;top:241.76px" class="cls_027"><span class="cls_027">§ Int</span></div>
<div style="position:absolute;left:117.82px;top:273.92px" class="cls_027"><span class="cls_027">§ float</span></div>
<div style="position:absolute;left:117.82px;top:304.88px" class="cls_027"><span class="cls_027">§ string</span></div>
<div style="position:absolute;left:227.75px;top:378.48px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">greeting </span><span class="cls_010">= </span><span class="cls_014">'Hello World‘</span></div>
<div style="position:absolute;left:227.75px;top:400.56px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">greeting[0] =‘J’</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:157.54px;top:16.56px" class="cls_009"><span class="cls_009">Test for Immutable type</span></div>
<div style="position:absolute;left:81.82px;top:67.36px" class="cls_028"><span class="cls_028">§ The id() is a built in command in</span></div>
<div style="position:absolute;left:108.82px;top:113.44px" class="cls_028"><span class="cls_028">Python that gives the memory</span></div>
<div style="position:absolute;left:108.82px;top:159.52px" class="cls_028"><span class="cls_028">location of the variable</span></div>
<div style="position:absolute;left:81.82px;top:211.36px" class="cls_028"><span class="cls_028">x=5</span></div>
<div style="position:absolute;left:81.82px;top:264.40px" class="cls_028"><span class="cls_028">print(id(x))</span></div>
<div style="position:absolute;left:81.82px;top:318.40px" class="cls_028"><span class="cls_028">x=x+1</span></div>
<div style="position:absolute;left:81.82px;top:372.40px" class="cls_028"><span class="cls_028">print(id(x))</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:228.24px;top:16.56px" class="cls_009"><span class="cls_009">The in operator</span></div>
<div style="position:absolute;left:39.86px;top:66.72px" class="cls_029"><span class="cls_029">§ The </span><span class="cls_030">in </span><span class="cls_029">keyword can</span></div>
<div style="position:absolute;left:66.86px;top:110.64px" class="cls_029"><span class="cls_029">also be used to check</span></div>
<div style="position:absolute;left:401.21px;top:142.32px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">fruit </span><span class="cls_010">=</span><span class="cls_014"> 'banana'</span></div>
<div style="position:absolute;left:66.86px;top:153.84px" class="cls_029"><span class="cls_029">to see if one string is</span></div>
<div style="position:absolute;left:401.21px;top:163.44px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">'n' </span><span class="cls_013">in</span><span class="cls_012"> fruit</span></div>
<div style="position:absolute;left:401.21px;top:185.52px" class="cls_010"><span class="cls_010">True</span></div>
<div style="position:absolute;left:66.86px;top:196.80px" class="cls_029"><span class="cls_029">“in” another string</span></div>
<div style="position:absolute;left:401.21px;top:207.36px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">'m' </span><span class="cls_013">in</span><span class="cls_012"> fruit</span></div>
<div style="position:absolute;left:401.21px;top:228.48px" class="cls_010"><span class="cls_010">False</span></div>
<div style="position:absolute;left:401.21px;top:250.32px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">'nan' </span><span class="cls_013">in</span><span class="cls_012"> fruit</span></div>
<div style="position:absolute;left:39.86px;top:246.72px" class="cls_029"><span class="cls_029">§ The in expression is a</span></div>
<div style="position:absolute;left:401.21px;top:271.44px" class="cls_010"><span class="cls_010">True</span></div>
<div style="position:absolute;left:401.21px;top:293.52px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_013">if </span><span class="cls_014">'a' </span><span class="cls_013">in </span><span class="cls_012">fruit</span><span class="cls_010"> :</span></div>
<div style="position:absolute;left:66.86px;top:289.68px" class="cls_029"><span class="cls_029">logical expression that</span></div>
<div style="position:absolute;left:487.21px;top:315.36px" class="cls_013"><span class="cls_013">print</span><span class="cls_010">(</span><span class="cls_014">'Found it!'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:66.86px;top:333.84px" class="cls_029"><span class="cls_029">returns True or False</span></div>
<div style="position:absolute;left:401.21px;top:358.32px" class="cls_010"><span class="cls_010">Found it!</span></div>
<div style="position:absolute;left:401.21px;top:379.44px" class="cls_010"><span class="cls_010">>>></span></div>
<div style="position:absolute;left:66.86px;top:376.80px" class="cls_029"><span class="cls_029">and can be used in an</span></div>
<div style="position:absolute;left:66.86px;top:419.76px" class="cls_029"><span class="cls_029">if statement</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:247.20px;top:16.56px" class="cls_009"><span class="cls_009">String library</span></div>
<div style="position:absolute;left:3.31px;top:46.92px" class="cls_031"><span class="cls_031">§ Python has a number of</span></div>
<div style="position:absolute;left:30.31px;top:81.96px" class="cls_031"><span class="cls_031">string functions which are</span></div>
<div style="position:absolute;left:30.31px;top:117.96px" class="cls_031"><span class="cls_031">in the string library</span></div>
<div style="position:absolute;left:374.21px;top:146.64px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">greet </span><span class="cls_010">= </span><span class="cls_014">'Hello Bob'</span></div>
<div style="position:absolute;left:3.31px;top:160.92px" class="cls_031"><span class="cls_031">§ These functions are</span></div>
<div style="position:absolute;left:374.21px;top:167.76px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">zap </span><span class="cls_010">=</span><span class="cls_012"> greet</span><span class="cls_011">.lower</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:374.21px;top:189.60px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">zap</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:30.31px;top:195.96px" class="cls_031"><span class="cls_031">already built into every</span></div>
<div style="position:absolute;left:374.21px;top:211.68px" class="cls_010"><span class="cls_010">hello bob</span></div>
<div style="position:absolute;left:374.21px;top:232.80px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">greet)</span></div>
<div style="position:absolute;left:30.31px;top:231.96px" class="cls_031"><span class="cls_031">string - we invoke them by</span></div>
<div style="position:absolute;left:374.21px;top:254.64px" class="cls_010"><span class="cls_010">Hello Bob</span></div>
<div style="position:absolute;left:30.31px;top:267.96px" class="cls_031"><span class="cls_031">appending the function to</span></div>
<div style="position:absolute;left:374.21px;top:275.76px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_013">print</span><span class="cls_010">(</span><span class="cls_014">'Hi There'</span><span class="cls_011">.lower</span><span class="cls_010">())</span></div>
<div style="position:absolute;left:374.21px;top:297.60px" class="cls_010"><span class="cls_010">hi there</span></div>
<div style="position:absolute;left:30.31px;top:303.00px" class="cls_031"><span class="cls_031">the string variable</span></div>
<div style="position:absolute;left:374.21px;top:319.68px" class="cls_010"><span class="cls_010">>>></span></div>
<div style="position:absolute;left:3.31px;top:345.96px" class="cls_031"><span class="cls_031">§ These functions do not</span></div>
<div style="position:absolute;left:30.31px;top:381.00px" class="cls_031"><span class="cls_031">modify the original string,</span></div>
<div style="position:absolute;left:30.31px;top:417.00px" class="cls_031"><span class="cls_031">instead they return a new</span></div>
<div style="position:absolute;left:30.31px;top:453.00px" class="cls_031"><span class="cls_031">string that has been altered</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:158.16px;top:16.56px" class="cls_009"><span class="cls_009">Common String Library</span></div>
<div style="position:absolute;left:278.21px;top:59.76px" class="cls_009"><span class="cls_009">functions</span></div>
<div style="position:absolute;left:196.92px;top:119.52px" class="cls_011"><span class="cls_011">str.replace</span><span class="cls_010">(old, new[, count])</span></div>
<div style="position:absolute;left:196.92px;top:144.48px" class="cls_011"><span class="cls_011">str.lower</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:196.92px;top:169.44px" class="cls_011"><span class="cls_011">str.rstrip</span><span class="cls_010">([chars])</span></div>
<div style="position:absolute;left:196.92px;top:194.40px" class="cls_011"><span class="cls_011">str.strip</span><span class="cls_010">([chars])</span></div>
<div style="position:absolute;left:196.92px;top:219.36px" class="cls_011"><span class="cls_011">str.upper</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:159.72px;top:308.40px" class="cls_011"><span class="cls_011">str.capitalize</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:159.72px;top:333.36px" class="cls_011"><span class="cls_011">str.center</span><span class="cls_010">(width[, fillchar])</span></div>
<div style="position:absolute;left:159.72px;top:358.56px" class="cls_011"><span class="cls_011">str.endswith</span><span class="cls_010">(suffix[, start[, end]])</span></div>
<div style="position:absolute;left:159.72px;top:383.52px" class="cls_011"><span class="cls_011">str.find</span><span class="cls_010">(sub[, start[, end]])</span></div>
<div style="position:absolute;left:159.72px;top:408.48px" class="cls_011"><span class="cls_011">str.lstrip</span><span class="cls_010">([chars])</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:100.25px;top:20.16px" class="cls_009"><span class="cls_009">String Methods Documentation</span></div>
<div style="position:absolute;left:29.40px;top:82.80px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">stuff </span><span class="cls_010">= </span><span class="cls_014">'Hello world'</span></div>
<div style="position:absolute;left:29.40px;top:103.68px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> type</span><span class="cls_010">(</span><span class="cls_012">stuff</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:29.40px;top:125.76px" class="cls_010"><span class="cls_010">&lt;class 'str'></span></div>
<div style="position:absolute;left:29.40px;top:147.60px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> dir</span><span class="cls_010">(</span><span class="cls_012">stuff</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:29.40px;top:168.72px" class="cls_010"><span class="cls_010">['capitalize', 'casefold', 'center', 'count', 'encode',</span></div>
<div style="position:absolute;left:29.40px;top:190.80px" class="cls_010"><span class="cls_010">'endswith', 'expandtabs', 'find', 'format', 'format_map',</span></div>
<div style="position:absolute;left:29.40px;top:211.68px" class="cls_010"><span class="cls_010">'index', 'isalnum', 'isalpha', 'isdecimal', 'isdigit',</span></div>
<div style="position:absolute;left:29.40px;top:233.76px" class="cls_010"><span class="cls_010">'isidentifier', 'islower', 'isnumeric', 'isprintable',</span></div>
<div style="position:absolute;left:29.40px;top:255.60px" class="cls_010"><span class="cls_010">'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower',</span></div>
<div style="position:absolute;left:29.40px;top:276.72px" class="cls_010"><span class="cls_010">'lstrip', 'maketrans', 'partition', 'replace', 'rfind',</span></div>
<div style="position:absolute;left:29.40px;top:298.80px" class="cls_010"><span class="cls_010">'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split',</span></div>
<div style="position:absolute;left:29.40px;top:319.68px" class="cls_010"><span class="cls_010">'splitlines', 'startswith', 'strip', 'swapcase', 'title',</span></div>
<div style="position:absolute;left:29.40px;top:341.76px" class="cls_010"><span class="cls_010">'translate', 'upper', 'zfill']</span></div>
<div style="position:absolute;left:187.43px;top:408.96px" class="cls_040"><span class="cls_040"> </span><A HREF="https://docs.python.org/2/library/stdtypes.html">https://docs.python.org/3/library/stdtypes.html</A> </div>
<div style="position:absolute;left:307.25px;top:429.84px" class="cls_033"><span class="cls_033">String methods</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:200.28px;top:16.56px" class="cls_009"><span class="cls_009">Searching a String</span></div>
<div style="position:absolute;left:350.41px;top:89.84px" class="cls_019"><span class="cls_019">b</span></div>
<div style="position:absolute;left:409.40px;top:89.84px" class="cls_019"><span class="cls_019">a</span></div>
<div style="position:absolute;left:470.41px;top:89.84px" class="cls_019"><span class="cls_019">n</span></div>
<div style="position:absolute;left:529.40px;top:89.84px" class="cls_019"><span class="cls_019">a</span></div>
<div style="position:absolute;left:586.45px;top:89.84px" class="cls_019"><span class="cls_019">n</span></div>
<div style="position:absolute;left:645.44px;top:89.84px" class="cls_019"><span class="cls_019">a</span></div>
<div style="position:absolute;left:18.17px;top:116.80px" class="cls_028"><span class="cls_028">§ find() finds the first</span></div>
<div style="position:absolute;left:350.40px;top:147.68px" class="cls_019"><span class="cls_019">0</span></div>
<div style="position:absolute;left:409.39px;top:147.68px" class="cls_019"><span class="cls_019">1</span></div>
<div style="position:absolute;left:470.45px;top:147.68px" class="cls_019"><span class="cls_019">2</span></div>
<div style="position:absolute;left:529.44px;top:147.68px" class="cls_019"><span class="cls_019">3</span></div>
<div style="position:absolute;left:586.44px;top:147.68px" class="cls_019"><span class="cls_019">4</span></div>
<div style="position:absolute;left:645.43px;top:147.68px" class="cls_019"><span class="cls_019">5</span></div>
<div style="position:absolute;left:45.17px;top:162.88px" class="cls_028"><span class="cls_028">occurrence of the</span></div>
<div style="position:absolute;left:45.17px;top:208.96px" class="cls_028"><span class="cls_028">substring</span></div>
<div style="position:absolute;left:387.29px;top:246.48px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">fruit </span><span class="cls_010">=</span><span class="cls_014"> 'banana'</span></div>
<div style="position:absolute;left:387.29px;top:267.36px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">pos </span><span class="cls_010">=</span><span class="cls_012"> fruit</span><span class="cls_011">.find</span><span class="cls_010">(</span><span class="cls_014">'na'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:18.17px;top:262.96px" class="cls_028"><span class="cls_028">§ If the substring is not</span></div>
<div style="position:absolute;left:387.29px;top:289.44px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">pos</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:387.29px;top:311.52px" class="cls_010"><span class="cls_010">2</span></div>
<div style="position:absolute;left:45.17px;top:308.80px" class="cls_028"><span class="cls_028">found, find() returns a</span></div>
<div style="position:absolute;left:387.29px;top:332.40px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">aa </span><span class="cls_010">=</span><span class="cls_012"> fruit</span><span class="cls_011">.find</span><span class="cls_010">(</span><span class="cls_014">'z'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:387.29px;top:354.48px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">aa</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:45.17px;top:358.96px" class="cls_028"><span class="cls_028">-1</span></div>
<div style="position:absolute;left:387.29px;top:375.36px" class="cls_010"><span class="cls_010">-1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:64.97px;top:5.76px" class="cls_009"><span class="cls_009">Changing to Upper or Lower Case</span></div>
<div style="position:absolute;left:23.38px;top:65.76px" class="cls_029"><span class="cls_029">§ You can make a copy</span></div>
<div style="position:absolute;left:50.38px;top:104.64px" class="cls_029"><span class="cls_029">of a string in lower</span></div>
<div style="position:absolute;left:50.38px;top:144.72px" class="cls_029"><span class="cls_029">case or upper case</span></div>
<div style="position:absolute;left:402.65px;top:165.12px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">greet </span><span class="cls_010">= </span><span class="cls_014">'Hello Bob'</span></div>
<div style="position:absolute;left:402.65px;top:186.00px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">nnn </span><span class="cls_010">=</span><span class="cls_012"> greet</span><span class="cls_011">.upper</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:23.38px;top:191.76px" class="cls_029"><span class="cls_029">§ Often when we are</span></div>
<div style="position:absolute;left:402.65px;top:208.08px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">nnn</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:402.65px;top:230.16px" class="cls_010"><span class="cls_010">HELLO BOB</span></div>
<div style="position:absolute;left:50.38px;top:230.64px" class="cls_029"><span class="cls_029">searching for a string</span></div>
<div style="position:absolute;left:402.65px;top:251.04px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">www </span><span class="cls_010">=</span><span class="cls_012"> greet</span><span class="cls_011">.lower</span><span class="cls_013">()</span></div>
<div style="position:absolute;left:402.65px;top:273.12px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_012">www</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:50.38px;top:270.72px" class="cls_029"><span class="cls_029">using find() we first</span></div>
<div style="position:absolute;left:402.65px;top:294.00px" class="cls_010"><span class="cls_010">hello bob</span></div>
<div style="position:absolute;left:402.65px;top:316.08px" class="cls_010"><span class="cls_010">>>></span></div>
<div style="position:absolute;left:50.38px;top:309.60px" class="cls_029"><span class="cls_029">convert the string to</span></div>
<div style="position:absolute;left:50.38px;top:349.68px" class="cls_029"><span class="cls_029">lower case so we can</span></div>
<div style="position:absolute;left:50.38px;top:389.76px" class="cls_029"><span class="cls_029">search a string</span></div>
<div style="position:absolute;left:50.38px;top:428.64px" class="cls_029"><span class="cls_029">regardless of case</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:188.16px;top:16.56px" class="cls_009"><span class="cls_009">Search and Replace</span></div>
<div style="position:absolute;left:35.14px;top:84.88px" class="cls_028"><span class="cls_028">§ The replace() function is like a “search and</span></div>
<div style="position:absolute;left:62.14px;top:130.96px" class="cls_028"><span class="cls_028">replace” operation in a word processor</span></div>
<div style="position:absolute;left:35.14px;top:184.96px" class="cls_028"><span class="cls_028">§ It replaces all occurrences of the search</span></div>
<div style="position:absolute;left:62.14px;top:231.04px" class="cls_028"><span class="cls_028">string with the replacement string</span></div>
<div style="position:absolute;left:152.69px;top:299.04px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">greet = 'Hello</span><span class="cls_012"> Bob</span><span class="cls_014">'</span></div>
<div style="position:absolute;left:152.69px;top:320.16px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">nstr = greet.</span><span class="cls_011">replace</span><span class="cls_014">(</span><span class="cls_012">'Bob'</span><span class="cls_014">,</span><span class="cls_017">'Jane'</span><span class="cls_014">)</span></div>
<div style="position:absolute;left:152.69px;top:342.24px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_014">nstr</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:152.69px;top:364.08px" class="cls_010"><span class="cls_010">Hello</span><span class="cls_017"> Jane</span></div>
<div style="position:absolute;left:152.69px;top:385.20px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_014">nstr = greet.</span><span class="cls_011">replace</span><span class="cls_014">(</span><span class="cls_012">'o'</span><span class="cls_014">,</span><span class="cls_017">'X'</span><span class="cls_014">)</span></div>
<div style="position:absolute;left:152.69px;top:407.04px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print</span><span class="cls_010">(</span><span class="cls_014">nstr</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:152.69px;top:428.16px" class="cls_010"><span class="cls_010">Hell</span><span class="cls_017">X</span><span class="cls_010"> B</span><span class="cls_017">X</span><span class="cls_010">b</span></div>
<div style="position:absolute;left:152.69px;top:450.24px" class="cls_010"><span class="cls_010">>>></span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:163.18px;top:16.56px" class="cls_009"><span class="cls_009">Stripping white spaces</span></div>
<div style="position:absolute;left:50.95px;top:76.44px" class="cls_031"><span class="cls_031">§ Sometimes we want to</span></div>
<div style="position:absolute;left:77.95px;top:111.48px" class="cls_031"><span class="cls_031">take a string and</span></div>
<div style="position:absolute;left:77.95px;top:147.48px" class="cls_031"><span class="cls_031">remove whitespace at</span></div>
<div style="position:absolute;left:378.62px;top:174.24px" class="cls_010"><span class="cls_010">>>></span><span class="cls_012"> greet</span><span class="cls_010"> =</span><span class="cls_014"> '</span></div>
<div style="position:absolute;left:550.57px;top:174.24px" class="cls_014"><span class="cls_014">Hello Bob  '</span></div>
<div style="position:absolute;left:77.95px;top:183.48px" class="cls_031"><span class="cls_031">the beginning and/or</span></div>
<div style="position:absolute;left:378.62px;top:195.36px" class="cls_010"><span class="cls_010">>>></span><span class="cls_012"> greet</span><span class="cls_011">.lstrip</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:378.62px;top:217.44px" class="cls_010"><span class="cls_010">'Hello Bob  '</span></div>
<div style="position:absolute;left:77.95px;top:218.28px" class="cls_031"><span class="cls_031">end</span></div>
<div style="position:absolute;left:378.62px;top:239.28px" class="cls_010"><span class="cls_010">>>></span><span class="cls_012"> greet</span><span class="cls_011">.rstrip</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:378.62px;top:260.40px" class="cls_010"><span class="cls_010">'</span></div>
<div style="position:absolute;left:421.67px;top:260.40px" class="cls_010"><span class="cls_010">Hello Bob'</span></div>
<div style="position:absolute;left:50.95px;top:261.48px" class="cls_031"><span class="cls_031">§ lstrip() and rstrip()</span></div>
<div style="position:absolute;left:378.62px;top:282.24px" class="cls_010"><span class="cls_010">>>></span><span class="cls_012"> greet</span><span class="cls_011">.strip</span><span class="cls_010">()</span></div>
<div style="position:absolute;left:378.62px;top:303.36px" class="cls_010"><span class="cls_010">'Hello Bob'</span></div>
<div style="position:absolute;left:77.95px;top:297.48px" class="cls_031"><span class="cls_031">remove whitespace at</span></div>
<div style="position:absolute;left:378.62px;top:325.44px" class="cls_010"><span class="cls_010">>>></span></div>
<div style="position:absolute;left:77.95px;top:332.28px" class="cls_031"><span class="cls_031">the left or right</span></div>
<div style="position:absolute;left:50.95px;top:375.48px" class="cls_031"><span class="cls_031">§ strip() removes both</span></div>
<div style="position:absolute;left:77.95px;top:410.28px" class="cls_031"><span class="cls_031">beginning and ending</span></div>
<div style="position:absolute;left:77.95px;top:446.28px" class="cls_031"><span class="cls_031">whitespace</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:289.20px;top:16.56px" class="cls_009"><span class="cls_009">Prefixes</span></div>
<div style="position:absolute;left:81.82px;top:67.36px" class="cls_028"><span class="cls_028">§ startswith() method checks for the</span></div>
<div style="position:absolute;left:108.82px;top:113.44px" class="cls_028"><span class="cls_028">word till the first space</span></div>
<div style="position:absolute;left:81.82px;top:167.44px" class="cls_028"><span class="cls_028">§ Returns a Boolean type (True or</span></div>
<div style="position:absolute;left:108.82px;top:213.28px" class="cls_028"><span class="cls_028">False)</span></div>
<div style="position:absolute;left:158.16px;top:335.76px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">line </span><span class="cls_010">= </span><span class="cls_014">'Please have a nice day'</span></div>
<div style="position:absolute;left:158.16px;top:357.84px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">line</span><span class="cls_011">.startswith</span><span class="cls_010">(</span><span class="cls_014">'Please'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:158.16px;top:378.72px" class="cls_010"><span class="cls_010">True</span></div>
<div style="position:absolute;left:158.16px;top:400.80px" class="cls_010"><span class="cls_010">>>></span><span class="cls_012"> line</span><span class="cls_011">.startswith</span><span class="cls_010">(</span><span class="cls_014">'p'</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:158.16px;top:421.92px" class="cls_010"><span class="cls_010">False</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:163.18px;top:16.56px" class="cls_009"><span class="cls_009">Parsing and Extracting</span></div>
<div style="position:absolute;left:389.86px;top:89.52px" class="cls_034"><span class="cls_034">31</span></div>
<div style="position:absolute;left:261.89px;top:92.64px" class="cls_034"><span class="cls_034">21</span></div>
<div style="position:absolute;left:22.68px;top:175.60px" class="cls_035"><span class="cls_035">From </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span><span class="cls_042"> </span><span class="cls_035">Sat Jan</span></div>
<div style="position:absolute;left:514.73px;top:175.60px" class="cls_035"><span class="cls_035">5 09:14:16 2008</span></div>
<div style="position:absolute;left:35.28px;top:242.64px" class="cls_010"><span class="cls_010">>>></span><span class="cls_012"> data</span><span class="cls_010"> =</span><span class="cls_014"> 'From</span><span class="cls_037"> </span><A HREF="mailto:stephen.marquard@uct.ac.za">stephen.marquard@uct.ac.za</A> </span><span class="cls_044"> </span><span class="cls_014">Sat Jan</span></div>
<div style="position:absolute;left:604.98px;top:242.64px" class="cls_014"><span class="cls_014">5</span></div>
<div style="position:absolute;left:35.28px;top:264.72px" class="cls_014"><span class="cls_014">09:14:16 2008'</span></div>
<div style="position:absolute;left:35.28px;top:285.60px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">atpos </span><span class="cls_010">=</span><span class="cls_012"> data</span><span class="cls_011">.find</span><span class="cls_010">(</span><span class="cls_014">'@</span><span class="cls_010">')</span></div>
<div style="position:absolute;left:35.28px;top:307.68px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print(</span><span class="cls_012">atpos</span><span class="cls_013">)</span></div>
<div style="position:absolute;left:35.28px;top:329.76px" class="cls_010"><span class="cls_010">21</span></div>
<div style="position:absolute;left:35.28px;top:350.64px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">sppos </span><span class="cls_010">= </span><span class="cls_012">data</span><span class="cls_011">.find</span><span class="cls_010">(</span><span class="cls_014">' '</span><span class="cls_010">,</span><span class="cls_012">atpos</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:35.28px;top:372.72px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print(</span><span class="cls_012">sppos</span><span class="cls_013">)</span></div>
<div style="position:absolute;left:35.28px;top:393.60px" class="cls_010"><span class="cls_010">31</span></div>
<div style="position:absolute;left:35.28px;top:415.68px" class="cls_010"><span class="cls_010">>>> </span><span class="cls_012">host </span><span class="cls_010">= </span><span class="cls_012">data</span><span class="cls_017">[</span><span class="cls_012">atpos</span><span class="cls_017">+</span><span class="cls_014">1 </span><span class="cls_017">:</span><span class="cls_012"> sppos</span><span class="cls_017">]</span></div>
<div style="position:absolute;left:35.28px;top:437.76px" class="cls_010"><span class="cls_010">>>></span><span class="cls_013"> print(</span><span class="cls_012">host</span><span class="cls_013">)</span></div>
<div style="position:absolute;left:35.28px;top:458.64px" class="cls_010"><span class="cls_010">uct.ac.za</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.20px;top:16.56px" class="cls_009"><span class="cls_009">Summary</span></div>
<div style="position:absolute;left:27.77px;top:71.20px" class="cls_028"><span class="cls_028">§ String data type</span></div>
<div style="position:absolute;left:27.77px;top:125.20px" class="cls_028"><span class="cls_028">§ Extracting characters from strings</span></div>
<div style="position:absolute;left:27.77px;top:178.00px" class="cls_028"><span class="cls_028">§ Looping through strings using the </span><span class="cls_038">while</span><span class="cls_028"> and</span></div>
<div style="position:absolute;left:54.77px;top:224.08px" class="cls_038"><span class="cls_038">for</span><span class="cls_028"> loops</span></div>
<div style="position:absolute;left:27.77px;top:278.08px" class="cls_028"><span class="cls_028">§ String Slices</span></div>
<div style="position:absolute;left:27.77px;top:331.12px" class="cls_028"><span class="cls_028">§ Immutable data types</span></div>
<div style="position:absolute;left:27.77px;top:385.12px" class="cls_028"><span class="cls_028">§ in operator</span></div>
<div style="position:absolute;left:27.77px;top:438.16px" class="cls_028"><span class="cls_028">§ String Library methods</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.50px;top:16.56px" class="cls_009"><span class="cls_009">To Do List for Next Class</span></div>
<div style="position:absolute;left:107.95px;top:96.16px" class="cls_028"><span class="cls_028">• Read Chapters 7</span></div>
<div style="position:absolute;left:107.95px;top:146.08px" class="cls_028"><span class="cls_028">• Execute code snippets and</span></div>
<div style="position:absolute;left:134.95px;top:192.16px" class="cls_028"><span class="cls_028">chapter exercises in the book</span></div>
<div style="position:absolute;left:107.95px;top:250.24px" class="cls_028"><span class="cls_028">• Start working on HW2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.21px;top:16.56px" class="cls_009"><span class="cls_009">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.31px;top:59.76px" class="cls_009"><span class="cls_009">tions</span></div>
<div style="position:absolute;left:107.95px;top:127.44px" class="cls_039"><span class="cls_039">§  These slides are Copyright 2010-  Charles R. Severance</span></div>
<div style="position:absolute;left:134.95px;top:148.56px" class="cls_039"><span class="cls_039">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.95px;top:170.40px" class="cls_039"><span class="cls_039">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.95px;top:191.52px" class="cls_039"><span class="cls_039">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.95px;top:213.36px" class="cls_039"><span class="cls_039">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.95px;top:235.44px" class="cls_039"><span class="cls_039">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.95px;top:256.56px" class="cls_039"><span class="cls_039">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.95px;top:278.40px" class="cls_039"><span class="cls_039">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.95px;top:299.52px" class="cls_039"><span class="cls_039">materials.</span></div>
<div style="position:absolute;left:107.95px;top:352.56px" class="cls_039"><span class="cls_039">§  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.90px;top:374.40px" class="cls_039"><span class="cls_039">Michigan School of Information</span></div>
<div style="position:absolute;left:107.95px;top:427.44px" class="cls_039"><span class="cls_039">§  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.90px;top:448.56px" class="cls_039"><span class="cls_039">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter6string/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>

</body>
</html>
