<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:20.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:20.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_026{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Courier New,serif;font-size:16.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_029{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_032{font-family:Courier New,serif;font-size:18.1px;color:rgb(227,107,8);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Courier New,serif;font-size:18.1px;color:rgb(227,107,8);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture5Conditional/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.57px;top:185.68px" class="cls_002"><span class="cls_002">Chapter 3:</span></div>
<div style="position:absolute;left:44.57px;top:222.64px" class="cls_002"><span class="cls_002">Conditional Execution</span></div>
<div style="position:absolute;left:44.93px;top:353.52px" class="cls_003"><span class="cls_003">INSY 5336: Python Programming</span></div>
<div style="position:absolute;left:44.57px;top:407.28px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:44.57px;top:432.16px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:60.48px;top:224.72px" class="cls_007"><span class="cls_007">Chapter 3: Conditional Execution</span></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:169.30px;top:16.56px" class="cls_008"><span class="cls_008">Conditional Execution</span></div>
<div style="position:absolute;left:93.58px;top:46.40px" class="cls_009"><span class="cls_009">x = 5</span></div>
<div style="position:absolute;left:362.50px;top:103.12px" class="cls_010"><span class="cls_010">• Some statements</span></div>
<div style="position:absolute;left:79.82px;top:131.36px" class="cls_009"><span class="cls_009">x &lt; 10 ?</span></div>
<div style="position:absolute;left:171.89px;top:148.64px" class="cls_009"><span class="cls_009">Yes</span></div>
<div style="position:absolute;left:389.50px;top:142.24px" class="cls_010"><span class="cls_010">are executed</span></div>
<div style="position:absolute;left:76.49px;top:176.24px" class="cls_009"><span class="cls_009">No</span></div>
<div style="position:absolute;left:389.50px;top:180.16px" class="cls_010"><span class="cls_010">based on certain</span></div>
<div style="position:absolute;left:189.63px;top:204.56px" class="cls_009"><span class="cls_009">print('Smaller')</span></div>
<div style="position:absolute;left:389.50px;top:218.08px" class="cls_010"><span class="cls_010">conditions</span></div>
<div style="position:absolute;left:362.50px;top:265.12px" class="cls_010"><span class="cls_010">• We use Boolean</span></div>
<div style="position:absolute;left:171.58px;top:288.80px" class="cls_009"><span class="cls_009">Yes</span></div>
<div style="position:absolute;left:79.99px;top:304.16px" class="cls_009"><span class="cls_009">x > 20 ?</span></div>
<div style="position:absolute;left:389.50px;top:303.04px" class="cls_010"><span class="cls_010">expressions to</span></div>
<div style="position:absolute;left:78.38px;top:349.04px" class="cls_009"><span class="cls_009">No</span></div>
<div style="position:absolute;left:197.04px;top:351.92px" class="cls_009"><span class="cls_009">print('Bigger')</span></div>
<div style="position:absolute;left:389.50px;top:342.16px" class="cls_010"><span class="cls_010">evaluate</span></div>
<div style="position:absolute;left:389.50px;top:380.08px" class="cls_010"><span class="cls_010">conditions</span></div>
<div style="position:absolute;left:54.23px;top:432.08px" class="cls_009"><span class="cls_009">print(’Finished')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:176.16px;top:16.56px" class="cls_008"><span class="cls_008">Boolean Expressions</span></div>
<div style="position:absolute;left:75.26px;top:127.92px" class="cls_011"><span class="cls_011">• A Boolean expression asks</span></div>
<div style="position:absolute;left:102.21px;top:163.92px" class="cls_011"><span class="cls_011">a questions and responds</span></div>
<div style="position:absolute;left:494.09px;top:180.72px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_013">5 == 5</span></div>
<div style="position:absolute;left:494.09px;top:202.56px" class="cls_012"><span class="cls_012">True</span></div>
<div style="position:absolute;left:102.21px;top:199.92px" class="cls_011"><span class="cls_011">with either True or False</span></div>
<div style="position:absolute;left:494.09px;top:223.68px" class="cls_012"><span class="cls_012">>>> </span><span class="cls_013">5 == 4</span></div>
<div style="position:absolute;left:494.09px;top:245.76px" class="cls_012"><span class="cls_012">False</span></div>
<div style="position:absolute;left:75.26px;top:242.88px" class="cls_011"><span class="cls_011">• E.g. 5 == 5 is True</span></div>
<div style="position:absolute;left:494.09px;top:266.64px" class="cls_012"><span class="cls_012">>>></span><span class="cls_013"> type(</span><span class="cls_014">True</span><span class="cls_013">)</span></div>
<div style="position:absolute;left:75.21px;top:285.84px" class="cls_011"><span class="cls_011">•</span></div>
<div style="position:absolute;left:102.21px;top:285.84px" class="cls_011"><span class="cls_011">==</span></div>
<div style="position:absolute;left:145.59px;top:285.84px" class="cls_011"><span class="cls_011">is called a comparison</span></div>
<div style="position:absolute;left:102.21px;top:321.84px" class="cls_011"><span class="cls_011">operator</span></div>
<div style="position:absolute;left:75.26px;top:364.80px" class="cls_011"><span class="cls_011">• True and False are special</span></div>
<div style="position:absolute;left:102.21px;top:400.80px" class="cls_011"><span class="cls_011">types and not strings</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:297.38px;top:16.56px" class="cls_008"><span class="cls_008">type bool</span></div>
<div style="position:absolute;left:91.06px;top:97.80px" class="cls_015"><span class="cls_015">• type bool has two values:</span></div>
<div style="position:absolute;left:127.06px;top:130.80px" class="cls_016"><span class="cls_016">- True</span></div>
<div style="position:absolute;left:127.06px;top:157.68px" class="cls_016"><span class="cls_016">- False</span></div>
<div style="position:absolute;left:91.06px;top:187.80px" class="cls_015"><span class="cls_015">• Python’s implementation of Boolean algebra</span></div>
<div style="position:absolute;left:118.06px;top:213.72px" class="cls_015"><span class="cls_015">is not “strict” i.e. there is not strict type</span></div>
<div style="position:absolute;left:118.06px;top:239.64px" class="cls_015"><span class="cls_015">checking</span></div>
<div style="position:absolute;left:91.06px;top:272.76px" class="cls_015"><span class="cls_015">• E.g. If we perform an arithmetic operation</span></div>
<div style="position:absolute;left:118.06px;top:298.68px" class="cls_015"><span class="cls_015">between an integer and type bool, there is</span></div>
<div style="position:absolute;left:118.06px;top:324.84px" class="cls_015"><span class="cls_015">an answer</span></div>
<div style="position:absolute;left:91.06px;top:356.76px" class="cls_015"><span class="cls_015">• What do the following do:</span></div>
<div style="position:absolute;left:127.06px;top:388.80px" class="cls_016"><span class="cls_016">True + 5</span></div>
<div style="position:absolute;left:127.06px;top:417.84px" class="cls_016"><span class="cls_016">False * 5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:181.20px;top:16.56px" class="cls_008"><span class="cls_008">Comparison Operators</span></div>
<div style="position:absolute;left:23.98px;top:102.12px" class="cls_015"><span class="cls_015">• Result of a</span></div>
<div style="position:absolute;left:366.25px;top:111.76px" class="cls_017"><span class="cls_017">Comparison</span></div>
<div style="position:absolute;left:556.38px;top:123.76px" class="cls_018"><span class="cls_018">Meaning</span></div>
<div style="position:absolute;left:380.93px;top:135.76px" class="cls_017"><span class="cls_017">Operator</span></div>
<div style="position:absolute;left:50.98px;top:131.16px" class="cls_015"><span class="cls_015">comparison operation</span></div>
<div style="position:absolute;left:50.98px;top:160.20px" class="cls_015"><span class="cls_015">is a type bool i.e.</span></div>
<div style="position:absolute;left:414.91px;top:171.12px" class="cls_019"><span class="cls_019">&lt;</span></div>
<div style="position:absolute;left:556.01px;top:171.12px" class="cls_020"><span class="cls_020">Less than</span></div>
<div style="position:absolute;left:50.98px;top:189.24px" class="cls_015"><span class="cls_015">True or False</span></div>
<div style="position:absolute;left:409.61px;top:206.88px" class="cls_019"><span class="cls_019">&lt;=</span></div>
<div style="position:absolute;left:510.23px;top:206.88px" class="cls_020"><span class="cls_020">Less than or Equal to</span></div>
<div style="position:absolute;left:23.98px;top:225.24px" class="cls_015"><span class="cls_015">• Comparison</span></div>
<div style="position:absolute;left:412.16px;top:242.88px" class="cls_019"><span class="cls_019">==</span></div>
<div style="position:absolute;left:561.91px;top:242.88px" class="cls_020"><span class="cls_020">Equal to</span></div>
<div style="position:absolute;left:50.98px;top:254.28px" class="cls_015"><span class="cls_015">operators are used in</span></div>
<div style="position:absolute;left:409.61px;top:278.88px" class="cls_019"><span class="cls_019">>=</span></div>
<div style="position:absolute;left:498.78px;top:278.88px" class="cls_020"><span class="cls_020">Greater than or Equal to</span></div>
<div style="position:absolute;left:50.98px;top:283.32px" class="cls_015"><span class="cls_015">Boolean Expressions</span></div>
<div style="position:absolute;left:414.91px;top:314.88px" class="cls_019"><span class="cls_019">></span></div>
<div style="position:absolute;left:544.58px;top:314.88px" class="cls_020"><span class="cls_020">Greater than</span></div>
<div style="position:absolute;left:23.98px;top:319.32px" class="cls_015"><span class="cls_015">• The variables are not</span></div>
<div style="position:absolute;left:412.61px;top:350.88px" class="cls_019"><span class="cls_019">!=</span></div>
<div style="position:absolute;left:556.43px;top:350.88px" class="cls_020"><span class="cls_020">Not equal</span></div>
<div style="position:absolute;left:50.98px;top:348.12px" class="cls_015"><span class="cls_015">changed as a result</span></div>
<div style="position:absolute;left:50.98px;top:377.16px" class="cls_015"><span class="cls_015">of execution</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:136.23px;top:16.56px" class="cls_008"><span class="cls_008">Combining expressions and</span></div>
<div style="position:absolute;left:206.13px;top:59.76px" class="cls_008"><span class="cls_008">Boolean expression</span></div>
<div style="position:absolute;left:61.39px;top:148.48px" class="cls_010"><span class="cls_010">• We can combine Boolean expressions</span></div>
<div style="position:absolute;left:88.39px;top:187.36px" class="cls_010"><span class="cls_010">into other expressions</span></div>
<div style="position:absolute;left:61.39px;top:233.44px" class="cls_010"><span class="cls_010">• What do the following do:</span></div>
<div style="position:absolute;left:97.39px;top:274.40px" class="cls_021"><span class="cls_021">x = ( 5==5)</span></div>
<div style="position:absolute;left:97.39px;top:315.44px" class="cls_021"><span class="cls_021">y = (x==4)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:234.86px;top:16.56px" class="cls_008"><span class="cls_008">Boolean Algebra</span></div>
<div style="position:absolute;left:133.39px;top:106.28px" class="cls_022"><span class="cls_022">AND Operation</span></div>
<div style="position:absolute;left:454.54px;top:106.28px" class="cls_022"><span class="cls_022">OR Operation</span></div>
<div style="position:absolute;left:97.71px;top:153.04px" class="cls_023"><span class="cls_023">Expression</span></div>
<div style="position:absolute;left:259.86px;top:153.04px" class="cls_023"><span class="cls_023">Result</span></div>
<div style="position:absolute;left:410.58px;top:153.04px" class="cls_023"><span class="cls_023">Expression</span></div>
<div style="position:absolute;left:572.73px;top:153.04px" class="cls_023"><span class="cls_023">Result</span></div>
<div style="position:absolute;left:89.56px;top:185.28px" class="cls_020"><span class="cls_020">True and True   True</span></div>
<div style="position:absolute;left:402.43px;top:185.28px" class="cls_020"><span class="cls_020">True or True</span></div>
<div style="position:absolute;left:541.28px;top:185.28px" class="cls_020"><span class="cls_020">True</span></div>
<div style="position:absolute;left:89.56px;top:214.08px" class="cls_020"><span class="cls_020">True and False  False</span></div>
<div style="position:absolute;left:402.43px;top:214.08px" class="cls_020"><span class="cls_020">True or False</span></div>
<div style="position:absolute;left:541.28px;top:214.08px" class="cls_020"><span class="cls_020">True</span></div>
<div style="position:absolute;left:89.56px;top:242.88px" class="cls_020"><span class="cls_020">False and True   False</span></div>
<div style="position:absolute;left:402.43px;top:242.88px" class="cls_020"><span class="cls_020">False or True</span></div>
<div style="position:absolute;left:541.28px;top:242.88px" class="cls_020"><span class="cls_020">True</span></div>
<div style="position:absolute;left:89.56px;top:271.68px" class="cls_020"><span class="cls_020">False and False  False</span></div>
<div style="position:absolute;left:402.43px;top:271.68px" class="cls_020"><span class="cls_020">False or False   False</span></div>
<div style="position:absolute;left:274.20px;top:340.00px" class="cls_024"><span class="cls_024">Complement Operation</span></div>
<div style="position:absolute;left:261.40px;top:376.48px" class="cls_023"><span class="cls_023">Expression</span></div>
<div style="position:absolute;left:423.55px;top:376.48px" class="cls_023"><span class="cls_023">Result</span></div>
<div style="position:absolute;left:253.25px;top:408.72px" class="cls_020"><span class="cls_020">not(True)</span></div>
<div style="position:absolute;left:392.10px;top:408.72px" class="cls_020"><span class="cls_020">False</span></div>
<div style="position:absolute;left:253.25px;top:437.52px" class="cls_020"><span class="cls_020">not(False)</span></div>
<div style="position:absolute;left:392.10px;top:437.52px" class="cls_020"><span class="cls_020">True</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:223.22px;top:16.56px" class="cls_008"><span class="cls_008">Logical Operators</span></div>
<div style="position:absolute;left:61.39px;top:96.40px" class="cls_010"><span class="cls_010">• Logical operators are used in Boolean</span></div>
<div style="position:absolute;left:88.39px;top:135.52px" class="cls_010"><span class="cls_010">expressions</span></div>
<div style="position:absolute;left:229.51px;top:257.92px" class="cls_017"><span class="cls_017">Logical</span></div>
<div style="position:absolute;left:378.31px;top:269.92px" class="cls_018"><span class="cls_018">Meaning</span></div>
<div style="position:absolute;left:221.91px;top:281.92px" class="cls_017"><span class="cls_017">Operator</span></div>
<div style="position:absolute;left:247.01px;top:317.28px" class="cls_019"><span class="cls_019">and</span></div>
<div style="position:absolute;left:363.08px;top:317.28px" class="cls_020"><span class="cls_020">Boolean AND</span></div>
<div style="position:absolute;left:253.36px;top:353.04px" class="cls_019"><span class="cls_019">or</span></div>
<div style="position:absolute;left:367.79px;top:353.04px" class="cls_020"><span class="cls_020">Boolean OR</span></div>
<div style="position:absolute;left:251.86px;top:389.04px" class="cls_019"><span class="cls_019">not</span></div>
<div style="position:absolute;left:362.26px;top:389.04px" class="cls_020"><span class="cls_020">Boolean NOT</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:169.30px;top:16.56px" class="cls_008"><span class="cls_008">Conditional Execution</span></div>
<div style="position:absolute;left:93.58px;top:46.40px" class="cls_009"><span class="cls_009">x = 5</span></div>
<div style="position:absolute;left:79.82px;top:131.36px" class="cls_009"><span class="cls_009">x &lt; 10 ?</span></div>
<div style="position:absolute;left:171.89px;top:148.64px" class="cls_009"><span class="cls_009">Yes</span></div>
<div style="position:absolute;left:371.98px;top:152.96px" class="cls_026"><span class="cls_026">x = 5</span></div>
<div style="position:absolute;left:371.93px;top:171.92px" class="cls_025"><span class="cls_025">if </span><span class="cls_026">x &lt; 10:</span></div>
<div style="position:absolute;left:76.49px;top:176.24px" class="cls_009"><span class="cls_009">No</span></div>
<div style="position:absolute;left:400.73px;top:191.84px" class="cls_025"><span class="cls_025">print(</span><span class="cls_026">'Smaller’</span><span class="cls_025">)</span></div>
<div style="position:absolute;left:189.63px;top:204.56px" class="cls_009"><span class="cls_009">print('Smaller')</span></div>
<div style="position:absolute;left:371.98px;top:210.80px" class="cls_025"><span class="cls_025">if </span><span class="cls_026">x > 20:</span></div>
<div style="position:absolute;left:439.03px;top:229.76px" class="cls_025"><span class="cls_025">t(</span><span class="cls_026">'Bigger’</span><span class="cls_025">)</span></div>
<div style="position:absolute;left:448.58px;top:248.96px" class="cls_026"><span class="cls_026">inished'</span><span class="cls_025">)</span></div>
<div style="position:absolute;left:173.74px;top:285.92px" class="cls_009"><span class="cls_009">Yes</span></div>
<div style="position:absolute;left:79.99px;top:302.72px" class="cls_009"><span class="cls_009">x > 20 ?</span></div>
<div style="position:absolute;left:78.38px;top:349.04px" class="cls_009"><span class="cls_009">No</span></div>
<div style="position:absolute;left:197.04px;top:351.92px" class="cls_009"><span class="cls_009">print('Bigger')</span></div>
<div style="position:absolute;left:366.29px;top:372.96px" class="cls_027"><span class="cls_027">Note the indentation in the code</span></div>
<div style="position:absolute;left:54.23px;top:432.08px" class="cls_009"><span class="cls_009">print(’Finished')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:280.34px;top:16.56px" class="cls_008"><span class="cls_008">Indentation</span></div>
<div style="position:absolute;left:61.39px;top:94.44px" class="cls_015"><span class="cls_015">• Increase indent after the </span><span class="cls_028">“:” </span><span class="cls_015">of an if statement</span></div>
<div style="position:absolute;left:88.39px;top:123.24px" class="cls_015"><span class="cls_015">or for statement</span></div>
<div style="position:absolute;left:61.39px;top:158.28px" class="cls_015"><span class="cls_015">• Maintain indent to indicate the scope of the</span></div>
<div style="position:absolute;left:88.39px;top:187.32px" class="cls_015"><span class="cls_015">block (which lines are affected by the if/for)</span></div>
<div style="position:absolute;left:61.39px;top:222.36px" class="cls_015"><span class="cls_015">• Reduce indent back to the level of the if</span></div>
<div style="position:absolute;left:88.39px;top:251.40px" class="cls_015"><span class="cls_015">statement or for statement to indicate the end</span></div>
<div style="position:absolute;left:88.39px;top:280.44px" class="cls_015"><span class="cls_015">of the block</span></div>
<div style="position:absolute;left:61.39px;top:315.24px" class="cls_015"><span class="cls_015">• Blank lines are ignored - they do not affect</span></div>
<div style="position:absolute;left:88.39px;top:344.28px" class="cls_015"><span class="cls_015">indentation</span></div>
<div style="position:absolute;left:61.39px;top:379.32px" class="cls_015"><span class="cls_015">• Comments on a line by themselves are</span></div>
<div style="position:absolute;left:88.39px;top:408.36px" class="cls_015"><span class="cls_015">ignored with regard to indentation</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:201.14px;top:16.56px" class="cls_008"><span class="cls_008">Nested if statements</span></div>
<div style="position:absolute;left:358.05px;top:115.92px" class="cls_012"><span class="cls_012">x = 42</span></div>
<div style="position:absolute;left:138.05px;top:122.16px" class="cls_027"><span class="cls_027">yes</span></div>
<div style="position:absolute;left:44.92px;top:132.48px" class="cls_027"><span class="cls_027">x > 1</span></div>
<div style="position:absolute;left:358.05px;top:138.00px" class="cls_029"><span class="cls_029">if x > 1 :</span></div>
<div style="position:absolute;left:37.42px;top:159.36px" class="cls_027"><span class="cls_027">no</span></div>
<div style="position:absolute;left:401.10px;top:158.88px" class="cls_029"><span class="cls_029">print('More than one')</span></div>
<div style="position:absolute;left:401.10px;top:180.96px" class="cls_014"><span class="cls_014">if x &lt; 100 :</span></div>
<div style="position:absolute;left:444.10px;top:201.84px" class="cls_014"><span class="cls_014">print('Less than 100')</span></div>
<div style="position:absolute;left:84.41px;top:205.92px" class="cls_027"><span class="cls_027">print('More than one’)</span></div>
<div style="position:absolute;left:358.05px;top:223.92px" class="cls_030"><span class="cls_030">print('All done')</span></div>
<div style="position:absolute;left:256.56px;top:279.36px" class="cls_027"><span class="cls_027">yes</span></div>
<div style="position:absolute;left:139.80px;top:295.68px" class="cls_027"><span class="cls_027">x &lt; 100</span></div>
<div style="position:absolute;left:399.89px;top:282.40px" class="cls_010"><span class="cls_010">• Nesting of code</span></div>
<div style="position:absolute;left:142.75px;top:336.24px" class="cls_027"><span class="cls_027">no</span></div>
<div style="position:absolute;left:426.89px;top:321.52px" class="cls_010"><span class="cls_010">blocks is achieved</span></div>
<div style="position:absolute;left:209.23px;top:351.36px" class="cls_027"><span class="cls_027">print('Less than 100')</span></div>
<div style="position:absolute;left:426.89px;top:359.44px" class="cls_010"><span class="cls_010">by indentation</span></div>
<div style="position:absolute;left:61.75px;top:399.36px" class="cls_027"><span class="cls_027">print('All Done')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:208.34px;top:16.56px" class="cls_008"><span class="cls_008">Alternate Execution</span></div>
<div style="position:absolute;left:409.13px;top:122.64px" class="cls_012"><span class="cls_012">x = 4</span></div>
<div style="position:absolute;left:409.13px;top:166.56px" class="cls_029"><span class="cls_029">if x > 2 :</span></div>
<div style="position:absolute;left:452.23px;top:187.68px" class="cls_029"><span class="cls_029">print('Bigger')</span></div>
<div style="position:absolute;left:112.37px;top:197.76px" class="cls_027"><span class="cls_027">no</span></div>
<div style="position:absolute;left:255.38px;top:195.60px" class="cls_027"><span class="cls_027">yes</span></div>
<div style="position:absolute;left:174.94px;top:208.56px" class="cls_027"><span class="cls_027">x > 2</span></div>
<div style="position:absolute;left:409.13px;top:209.52px" class="cls_014"><span class="cls_014">else :</span></div>
<div style="position:absolute;left:452.23px;top:230.64px" class="cls_014"><span class="cls_014">print('Smaller')</span></div>
<div style="position:absolute;left:40.12px;top:270.48px" class="cls_027"><span class="cls_027">print('Not bigger')</span></div>
<div style="position:absolute;left:241.21px;top:269.52px" class="cls_027"><span class="cls_027">print('Bigger')</span></div>
<div style="position:absolute;left:409.13px;top:274.56px" class="cls_030"><span class="cls_030">print('All done')</span></div>
<div style="position:absolute;left:133.84px;top:312.48px" class="cls_027"><span class="cls_027">print('All Done')</span></div>
<div style="position:absolute;left:374.47px;top:318.48px" class="cls_011"><span class="cls_011">• Same indentation used</span></div>
<div style="position:absolute;left:401.47px;top:347.52px" class="cls_011"><span class="cls_011">for different if and else</span></div>
<div style="position:absolute;left:401.47px;top:376.56px" class="cls_011"><span class="cls_011">blocks</span></div>
<div style="position:absolute;left:374.47px;top:412.56px" class="cls_011"><span class="cls_011">• Blank line does not</span></div>
<div style="position:absolute;left:401.47px;top:441.60px" class="cls_011"><span class="cls_011">impact indentation</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:192.36px;top:16.56px" class="cls_008"><span class="cls_008">Chained Conditionals</span></div>
<div style="position:absolute;left:409.13px;top:101.76px" class="cls_029"><span class="cls_029">if </span><span class="cls_012">x &lt; 2 :</span></div>
<div style="position:absolute;left:452.23px;top:123.60px" class="cls_029"><span class="cls_029">print(</span><span class="cls_012">'small'</span><span class="cls_029">)</span></div>
<div style="position:absolute;left:409.13px;top:144.72px" class="cls_029"><span class="cls_029">elif </span><span class="cls_012">x &lt; 10 :</span></div>
<div style="position:absolute;left:452.23px;top:166.80px" class="cls_029"><span class="cls_029">print(</span><span class="cls_012">'Medium'</span><span class="cls_029">)</span></div>
<div style="position:absolute;left:158.38px;top:174.48px" class="cls_027"><span class="cls_027">yes</span></div>
<div style="position:absolute;left:77.93px;top:187.44px" class="cls_027"><span class="cls_027">x &lt; 2</span></div>
<div style="position:absolute;left:409.13px;top:187.68px" class="cls_029"><span class="cls_029">else</span><span class="cls_012"> :</span></div>
<div style="position:absolute;left:452.23px;top:209.76px" class="cls_029"><span class="cls_029">print(</span><span class="cls_012">'LARGE'</span><span class="cls_029">)</span></div>
<div style="position:absolute;left:51.43px;top:228.72px" class="cls_027"><span class="cls_027">no</span></div>
<div style="position:absolute;left:151.56px;top:227.52px" class="cls_027"><span class="cls_027">print(’small')</span></div>
<div style="position:absolute;left:409.13px;top:231.60px" class="cls_029"><span class="cls_029">print</span><span class="cls_012">('All done'</span><span class="cls_029">)</span></div>
<div style="position:absolute;left:160.49px;top:257.76px" class="cls_027"><span class="cls_027">yes</span></div>
<div style="position:absolute;left:85.06px;top:262.08px" class="cls_027"><span class="cls_027">x &lt;</span></div>
<div style="position:absolute;left:87.46px;top:282.96px" class="cls_027"><span class="cls_027">10</span></div>
<div style="position:absolute;left:63.43px;top:320.40px" class="cls_027"><span class="cls_027">no</span></div>
<div style="position:absolute;left:137.21px;top:330.96px" class="cls_027"><span class="cls_027">print(’medium')</span></div>
<div style="position:absolute;left:347.42px;top:322.00px" class="cls_010"><span class="cls_010">• Multiple if statements</span></div>
<div style="position:absolute;left:51.12px;top:371.04px" class="cls_027"><span class="cls_027">print(’large')</span></div>
<div style="position:absolute;left:374.42px;top:360.88px" class="cls_010"><span class="cls_010">can be consolidated</span></div>
<div style="position:absolute;left:374.42px;top:399.04px" class="cls_010"><span class="cls_010">into an elif (else-if)</span></div>
<div style="position:absolute;left:36.20px;top:426.72px" class="cls_027"><span class="cls_027">print('All Done')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:123.24px;top:16.56px" class="cls_008"><span class="cls_008">How to deal with Exceptions?</span></div>
<div style="position:absolute;left:57.22px;top:109.12px" class="cls_010"><span class="cls_010">• Poor Error checking</span></div>
<div style="position:absolute;left:452.88px;top:122.16px" class="cls_013"><span class="cls_013">astr = 'Hello Bob'</span></div>
<div style="position:absolute;left:452.88px;top:143.28px" class="cls_013"><span class="cls_013">istr = int(astr)</span></div>
<div style="position:absolute;left:84.22px;top:148.00px" class="cls_010"><span class="cls_010">leads to robustness</span></div>
<div style="position:absolute;left:452.88px;top:165.12px" class="cls_013"><span class="cls_013">print('First', istr)</span></div>
<div style="position:absolute;left:452.88px;top:187.20px" class="cls_013"><span class="cls_013">astr = '123'</span></div>
<div style="position:absolute;left:84.22px;top:186.16px" class="cls_010"><span class="cls_010">problems</span></div>
<div style="position:absolute;left:452.88px;top:208.08px" class="cls_013"><span class="cls_013">istr = int(astr)</span></div>
<div style="position:absolute;left:452.88px;top:230.16px" class="cls_013"><span class="cls_013">print('Second', istr)</span></div>
<div style="position:absolute;left:57.22px;top:232.00px" class="cls_010"><span class="cls_010">• Invalid statements halt</span></div>
<div style="position:absolute;left:84.22px;top:271.12px" class="cls_010"><span class="cls_010">program execution</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:199.22px;top:16.56px" class="cls_008"><span class="cls_008">Catching Exceptions</span></div>
<div style="position:absolute;left:303.17px;top:78.00px" class="cls_011"><span class="cls_011">• When the first conversion</span></div>
<div style="position:absolute;left:36.12px;top:108.48px" class="cls_013"><span class="cls_013">astr = 'Hello Bob'</span></div>
<div style="position:absolute;left:330.17px;top:114.00px" class="cls_011"><span class="cls_011">fails - it just drops into</span></div>
<div style="position:absolute;left:36.12px;top:130.32px" class="cls_013"><span class="cls_013">try:</span></div>
<div style="position:absolute;left:79.22px;top:151.44px" class="cls_014"><span class="cls_014">istr = int(astr)</span></div>
<div style="position:absolute;left:330.17px;top:150.00px" class="cls_011"><span class="cls_011">the </span><span class="cls_031">except: </span><span class="cls_011">clause and</span></div>
<div style="position:absolute;left:36.12px;top:173.28px" class="cls_013"><span class="cls_013">except:</span></div>
<div style="position:absolute;left:79.22px;top:194.40px" class="cls_013"><span class="cls_013">istr = -1</span></div>
<div style="position:absolute;left:330.17px;top:186.00px" class="cls_011"><span class="cls_011">the program continues.</span></div>
<div style="position:absolute;left:36.12px;top:237.36px" class="cls_013"><span class="cls_013">print('First', istr)</span></div>
<div style="position:absolute;left:303.17px;top:228.96px" class="cls_011"><span class="cls_011">• When the second</span></div>
<div style="position:absolute;left:330.17px;top:264.96px" class="cls_011"><span class="cls_011">conversion succeeds - it</span></div>
<div style="position:absolute;left:36.12px;top:280.32px" class="cls_013"><span class="cls_013">astr = '123'</span></div>
<div style="position:absolute;left:36.12px;top:304.32px" class="cls_013"><span class="cls_013">try:</span></div>
<div style="position:absolute;left:330.17px;top:300.96px" class="cls_011"><span class="cls_011">just skips the </span><span class="cls_031">except:</span></div>
<div style="position:absolute;left:79.22px;top:324.48px" class="cls_029"><span class="cls_029">istr = int(astr)</span></div>
<div style="position:absolute;left:36.12px;top:346.32px" class="cls_013"><span class="cls_013">except:</span></div>
<div style="position:absolute;left:330.17px;top:336.96px" class="cls_011"><span class="cls_011">clause and the program</span></div>
<div style="position:absolute;left:79.22px;top:367.44px" class="cls_013"><span class="cls_013">istr = -1</span></div>
<div style="position:absolute;left:330.17px;top:372.96px" class="cls_011"><span class="cls_011">continues.</span></div>
<div style="position:absolute;left:36.12px;top:411.36px" class="cls_013"><span class="cls_013">print('Second', istr)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:233.30px;top:16.56px" class="cls_008"><span class="cls_008">More Exceptions</span></div>
<div style="position:absolute;left:151.97px;top:96.96px" class="cls_013"><span class="cls_013">try:</span></div>
<div style="position:absolute;left:195.07px;top:118.08px" class="cls_032"><span class="cls_032">print('hello')</span></div>
<div style="position:absolute;left:151.97px;top:139.92px" class="cls_013"><span class="cls_013">except:</span></div>
<div style="position:absolute;left:195.07px;top:161.04px" class="cls_032"><span class="cls_032">print('something is wrong')</span></div>
<div style="position:absolute;left:151.97px;top:183.12px" class="cls_013"><span class="cls_013">else:</span></div>
<div style="position:absolute;left:195.07px;top:204.96px" class="cls_032"><span class="cls_032">print('nothing is wrong')</span></div>
<div style="position:absolute;left:151.97px;top:226.08px" class="cls_013"><span class="cls_013">finally:</span></div>
<div style="position:absolute;left:195.07px;top:247.92px" class="cls_032"><span class="cls_032">print('the try block is completed')</span></div>
<div style="position:absolute;left:57.60px;top:326.64px" class="cls_011"><span class="cls_011">• The </span><span class="cls_031">else </span><span class="cls_011">keyword block of code is executed</span></div>
<div style="position:absolute;left:84.60px;top:355.44px" class="cls_011"><span class="cls_011">if no errors were raised.</span></div>
<div style="position:absolute;left:57.60px;top:391.44px" class="cls_011"><span class="cls_011">• The </span><span class="cls_031">finally </span><span class="cls_011">keyword block of code is</span></div>
<div style="position:absolute;left:84.60px;top:420.48px" class="cls_011"><span class="cls_011">executed at the end of the try-except block.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:202.22px;top:16.56px" class="cls_008"><span class="cls_008">Exceptions Example</span></div>
<div style="position:absolute;left:8.57px;top:147.12px" class="cls_029"><span class="cls_029">rawstr = input('Enter a number:')</span></div>
<div style="position:absolute;left:8.57px;top:168.00px" class="cls_029"><span class="cls_029">try:</span></div>
<div style="position:absolute;left:385.97px;top:162.40px" class="cls_010"><span class="cls_010">• Error checking using</span></div>
<div style="position:absolute;left:51.62px;top:190.08px" class="cls_029"><span class="cls_029">ival = int(rawstr)</span></div>
<div style="position:absolute;left:8.57px;top:211.92px" class="cls_029"><span class="cls_029">except:</span></div>
<div style="position:absolute;left:412.97px;top:201.52px" class="cls_010"><span class="cls_010">try/except statements</span></div>
<div style="position:absolute;left:51.62px;top:233.04px" class="cls_014"><span class="cls_014">ival = -1</span></div>
<div style="position:absolute;left:8.57px;top:276.96px" class="cls_029"><span class="cls_029">if ival > 0 :</span></div>
<div style="position:absolute;left:51.62px;top:299.04px" class="cls_029"><span class="cls_029">print('Nice work')</span></div>
<div style="position:absolute;left:8.57px;top:319.92px" class="cls_029"><span class="cls_029">else:</span></div>
<div style="position:absolute;left:51.62px;top:342.00px" class="cls_029"><span class="cls_029">print('Not a number')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.20px;top:16.56px" class="cls_008"><span class="cls_008">Summary</span></div>
<div style="position:absolute;left:81.82px;top:89.04px" class="cls_033"><span class="cls_033">§ type bool</span></div>
<div style="position:absolute;left:81.82px;top:136.08px" class="cls_033"><span class="cls_033">§ Boolean Expression</span></div>
<div style="position:absolute;left:81.82px;top:183.12px" class="cls_033"><span class="cls_033">§ Comparison operators</span></div>
<div style="position:absolute;left:81.82px;top:230.16px" class="cls_033"><span class="cls_033">§ Logical operators</span></div>
<div style="position:absolute;left:81.82px;top:277.20px" class="cls_033"><span class="cls_033">§ If statements</span></div>
<div style="position:absolute;left:81.82px;top:324.24px" class="cls_033"><span class="cls_033">§ Indentation</span></div>
<div style="position:absolute;left:81.82px;top:371.04px" class="cls_033"><span class="cls_033">§ If else statement and elif statement</span></div>
<div style="position:absolute;left:81.82px;top:418.08px" class="cls_033"><span class="cls_033">§ Catching exceptions</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.50px;top:16.56px" class="cls_008"><span class="cls_008">To Do List for Next Class</span></div>
<div style="position:absolute;left:107.95px;top:96.16px" class="cls_034"><span class="cls_034">• Read Chapters 3 and 4</span></div>
<div style="position:absolute;left:107.95px;top:146.08px" class="cls_034"><span class="cls_034">• Execute code snippets and</span></div>
<div style="position:absolute;left:134.95px;top:192.16px" class="cls_034"><span class="cls_034">chapter exercises in the book</span></div>
<div style="position:absolute;left:107.95px;top:250.24px" class="cls_034"><span class="cls_034">• Start working on Homework 1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.18px;top:16.56px" class="cls_008"><span class="cls_008">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.28px;top:59.76px" class="cls_008"><span class="cls_008">tions</span></div>
<div style="position:absolute;left:107.95px;top:127.44px" class="cls_035"><span class="cls_035">§  These slides are Copyright 2010-  Charles R. Severance</span></div>
<div style="position:absolute;left:134.95px;top:148.56px" class="cls_035"><span class="cls_035">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.95px;top:170.40px" class="cls_035"><span class="cls_035">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.95px;top:191.52px" class="cls_035"><span class="cls_035">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.95px;top:213.36px" class="cls_035"><span class="cls_035">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.95px;top:235.44px" class="cls_035"><span class="cls_035">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.95px;top:256.56px" class="cls_035"><span class="cls_035">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.95px;top:278.40px" class="cls_035"><span class="cls_035">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.95px;top:299.52px" class="cls_035"><span class="cls_035">materials.</span></div>
<div style="position:absolute;left:107.95px;top:352.56px" class="cls_035"><span class="cls_035">§  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.90px;top:374.40px" class="cls_035"><span class="cls_035">Michigan School of Information</span></div>
<div style="position:absolute;left:107.95px;top:427.44px" class="cls_035"><span class="cls_035">§  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.90px;top:448.56px" class="cls_035"><span class="cls_035">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5Conditional/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>

</body>
</html>
