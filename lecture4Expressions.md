<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,73,125);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,73,125);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_075{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_075{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_076{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_076{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:26.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:26.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Courier New,serif;font-size:16.0px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Courier New,serif;font-size:16.0px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:30.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_024{font-family:Courier New,serif;font-size:36.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Courier New,serif;font-size:36.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Courier New,serif;font-size:36.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Courier New,serif;font-size:36.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Courier New,serif;font-size:36.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Courier New,serif;font-size:36.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:32.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:32.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,249,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,249,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,83,90);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,83,90);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,252,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,252,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_040{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Courier New,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_037{font-family:Courier New,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_038{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_038{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_039{font-family:Courier New,serif;font-size:24.1px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_039{font-family:Courier New,serif;font-size:24.1px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:24.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:24.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:24.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:24.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_043{font-family:Arial,serif;font-size:24.1px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:Arial,serif;font-size:24.1px;color:rgb(102,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Courier New,serif;font-size:24.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:32.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_046{font-family:Arial,serif;font-size:32.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_047{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_045{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_045{font-family:Courier New,serif;font-size:24.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_048{font-family:Arial,serif;font-size:20.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_048{font-family:Arial,serif;font-size:20.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_049{font-family:Arial,serif;font-size:32.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_049{font-family:Arial,serif;font-size:32.0px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_051{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_051{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_052{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,191,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_052{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,191,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_053{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,249,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_053{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,249,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_054{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_054{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_055{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_055{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_056{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_056{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_057{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_057{font-family:Courier New,serif;font-size:20.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_058{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_058{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_059{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_059{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_060{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_060{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,152,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_061{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_061{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_062{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_062{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_063{font-family:Courier New,serif;font-size:20.1px;color:rgb(224,101,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_063{font-family:Courier New,serif;font-size:20.1px;color:rgb(224,101,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_064{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,64,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_064{font-family:Courier New,serif;font-size:20.1px;color:rgb(255,64,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_065{font-family:Courier New,serif;font-size:14.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_065{font-family:Courier New,serif;font-size:14.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_066{font-family:Courier New,serif;font-size:14.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_066{font-family:Courier New,serif;font-size:14.0px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_067{font-family:Courier New,serif;font-size:14.0px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_067{font-family:Courier New,serif;font-size:14.0px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_069{font-family:Courier New,serif;font-size:14.0px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_069{font-family:Courier New,serif;font-size:14.0px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_068{font-family:Courier New,serif;font-size:14.0px;color:rgb(224,101,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_068{font-family:Courier New,serif;font-size:14.0px;color:rgb(224,101,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_070{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_070{font-family:Courier New,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_071{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_071{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_072{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_072{font-family:Arial,serif;font-size:25.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_073{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_073{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_074{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_074{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture4Expressions/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.56px;top:187.12px" class="cls_002"><span class="cls_002">Jupyter Notebook and</span></div>
<div style="position:absolute;left:44.56px;top:223.12px" class="cls_002"><span class="cls_002">Chapter 2:</span></div>
<div style="position:absolute;left:44.56px;top:260.08px" class="cls_002"><span class="cls_002">Expressions</span></div>
<div style="position:absolute;left:44.93px;top:353.76px" class="cls_003"><span class="cls_003">INSY 5336: Python Programming</span></div>
<div style="position:absolute;left:44.56px;top:407.04px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:44.56px;top:432.40px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:51.50px;top:327.44px" class="cls_007"><span class="cls_007">Introduction to Jupyter Notebook</span></div>
<div style="position:absolute;left:7.20px;top:522.00px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:254.21px;top:17.76px" class="cls_009"><span class="cls_009">Introduction</span></div>
<div style="position:absolute;left:107.92px;top:128.56px" class="cls_008"><span class="cls_008">• Jupyter Notebook is an open</span></div>
<div style="position:absolute;left:134.92px;top:167.68px" class="cls_008"><span class="cls_008">source, interactive computational</span></div>
<div style="position:absolute;left:134.92px;top:205.60px" class="cls_008"><span class="cls_008">environment</span></div>
<div style="position:absolute;left:107.92px;top:251.68px" class="cls_008"><span class="cls_008">• In this class we will use Jupyter</span></div>
<div style="position:absolute;left:134.92px;top:289.60px" class="cls_008"><span class="cls_008">Notebook as IDE for Python</span></div>
<div style="position:absolute;left:107.92px;top:335.68px" class="cls_008"><span class="cls_008">• ipython is the predecessor to</span></div>
<div style="position:absolute;left:134.92px;top:374.56px" class="cls_008"><span class="cls_008">Jupyter Notebook</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:124.23px;top:17.76px" class="cls_009"><span class="cls_009">Jupyter Notebook Installation</span></div>
<div style="position:absolute;left:107.92px;top:128.56px" class="cls_008"><span class="cls_008">• Install Python and Jupyter using</span></div>
<div style="position:absolute;left:134.92px;top:167.68px" class="cls_008"><span class="cls_008"> </span><A HREF="http://www.anaconda.com/downloads/">www.anaconda.com/downloads</A> </div>
<div style="position:absolute;left:107.92px;top:213.52px" class="cls_008"><span class="cls_008">• It includes Python, the Jupyter</span></div>
<div style="position:absolute;left:134.92px;top:251.68px" class="cls_008"><span class="cls_008">Notebook, and other commonly</span></div>
<div style="position:absolute;left:134.92px;top:289.60px" class="cls_008"><span class="cls_008">used packages for scientific</span></div>
<div style="position:absolute;left:134.92px;top:328.72px" class="cls_008"><span class="cls_008">computing and data science.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.23px;top:17.76px" class="cls_009"><span class="cls_009">Running Jupyter Notebook</span></div>
<div style="position:absolute;left:107.92px;top:124.72px" class="cls_075"><span class="cls_075">Windows</span></div>
<div style="position:absolute;left:107.92px;top:167.68px" class="cls_008"><span class="cls_008">• At the cmd prompt type: jupyter</span></div>
<div style="position:absolute;left:134.92px;top:201.52px" class="cls_008"><span class="cls_008">notebook</span></div>
<div style="position:absolute;left:107.92px;top:243.52px" class="cls_075"><span class="cls_075">Apple iMac or laptop</span></div>
<div style="position:absolute;left:107.92px;top:286.72px" class="cls_008"><span class="cls_008">• At the terminal window type:</span></div>
<div style="position:absolute;left:134.92px;top:320.56px" class="cls_008"><span class="cls_008">jupyter notebook</span></div>
<div style="position:absolute;left:107.92px;top:362.56px" class="cls_075"><span class="cls_075">From Anaconda Navigator</span></div>
<div style="position:absolute;left:107.92px;top:405.52px" class="cls_008"><span class="cls_008">• Launch jupyter notebook</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:92.87px;top:17.76px" class="cls_009"><span class="cls_009">Running Jupyter Notebook from</span></div>
<div style="position:absolute;left:162.53px;top:60.72px" class="cls_009"><span class="cls_009">the Anaconda Navigator</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:147.55px;top:17.76px" class="cls_009"><span class="cls_009">Types of Cells in Jupyter</span></div>
<div style="position:absolute;left:276.21px;top:60.72px" class="cls_009"><span class="cls_009">Notebook</span></div>
<div style="position:absolute;left:107.92px;top:124.72px" class="cls_008"><span class="cls_008">• Code Cells:</span></div>
<div style="position:absolute;left:143.92px;top:166.64px" class="cls_010"><span class="cls_010">- Allow you to enter and run Python</span></div>
<div style="position:absolute;left:166.42px;top:196.64px" class="cls_010"><span class="cls_010">code</span></div>
<div style="position:absolute;left:143.92px;top:233.60px" class="cls_010"><span class="cls_010">- Run a code cell by Shift+Enter</span></div>
<div style="position:absolute;left:107.92px;top:271.60px" class="cls_008"><span class="cls_008">• Markdown Cells:</span></div>
<div style="position:absolute;left:143.92px;top:313.52px" class="cls_010"><span class="cls_010">- Allow you to enter text content</span></div>
<div style="position:absolute;left:143.92px;top:350.72px" class="cls_010"><span class="cls_010">- Markdown language is a markup</span></div>
<div style="position:absolute;left:166.42px;top:380.72px" class="cls_010"><span class="cls_010">language (like HTML) for text</span></div>
<div style="position:absolute;left:166.42px;top:410.72px" class="cls_010"><span class="cls_010">display</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:117.21px;top:17.76px" class="cls_009"><span class="cls_009">Markdown Language Basics</span></div>
<div style="position:absolute;left:95.54px;top:119.20px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:122.54px;top:119.20px" class="cls_008"><span class="cls_008"># Heading</span></div>
<div style="position:absolute;left:95.54px;top:165.04px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:122.54px;top:165.04px" class="cls_008"><span class="cls_008">*italics*</span></div>
<div style="position:absolute;left:95.54px;top:211.12px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:122.54px;top:211.12px" class="cls_008"><span class="cls_008">__bold__</span></div>
<div style="position:absolute;left:95.54px;top:257.20px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:122.54px;top:257.20px" class="cls_008"><span class="cls_008">`</span><A HREF="http://www.cnn.com/`/">http://www.cnn.com/`</A> </div>
<div style="position:absolute;left:95.54px;top:303.04px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:122.54px;top:303.04px" class="cls_008"><span class="cls_008">```python x=0.6 ```</span></div>
<div style="position:absolute;left:95.54px;top:350.08px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:122.54px;top:350.08px" class="cls_008"><span class="cls_008">- Bullet2</span></div>
<div style="position:absolute;left:95.54px;top:396.16px" class="cls_008"><span class="cls_008">•</span></div>
<div style="position:absolute;left:202.41px;top:396.16px" class="cls_008"><span class="cls_008">- Bullet2.1</span></div>
<div style="position:absolute;left:32.99px;top:447.12px" class="cls_011"><span class="cls_011">Reference:</span></div>
<div style="position:absolute;left:32.99px;top:461.04px" class="cls_076"><span class="cls_076"> </span><A HREF="https://medium.com/ibm-data-science-experience/markdown-for-jupyter-notebooks-cheatsheet-386c05aeebed">https://medium.com/ibm-data-science-experience/markdown-for-jupyter-notebooks-cheatsheet-386c05aeebed</A> </div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:145.21px;top:17.76px" class="cls_009"><span class="cls_009">Jupyter Notebook Kernel</span></div>
<div style="position:absolute;left:95.54px;top:119.20px" class="cls_008"><span class="cls_008">• Python code is run in a separate</span></div>
<div style="position:absolute;left:122.54px;top:158.08px" class="cls_008"><span class="cls_008">process called the Kernel</span></div>
<div style="position:absolute;left:95.54px;top:204.16px" class="cls_008"><span class="cls_008">• The Kernel can be interrupted or</span></div>
<div style="position:absolute;left:122.54px;top:242.08px" class="cls_008"><span class="cls_008">restarted</span></div>
<div style="position:absolute;left:95.54px;top:288.16px" class="cls_008"><span class="cls_008">• A running Kernel saves all results</span></div>
<div style="position:absolute;left:122.54px;top:326.08px" class="cls_008"><span class="cls_008">and variable values</span></div>
<div style="position:absolute;left:95.54px;top:373.12px" class="cls_008"><span class="cls_008">• Restart the Kernel before</span></div>
<div style="position:absolute;left:122.54px;top:411.04px" class="cls_008"><span class="cls_008">beginning a new project execution</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:101.10px;top:180.32px" class="cls_007"><span class="cls_007">Variables, Expressions and</span></div>
<div style="position:absolute;left:252.13px;top:228.32px" class="cls_007"><span class="cls_007">Statements</span></div>
<div style="position:absolute;left:7.20px;top:522.00px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:271.21px;top:17.76px" class="cls_009"><span class="cls_009">Constants</span></div>
<div style="position:absolute;left:81.92px;top:93.84px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> Constants are Fixed Values such</span></div>
<div style="position:absolute;left:108.92px;top:132.96px" class="cls_014"><span class="cls_014">as:</span></div>
<div style="position:absolute;left:507.85px;top:143.52px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_019">123</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:507.85px;top:165.60px" class="cls_015"><span class="cls_015">123</span></div>
<div style="position:absolute;left:117.92px;top:178.96px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Numbers</span></div>
<div style="position:absolute;left:507.85px;top:186.48px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_019">98.6</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:507.85px;top:208.56px" class="cls_015"><span class="cls_015">98.6</span></div>
<div style="position:absolute;left:117.92px;top:220.00px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Letters</span></div>
<div style="position:absolute;left:507.85px;top:230.64px" class="cls_015"><span class="cls_015">>>></span><span class="cls_018"> print(</span><span class="cls_019">'Hello</span></div>
<div style="position:absolute;left:507.85px;top:251.52px" class="cls_019"><span class="cls_019">world'</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:117.92px;top:259.84px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Strings</span></div>
<div style="position:absolute;left:507.85px;top:273.60px" class="cls_015"><span class="cls_015">Hello world</span></div>
<div style="position:absolute;left:81.92px;top:301.92px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> Numeric constants are written as</span></div>
<div style="position:absolute;left:108.92px;top:340.80px" class="cls_014"><span class="cls_014">their value</span></div>
<div style="position:absolute;left:81.92px;top:387.84px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> String constants use single quotes</span></div>
<div style="position:absolute;left:108.92px;top:427.92px" class="cls_014"><span class="cls_014">(‘) or double quotes (“)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:217.54px;top:17.76px" class="cls_009"><span class="cls_009">Reserved Words</span></div>
<div style="position:absolute;left:149.64px;top:123.92px" class="cls_020"><span class="cls_020">False</span></div>
<div style="position:absolute;left:221.64px;top:123.92px" class="cls_020"><span class="cls_020">class</span></div>
<div style="position:absolute;left:293.64px;top:123.92px" class="cls_020"><span class="cls_020">return  is</span></div>
<div style="position:absolute;left:437.64px;top:123.92px" class="cls_020"><span class="cls_020">finally</span></div>
<div style="position:absolute;left:149.64px;top:144.08px" class="cls_020"><span class="cls_020">None</span></div>
<div style="position:absolute;left:221.64px;top:144.08px" class="cls_020"><span class="cls_020">if</span></div>
<div style="position:absolute;left:293.64px;top:144.08px" class="cls_020"><span class="cls_020">for</span></div>
<div style="position:absolute;left:365.64px;top:144.08px" class="cls_020"><span class="cls_020">lambda  continue</span></div>
<div style="position:absolute;left:149.64px;top:163.04px" class="cls_020"><span class="cls_020">True</span></div>
<div style="position:absolute;left:221.64px;top:163.04px" class="cls_020"><span class="cls_020">def</span></div>
<div style="position:absolute;left:293.64px;top:163.04px" class="cls_020"><span class="cls_020">from</span></div>
<div style="position:absolute;left:365.64px;top:163.04px" class="cls_020"><span class="cls_020">while</span></div>
<div style="position:absolute;left:437.64px;top:163.04px" class="cls_020"><span class="cls_020">nonlocal</span></div>
<div style="position:absolute;left:149.64px;top:182.00px" class="cls_020"><span class="cls_020">and</span></div>
<div style="position:absolute;left:221.64px;top:182.00px" class="cls_020"><span class="cls_020">del</span></div>
<div style="position:absolute;left:293.64px;top:182.00px" class="cls_020"><span class="cls_020">global not</span></div>
<div style="position:absolute;left:437.64px;top:182.00px" class="cls_020"><span class="cls_020">with</span></div>
<div style="position:absolute;left:149.64px;top:200.96px" class="cls_020"><span class="cls_020">as</span></div>
<div style="position:absolute;left:221.64px;top:200.96px" class="cls_020"><span class="cls_020">elif</span></div>
<div style="position:absolute;left:293.64px;top:200.96px" class="cls_020"><span class="cls_020">try</span></div>
<div style="position:absolute;left:365.64px;top:200.96px" class="cls_020"><span class="cls_020">or</span></div>
<div style="position:absolute;left:437.64px;top:200.96px" class="cls_020"><span class="cls_020">yield</span></div>
<div style="position:absolute;left:149.64px;top:219.92px" class="cls_020"><span class="cls_020">assert  else</span></div>
<div style="position:absolute;left:293.64px;top:219.92px" class="cls_020"><span class="cls_020">import  pass</span></div>
<div style="position:absolute;left:149.64px;top:240.08px" class="cls_020"><span class="cls_020">break</span></div>
<div style="position:absolute;left:221.64px;top:240.08px" class="cls_020"><span class="cls_020">except  in</span></div>
<div style="position:absolute;left:365.64px;top:240.08px" class="cls_020"><span class="cls_020">raise</span></div>
<div style="position:absolute;left:82.64px;top:300.16px" class="cls_021"><span class="cls_021">§</span><span class="cls_022"> Cannot use reserved words as</span></div>
<div style="position:absolute;left:109.64px;top:346.24px" class="cls_022"><span class="cls_022">variable names or identifiers</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:280.20px;top:17.76px" class="cls_009"><span class="cls_009">Variables</span></div>
<div style="position:absolute;left:81.92px;top:93.84px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> A </span><span class="cls_023">Variable</span><span class="cls_014"> is a named</span></div>
<div style="position:absolute;left:491.82px;top:102.72px" class="cls_024"><span class="cls_024">x </span><span class="cls_025">=</span><span class="cls_026"> 12.2</span></div>
<div style="position:absolute;left:108.92px;top:132.96px" class="cls_014"><span class="cls_014">place in memory</span></div>
<div style="position:absolute;left:491.82px;top:145.68px" class="cls_024"><span class="cls_024">y</span><span class="cls_025"> =</span><span class="cls_026"> 14</span></div>
<div style="position:absolute;left:81.92px;top:180.00px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> Programmers choose</span></div>
<div style="position:absolute;left:108.92px;top:219.84px" class="cls_014"><span class="cls_014">variable names</span></div>
<div style="position:absolute;left:81.92px;top:266.88px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> You assign a new value to a</span></div>
<div style="position:absolute;left:108.92px;top:306.00px" class="cls_014"><span class="cls_014">variable by the “=“ sign</span></div>
<div style="position:absolute;left:491.82px;top:316.80px" class="cls_024"><span class="cls_024">x</span></div>
<div style="position:absolute;left:547.60px;top:321.76px" class="cls_027"><span class="cls_027">12.2</span></div>
<div style="position:absolute;left:81.92px;top:352.80px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> You can also change the</span></div>
<div style="position:absolute;left:491.82px;top:359.76px" class="cls_024"><span class="cls_024">y</span></div>
<div style="position:absolute;left:547.60px;top:372.88px" class="cls_027"><span class="cls_027">14</span></div>
<div style="position:absolute;left:108.92px;top:392.88px" class="cls_014"><span class="cls_014">contents of a variable using</span></div>
<div style="position:absolute;left:108.92px;top:432.00px" class="cls_014"><span class="cls_014">the “=“ sign</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:280.20px;top:17.76px" class="cls_009"><span class="cls_009">Variables</span></div>
<div style="position:absolute;left:81.92px;top:93.84px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> A </span><span class="cls_023">Variable</span><span class="cls_014"> is a named</span></div>
<div style="position:absolute;left:491.82px;top:102.72px" class="cls_024"><span class="cls_024">x </span><span class="cls_025">=</span><span class="cls_026"> 12.2</span></div>
<div style="position:absolute;left:108.92px;top:132.96px" class="cls_014"><span class="cls_014">place in memory</span></div>
<div style="position:absolute;left:491.82px;top:145.68px" class="cls_024"><span class="cls_024">y</span><span class="cls_025"> =</span><span class="cls_026"> 14</span></div>
<div style="position:absolute;left:81.92px;top:180.00px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> Programmers choose</span></div>
<div style="position:absolute;left:491.82px;top:188.64px" class="cls_024"><span class="cls_024">x </span><span class="cls_025">=</span><span class="cls_026"> 100</span></div>
<div style="position:absolute;left:108.92px;top:219.84px" class="cls_014"><span class="cls_014">variable names</span></div>
<div style="position:absolute;left:81.92px;top:266.88px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> You assign a new value to a</span></div>
<div style="position:absolute;left:108.92px;top:306.00px" class="cls_014"><span class="cls_014">variable by the “=“ sign</span></div>
<div style="position:absolute;left:491.82px;top:316.80px" class="cls_024"><span class="cls_024">x</span></div>
<div style="position:absolute;left:627.51px;top:321.76px" class="cls_027"><span class="cls_027">100</span></div>
<div style="position:absolute;left:81.92px;top:352.80px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> You can also change the</span></div>
<div style="position:absolute;left:491.82px;top:359.76px" class="cls_024"><span class="cls_024">y</span></div>
<div style="position:absolute;left:547.60px;top:372.88px" class="cls_027"><span class="cls_027">14</span></div>
<div style="position:absolute;left:108.92px;top:392.88px" class="cls_014"><span class="cls_014">contents of a variable using</span></div>
<div style="position:absolute;left:108.92px;top:432.00px" class="cls_014"><span class="cls_014">the “=“ sign</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:182.20px;top:17.76px" class="cls_009"><span class="cls_009">Variable Name Rules</span></div>
<div style="position:absolute;left:107.92px;top:100.44px" class="cls_028"><span class="cls_028">• Cannot use Python keywords</span></div>
<div style="position:absolute;left:107.92px;top:132.36px" class="cls_028"><span class="cls_028">• Must start with a letter or underscore</span></div>
<div style="position:absolute;left:134.92px;top:158.52px" class="cls_028"><span class="cls_028">“_”</span></div>
<div style="position:absolute;left:107.92px;top:190.44px" class="cls_028"><span class="cls_028">• Must consist of letters, numbers and</span></div>
<div style="position:absolute;left:134.92px;top:216.36px" class="cls_028"><span class="cls_028">underscores</span></div>
<div style="position:absolute;left:107.92px;top:249.48px" class="cls_028"><span class="cls_028">• Case Sensitive</span></div>
<div style="position:absolute;left:107.92px;top:281.40px" class="cls_028"><span class="cls_028">• Cannot contain a space</span></div>
<div style="position:absolute;left:86.48px;top:339.84px" class="cls_029"><span class="cls_029">Good:</span></div>
<div style="position:absolute;left:215.86px;top:339.84px" class="cls_030"><span class="cls_030">spam</span></div>
<div style="position:absolute;left:330.87px;top:339.84px" class="cls_030"><span class="cls_030">eggs</span></div>
<div style="position:absolute;left:431.50px;top:339.84px" class="cls_030"><span class="cls_030">spam23</span></div>
<div style="position:absolute;left:575.26px;top:339.84px" class="cls_030"><span class="cls_030">_speed</span></div>
<div style="position:absolute;left:86.48px;top:368.64px" class="cls_031"><span class="cls_031">Bad:</span></div>
<div style="position:absolute;left:215.86px;top:368.64px" class="cls_030"><span class="cls_030">23spam</span></div>
<div style="position:absolute;left:374.00px;top:368.64px" class="cls_030"><span class="cls_030">#sign  var.12</span></div>
<div style="position:absolute;left:86.48px;top:396.72px" class="cls_032"><span class="cls_032">Different:</span></div>
<div style="position:absolute;left:287.73px;top:396.72px" class="cls_030"><span class="cls_030">spam</span></div>
<div style="position:absolute;left:388.37px;top:396.72px" class="cls_030"><span class="cls_030">Spam</span></div>
<div style="position:absolute;left:489.00px;top:396.72px" class="cls_030"><span class="cls_030">SPAM</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:280.20px;top:17.76px" class="cls_009"><span class="cls_009">Variables</span></div>
<div style="position:absolute;left:39.38px;top:111.84px" class="cls_018"><span class="cls_018">x1q3z9ocd = 35.0</span></div>
<div style="position:absolute;left:470.51px;top:111.84px" class="cls_033"><span class="cls_033">a = 35.0</span></div>
<div style="position:absolute;left:39.38px;top:133.68px" class="cls_018"><span class="cls_018">x1q3z9afd = 12.50</span></div>
<div style="position:absolute;left:470.51px;top:133.68px" class="cls_033"><span class="cls_033">b = 12.50</span></div>
<div style="position:absolute;left:39.38px;top:154.80px" class="cls_018"><span class="cls_018">x1q3p9afd = x1q3z9ocd * x1q3z9afd</span></div>
<div style="position:absolute;left:470.51px;top:154.80px" class="cls_033"><span class="cls_033">c = a * b</span></div>
<div style="position:absolute;left:39.38px;top:176.88px" class="cls_018"><span class="cls_018">print(x1q3p9afd)</span></div>
<div style="position:absolute;left:470.51px;top:176.88px" class="cls_033"><span class="cls_033">print(c)</span></div>
<div style="position:absolute;left:421.32px;top:320.16px" class="cls_034"><span class="cls_034">hours = 35.0</span></div>
<div style="position:absolute;left:59.56px;top:334.92px" class="cls_028"><span class="cls_028">• What are these</span></div>
<div style="position:absolute;left:421.32px;top:342.00px" class="cls_034"><span class="cls_034">rate = 12.50</span></div>
<div style="position:absolute;left:421.32px;top:363.12px" class="cls_034"><span class="cls_034">pay = hours * rate</span></div>
<div style="position:absolute;left:86.56px;top:360.84px" class="cls_028"><span class="cls_028">different bits of code</span></div>
<div style="position:absolute;left:421.32px;top:385.20px" class="cls_034"><span class="cls_034">print(pay)</span></div>
<div style="position:absolute;left:86.56px;top:387.00px" class="cls_028"><span class="cls_028">doing?</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:262.21px;top:17.76px" class="cls_009"><span class="cls_009">Statements</span></div>
<div style="position:absolute;left:44.91px;top:128.64px" class="cls_035"><span class="cls_035">• A statement is a unit (or line) of code</span></div>
<div style="position:absolute;left:71.91px;top:164.64px" class="cls_035"><span class="cls_035">that the Python interpreter can execute</span></div>
<div style="position:absolute;left:369.76px;top:254.64px" class="cls_040"><span class="cls_040">Assignment Statement</span></div>
<div style="position:absolute;left:137.63px;top:258.24px" class="cls_036"><span class="cls_036">x</span><span class="cls_037"> =</span><span class="cls_038"> 2</span></div>
<div style="position:absolute;left:137.63px;top:287.28px" class="cls_036"><span class="cls_036">x</span><span class="cls_037"> = </span><span class="cls_036">x</span><span class="cls_037"> + </span><span class="cls_038">2</span></div>
<div style="position:absolute;left:369.76px;top:285.84px" class="cls_040"><span class="cls_040">Assignment with expression</span></div>
<div style="position:absolute;left:137.63px;top:315.12px" class="cls_039"><span class="cls_039">print(</span><span class="cls_036">x</span><span class="cls_039">)</span></div>
<div style="position:absolute;left:369.76px;top:321.84px" class="cls_040"><span class="cls_040">Print Statement</span></div>
<div style="position:absolute;left:130.97px;top:387.84px" class="cls_041"><span class="cls_041">Variable</span></div>
<div style="position:absolute;left:257.03px;top:387.84px" class="cls_003"><span class="cls_003">Operator</span><span class="cls_042">    Constant</span></div>
<div style="position:absolute;left:532.96px;top:388.08px" class="cls_043"><span class="cls_043">Function</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:164.21px;top:17.76px" class="cls_009"><span class="cls_009">Assignment Statement</span></div>
<div style="position:absolute;left:107.92px;top:125.64px" class="cls_028"><span class="cls_028">• We assign a value to a variable using</span></div>
<div style="position:absolute;left:134.92px;top:154.68px" class="cls_028"><span class="cls_028">the “=“ operator</span></div>
<div style="position:absolute;left:107.92px;top:190.68px" class="cls_028"><span class="cls_028">• An assignment statement consists of</span></div>
<div style="position:absolute;left:134.92px;top:219.72px" class="cls_028"><span class="cls_028">an expression on the right-hand side</span></div>
<div style="position:absolute;left:134.92px;top:248.52px" class="cls_028"><span class="cls_028">and a variable on the left-hand side</span></div>
<div style="position:absolute;left:208.95px;top:363.12px" class="cls_044"><span class="cls_044">x</span><span class="cls_030"> =</span></div>
<div style="position:absolute;left:266.45px;top:363.12px" class="cls_030"><span class="cls_030">3.9 </span><span class="cls_038">*</span><span class="cls_044"> x </span><span class="cls_038">*</span><span class="cls_030"> ( 1 </span><span class="cls_038">-</span><span class="cls_044"> x</span><span class="cls_030"> )</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:126.88px;top:17.76px" class="cls_009"><span class="cls_009">Anatomy of an Assignment</span></div>
<div style="position:absolute;left:451.80px;top:83.20px" class="cls_046"><span class="cls_046">x</span></div>
<div style="position:absolute;left:486.62px;top:83.20px" class="cls_027"><span class="cls_027">0.6</span></div>
<div style="position:absolute;left:26.84px;top:116.52px" class="cls_028"><span class="cls_028">• Original value of x is</span></div>
<div style="position:absolute;left:53.84px;top:142.44px" class="cls_028"><span class="cls_028">stored in memory</span></div>
<div style="position:absolute;left:621.09px;top:215.92px" class="cls_047"><span class="cls_047">0.6</span></div>
<div style="position:absolute;left:436.64px;top:219.76px" class="cls_047"><span class="cls_047">0.6</span></div>
<div style="position:absolute;left:284.62px;top:255.36px" class="cls_044"><span class="cls_044">x</span><span class="cls_030"> =</span><span class="cls_045"> 3.9 *  x</span></div>
<div style="position:absolute;left:485.88px;top:255.36px" class="cls_045"><span class="cls_045">* ( 1</span></div>
<div style="position:absolute;left:586.50px;top:255.36px" class="cls_045"><span class="cls_045">-  x )</span></div>
<div style="position:absolute;left:586.27px;top:339.52px" class="cls_048"><span class="cls_048">0.4</span></div>
<div style="position:absolute;left:26.84px;top:359.40px" class="cls_028"><span class="cls_028">• Once the expression is</span></div>
<div style="position:absolute;left:53.84px;top:388.20px" class="cls_028"><span class="cls_028">evaluated, the memory</span></div>
<div style="position:absolute;left:453.75px;top:407.44px" class="cls_046"><span class="cls_046">x</span></div>
<div style="position:absolute;left:486.13px;top:407.44px" class="cls_049"><span class="cls_049">0.936</span></div>
<div style="position:absolute;left:53.84px;top:417.24px" class="cls_028"><span class="cls_028">location is updated</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:273.21px;top:17.76px" class="cls_009"><span class="cls_009">Operators</span></div>
<div style="position:absolute;left:459.66px;top:154.56px" class="cls_050"><span class="cls_050">Operator</span></div>
<div style="position:absolute;left:554.78px;top:154.56px" class="cls_050"><span class="cls_050">Operation</span></div>
<div style="position:absolute;left:88.41px;top:169.36px" class="cls_008"><span class="cls_008">• Operators are</span></div>
<div style="position:absolute;left:494.77px;top:183.36px" class="cls_051"><span class="cls_051">+</span></div>
<div style="position:absolute;left:554.78px;top:183.36px" class="cls_051"><span class="cls_051">Addition</span></div>
<div style="position:absolute;left:497.02px;top:212.16px" class="cls_051"><span class="cls_051">-</span></div>
<div style="position:absolute;left:554.78px;top:212.16px" class="cls_051"><span class="cls_051">Subtraction</span></div>
<div style="position:absolute;left:115.41px;top:208.48px" class="cls_008"><span class="cls_008">symbols that</span></div>
<div style="position:absolute;left:554.78px;top:240.96px" class="cls_051"><span class="cls_051">Multiplication</span></div>
<div style="position:absolute;left:115.41px;top:246.40px" class="cls_008"><span class="cls_008">represent</span></div>
<div style="position:absolute;left:497.52px;top:269.76px" class="cls_051"><span class="cls_051">/</span></div>
<div style="position:absolute;left:554.78px;top:269.76px" class="cls_051"><span class="cls_051">Division</span></div>
<div style="position:absolute;left:115.41px;top:284.56px" class="cls_008"><span class="cls_008">computations</span></div>
<div style="position:absolute;left:493.02px;top:298.56px" class="cls_051"><span class="cls_051">**</span></div>
<div style="position:absolute;left:554.78px;top:298.56px" class="cls_051"><span class="cls_051">Power of</span></div>
<div style="position:absolute;left:492.02px;top:327.36px" class="cls_051"><span class="cls_051">%</span></div>
<div style="position:absolute;left:554.78px;top:327.36px" class="cls_051"><span class="cls_051">Remainder</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:175.21px;top:17.76px" class="cls_009"><span class="cls_009">Numeric Expressions</span></div>
<div style="position:absolute;left:273.54px;top:111.36px" class="cls_015"><span class="cls_015">>>></span><span class="cls_034"> jj</span><span class="cls_015"> = 23</span></div>
<div style="position:absolute;left:29.99px;top:119.52px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_034">xx</span><span class="cls_015"> = 2</span></div>
<div style="position:absolute;left:273.54px;top:133.20px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_034">kk</span><span class="cls_015"> = </span><span class="cls_034">jj</span><span class="cls_033"> % </span><span class="cls_015">5</span></div>
<div style="position:absolute;left:29.99px;top:141.60px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_034">xx</span><span class="cls_015"> = </span><span class="cls_034">xx</span><span class="cls_033"> +</span><span class="cls_015"> 2</span></div>
<div style="position:absolute;left:273.54px;top:154.32px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_034">kk</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:29.99px;top:162.72px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_034">xx</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:273.54px;top:176.40px" class="cls_052"><span class="cls_052">3</span></div>
<div style="position:absolute;left:29.99px;top:184.56px" class="cls_015"><span class="cls_015">4</span></div>
<div style="position:absolute;left:273.54px;top:198.24px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_015">4 </span><span class="cls_033">**</span></div>
<div style="position:absolute;left:434.79px;top:198.24px" class="cls_015"><span class="cls_015">3</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:29.99px;top:206.64px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_034">yy</span><span class="cls_015"> = 440 </span><span class="cls_033">*</span><span class="cls_015"> 12</span></div>
<div style="position:absolute;left:273.54px;top:219.36px" class="cls_015"><span class="cls_015">64</span></div>
<div style="position:absolute;left:29.99px;top:227.52px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_034">yy</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:29.99px;top:249.60px" class="cls_015"><span class="cls_015">5280</span></div>
<div style="position:absolute;left:29.99px;top:270.72px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_034">zz</span><span class="cls_015"> = </span><span class="cls_034">yy</span><span class="cls_033"> /</span><span class="cls_015"> 1000</span></div>
<div style="position:absolute;left:450.51px;top:276.00px" class="cls_050"><span class="cls_050">Operator</span></div>
<div style="position:absolute;left:545.64px;top:276.00px" class="cls_050"><span class="cls_050">Operation</span></div>
<div style="position:absolute;left:29.99px;top:292.56px" class="cls_015"><span class="cls_015">>>> </span><span class="cls_018">print(</span><span class="cls_053">zz</span><span class="cls_018">)</span></div>
<div style="position:absolute;left:485.62px;top:304.80px" class="cls_051"><span class="cls_051">+</span></div>
<div style="position:absolute;left:545.64px;top:304.80px" class="cls_051"><span class="cls_051">Addition</span></div>
<div style="position:absolute;left:29.99px;top:314.64px" class="cls_015"><span class="cls_015">5.28</span></div>
<div style="position:absolute;left:487.87px;top:333.60px" class="cls_051"><span class="cls_051">-</span></div>
<div style="position:absolute;left:545.64px;top:333.60px" class="cls_051"><span class="cls_051">Subtraction</span></div>
<div style="position:absolute;left:545.64px;top:362.40px" class="cls_051"><span class="cls_051">Multiplication</span></div>
<div style="position:absolute;left:488.37px;top:391.20px" class="cls_051"><span class="cls_051">/</span></div>
<div style="position:absolute;left:545.64px;top:391.20px" class="cls_051"><span class="cls_051">Division</span></div>
<div style="position:absolute;left:483.87px;top:420.00px" class="cls_051"><span class="cls_051">**</span></div>
<div style="position:absolute;left:545.64px;top:420.00px" class="cls_051"><span class="cls_051">Power of</span></div>
<div style="position:absolute;left:482.87px;top:448.80px" class="cls_051"><span class="cls_051">%</span></div>
<div style="position:absolute;left:545.64px;top:448.80px" class="cls_051"><span class="cls_051">Remainder</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:188.21px;top:17.76px" class="cls_009"><span class="cls_009">Order of Operations</span></div>
<div style="position:absolute;left:107.92px;top:122.60px" class="cls_054"><span class="cls_054">• We need rules to ensure that an</span></div>
<div style="position:absolute;left:134.92px;top:146.60px" class="cls_054"><span class="cls_054">expression is evaluated in a pre-</span></div>
<div style="position:absolute;left:134.92px;top:170.60px" class="cls_054"><span class="cls_054">determined order</span></div>
<div style="position:absolute;left:107.92px;top:200.60px" class="cls_054"><span class="cls_054">• This is called “operator precedence”</span></div>
<div style="position:absolute;left:107.92px;top:230.60px" class="cls_054"><span class="cls_054">• Which operator goes first, which goes next</span></div>
<div style="position:absolute;left:134.92px;top:254.60px" class="cls_054"><span class="cls_054">in the below expression:</span></div>
<div style="position:absolute;left:210.92px;top:340.72px" class="cls_055"><span class="cls_055">x</span><span class="cls_056"> = 1</span><span class="cls_057"> +</span><span class="cls_056"> 2 </span><span class="cls_057">* </span><span class="cls_056">3 </span><span class="cls_057">-</span><span class="cls_056"> 4</span><span class="cls_057"> / </span><span class="cls_056">5 </span><span class="cls_057">** </span><span class="cls_056">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:177.21px;top:17.76px" class="cls_009"><span class="cls_009">Operator Precedence</span></div>
<div style="position:absolute;left:101.88px;top:106.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_058"> Highest to Lowest </span><span class="cls_008">Precedence:</span></div>
<div style="position:absolute;left:137.88px;top:151.52px" class="cls_010"><span class="cls_010">- Parentheses</span></div>
<div style="position:absolute;left:137.88px;top:192.32px" class="cls_010"><span class="cls_010">- Exponent</span></div>
<div style="position:absolute;left:137.88px;top:232.40px" class="cls_010"><span class="cls_010">- Multiplication and Division (and</span></div>
<div style="position:absolute;left:160.38px;top:266.48px" class="cls_010"><span class="cls_010">remainder or modulus)</span></div>
<div style="position:absolute;left:137.88px;top:306.32px" class="cls_010"><span class="cls_010">- Addition and Subtraction</span></div>
<div style="position:absolute;left:101.88px;top:347.44px" class="cls_008"><span class="cls_008">• Operators with same precedence</span></div>
<div style="position:absolute;left:128.88px;top:385.36px" class="cls_008"><span class="cls_008">are evaluated from </span><span class="cls_058">left to right</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:285.21px;top:17.76px" class="cls_009"><span class="cls_009">Example</span></div>
<div style="position:absolute;left:426.37px;top:86.08px" class="cls_056"><span class="cls_056">1 + </span><span class="cls_057">2 ** 3</span><span class="cls_056"> / 4 * 5</span></div>
<div style="position:absolute;left:47.77px;top:99.52px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">x = 1 + 2 ** 3 / 4 * 5</span></div>
<div style="position:absolute;left:47.77px;top:123.52px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(x)</span></div>
<div style="position:absolute;left:47.77px;top:147.52px" class="cls_056"><span class="cls_056">11.0</span></div>
<div style="position:absolute;left:47.77px;top:171.52px" class="cls_056"><span class="cls_056">>>></span></div>
<div style="position:absolute;left:451.63px;top:179.92px" class="cls_056"><span class="cls_056">1 + </span><span class="cls_055">8 / 4</span><span class="cls_056"> * 5</span></div>
<div style="position:absolute;left:39.03px;top:264.48px" class="cls_035"><span class="cls_035">• Try this in Jupyter</span></div>
<div style="position:absolute;left:467.87px;top:283.12px" class="cls_056"><span class="cls_056">1 + </span><span class="cls_055">2 * 5</span></div>
<div style="position:absolute;left:66.03px;top:296.40px" class="cls_035"><span class="cls_035">Notebook</span></div>
<div style="position:absolute;left:39.03px;top:336.48px" class="cls_035"><span class="cls_035">• Add parenthesis to see</span></div>
<div style="position:absolute;left:483.44px;top:359.92px" class="cls_060"><span class="cls_060">1 + 10</span></div>
<div style="position:absolute;left:66.03px;top:368.40px" class="cls_035"><span class="cls_035">how the answer changes</span></div>
<div style="position:absolute;left:522.72px;top:436.00px" class="cls_060"><span class="cls_060">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:193.21px;top:17.76px" class="cls_009"><span class="cls_009">Good Mathematical</span></div>
<div style="position:absolute;left:177.21px;top:60.72px" class="cls_009"><span class="cls_009">Expression Practices</span></div>
<div style="position:absolute;left:101.88px;top:127.20px" class="cls_035"><span class="cls_035">• Remember operator precedence</span></div>
<div style="position:absolute;left:128.88px;top:163.20px" class="cls_035"><span class="cls_035">rules</span></div>
<div style="position:absolute;left:101.88px;top:206.16px" class="cls_035"><span class="cls_035">• Keep mathematical expressions</span></div>
<div style="position:absolute;left:128.88px;top:242.16px" class="cls_035"><span class="cls_035">simple and intuitive to understand</span></div>
<div style="position:absolute;left:101.88px;top:286.08px" class="cls_035"><span class="cls_035">• Use parenthesis when coding</span></div>
<div style="position:absolute;left:128.88px;top:322.08px" class="cls_035"><span class="cls_035">complex mathematical expressions</span></div>
<div style="position:absolute;left:101.88px;top:365.04px" class="cls_035"><span class="cls_035">• Break long expressions down to</span></div>
<div style="position:absolute;left:128.88px;top:401.04px" class="cls_035"><span class="cls_035">manageable chunks</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:256.21px;top:17.76px" class="cls_009"><span class="cls_009">Python type</span></div>
<div style="position:absolute;left:45.67px;top:103.32px" class="cls_028"><span class="cls_028">• Variables and Constants are categorized</span></div>
<div style="position:absolute;left:72.67px;top:132.36px" class="cls_028"><span class="cls_028">into a type in Python</span></div>
<div style="position:absolute;left:434.82px;top:154.88px" class="cls_061"><span class="cls_061">>>> ddd = 1 + 4</span></div>
<div style="position:absolute;left:434.82px;top:174.80px" class="cls_061"><span class="cls_061">>>> print(ddd)</span></div>
<div style="position:absolute;left:45.67px;top:168.36px" class="cls_028"><span class="cls_028">• Common Types in Python:</span></div>
<div style="position:absolute;left:434.82px;top:193.76px" class="cls_061"><span class="cls_061">5</span></div>
<div style="position:absolute;left:81.67px;top:203.52px" class="cls_040"><span class="cls_040">- int</span></div>
<div style="position:absolute;left:434.82px;top:212.72px" class="cls_061"><span class="cls_061">>>> eee = 'hello ' + 'there'</span></div>
<div style="position:absolute;left:434.82px;top:231.92px" class="cls_061"><span class="cls_061">>>> print(eee)</span></div>
<div style="position:absolute;left:81.67px;top:235.44px" class="cls_040"><span class="cls_040">- float</span></div>
<div style="position:absolute;left:434.82px;top:250.88px" class="cls_061"><span class="cls_061">hello there</span></div>
<div style="position:absolute;left:81.67px;top:267.36px" class="cls_040"><span class="cls_040">- str</span></div>
<div style="position:absolute;left:81.67px;top:298.32px" class="cls_040"><span class="cls_040">- list</span></div>
<div style="position:absolute;left:45.67px;top:330.36px" class="cls_028"><span class="cls_028">• Operator changes behavior based on</span></div>
<div style="position:absolute;left:72.67px;top:359.40px" class="cls_028"><span class="cls_028">operand types</span></div>
<div style="position:absolute;left:45.67px;top:395.40px" class="cls_028"><span class="cls_028">• E.g. </span><span class="cls_062">+</span><span class="cls_028"> is addition if operands are numeric</span></div>
<div style="position:absolute;left:72.67px;top:424.44px" class="cls_028"><span class="cls_028">but concatenate if operands are string</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:249.55px;top:17.76px" class="cls_009"><span class="cls_009">Type Matters</span></div>
<div style="position:absolute;left:358.37px;top:95.92px" class="cls_059"><span class="cls_059">>>> eee = 'hello ' + 'there'</span></div>
<div style="position:absolute;left:31.86px;top:99.36px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:58.86px;top:99.36px" class="cls_035"><span class="cls_035">Python knows the</span></div>
<div style="position:absolute;left:358.37px;top:119.92px" class="cls_059"><span class="cls_059">>>> </span><span class="cls_057">eee = eee + 1</span></div>
<div style="position:absolute;left:58.86px;top:128.40px" class="cls_035"><span class="cls_035">“type” of variables</span></div>
<div style="position:absolute;left:358.37px;top:143.92px" class="cls_063"><span class="cls_063">Traceback (most recent call</span></div>
<div style="position:absolute;left:58.86px;top:157.44px" class="cls_035"><span class="cls_035">and constants</span></div>
<div style="position:absolute;left:358.37px;top:167.92px" class="cls_063"><span class="cls_063">last):  File "&lt;stdin>", line</span></div>
<div style="position:absolute;left:358.37px;top:191.92px" class="cls_063"><span class="cls_063">1, in &lt;module>TypeError:</span></div>
<div style="position:absolute;left:31.86px;top:193.44px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:58.86px;top:193.44px" class="cls_035"><span class="cls_035">Some operations</span></div>
<div style="position:absolute;left:358.37px;top:215.92px" class="cls_063"><span class="cls_063">Can't convert 'int' object to</span></div>
<div style="position:absolute;left:58.86px;top:221.52px" class="cls_035"><span class="cls_035">are prohibited</span></div>
<div style="position:absolute;left:358.37px;top:239.92px" class="cls_063"><span class="cls_063">str implicitly</span></div>
<div style="position:absolute;left:358.37px;top:263.92px" class="cls_059"><span class="cls_059">>>> </span><span class="cls_055">type</span><span class="cls_059">(eee)</span></div>
<div style="position:absolute;left:31.86px;top:257.52px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:58.86px;top:257.52px" class="cls_035"><span class="cls_035">E.g. cannot “add 1”</span></div>
<div style="position:absolute;left:358.37px;top:287.92px" class="cls_059"><span class="cls_059">&lt;class'str'></span></div>
<div style="position:absolute;left:58.86px;top:286.32px" class="cls_035"><span class="cls_035">to a string</span></div>
<div style="position:absolute;left:358.37px;top:311.92px" class="cls_059"><span class="cls_059">>>> </span><span class="cls_055">type</span><span class="cls_059">('hello')</span></div>
<div style="position:absolute;left:31.86px;top:322.32px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:58.86px;top:322.32px" class="cls_035"><span class="cls_035">The type()</span></div>
<div style="position:absolute;left:358.37px;top:335.92px" class="cls_059"><span class="cls_059">&lt;class'str'></span></div>
<div style="position:absolute;left:58.86px;top:351.36px" class="cls_035"><span class="cls_035">command gives the</span></div>
<div style="position:absolute;left:358.37px;top:359.92px" class="cls_059"><span class="cls_059">>>> </span><span class="cls_055">type</span><span class="cls_059">(1)</span></div>
<div style="position:absolute;left:358.37px;top:383.92px" class="cls_059"><span class="cls_059">&lt;class'int'></span></div>
<div style="position:absolute;left:58.86px;top:380.40px" class="cls_035"><span class="cls_035">type of the variable</span></div>
<div style="position:absolute;left:358.37px;top:407.92px" class="cls_059"><span class="cls_059">>>></span></div>
<div style="position:absolute;left:58.86px;top:409.44px" class="cls_035"><span class="cls_035">(or constant)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.55px;top:17.76px" class="cls_009"><span class="cls_009">Type Conversions</span></div>
<div style="position:absolute;left:28.90px;top:106.48px" class="cls_008"><span class="cls_008">• When an integer and</span></div>
<div style="position:absolute;left:405.74px;top:127.84px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_055">float</span><span class="cls_056">(99) </span><span class="cls_057">+</span><span class="cls_056"> 100</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:405.74px;top:151.84px" class="cls_056"><span class="cls_056">199.0</span></div>
<div style="position:absolute;left:55.90px;top:145.36px" class="cls_008"><span class="cls_008">float are used in an</span></div>
<div style="position:absolute;left:405.74px;top:175.84px" class="cls_056"><span class="cls_056">>>> i = 42</span></div>
<div style="position:absolute;left:55.90px;top:183.52px" class="cls_008"><span class="cls_008">expression, the integer</span></div>
<div style="position:absolute;left:405.74px;top:199.84px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_055">type</span><span class="cls_056">(i)</span></div>
<div style="position:absolute;left:405.74px;top:223.84px" class="cls_056"><span class="cls_056">&lt;class'int'></span></div>
<div style="position:absolute;left:55.90px;top:221.44px" class="cls_008"><span class="cls_008">is implicitly converted to</span></div>
<div style="position:absolute;left:405.74px;top:247.84px" class="cls_056"><span class="cls_056">>>> f = </span><span class="cls_055">float</span><span class="cls_056">(i)</span></div>
<div style="position:absolute;left:55.90px;top:260.32px" class="cls_008"><span class="cls_008">a float</span></div>
<div style="position:absolute;left:405.74px;top:271.84px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_056">f</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:405.74px;top:295.84px" class="cls_056"><span class="cls_056">42.0</span></div>
<div style="position:absolute;left:28.90px;top:306.40px" class="cls_008"><span class="cls_008">• Numeric types can be</span></div>
<div style="position:absolute;left:405.74px;top:319.84px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_055">type</span><span class="cls_056">(f)</span></div>
<div style="position:absolute;left:405.74px;top:343.84px" class="cls_056"><span class="cls_056">&lt;class'float'></span></div>
<div style="position:absolute;left:55.90px;top:344.32px" class="cls_008"><span class="cls_008">changed by the built in</span></div>
<div style="position:absolute;left:405.74px;top:367.84px" class="cls_056"><span class="cls_056">>>></span></div>
<div style="position:absolute;left:55.90px;top:383.44px" class="cls_008"><span class="cls_008">functions int() and</span></div>
<div style="position:absolute;left:55.90px;top:421.36px" class="cls_008"><span class="cls_008">float()</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:289.21px;top:17.76px" class="cls_009"><span class="cls_009">Division</span></div>
<div style="position:absolute;left:44.68px;top:99.36px" class="cls_035"><span class="cls_035">• Division involving</span></div>
<div style="position:absolute;left:71.68px;top:128.40px" class="cls_035"><span class="cls_035">integers produces a</span></div>
<div style="position:absolute;left:428.23px;top:140.08px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_056">10 </span><span class="cls_057">/</span><span class="cls_056"> 2</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:428.23px;top:164.08px" class="cls_064"><span class="cls_064">5.0</span></div>
<div style="position:absolute;left:71.68px;top:157.44px" class="cls_035"><span class="cls_035">float result</span></div>
<div style="position:absolute;left:428.23px;top:188.08px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_056">9 </span><span class="cls_057">/</span><span class="cls_056"> 2</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:44.68px;top:193.44px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:71.68px;top:193.44px" class="cls_035"><span class="cls_035">This was different in</span></div>
<div style="position:absolute;left:428.23px;top:212.08px" class="cls_064"><span class="cls_064">4.5</span></div>
<div style="position:absolute;left:71.68px;top:221.52px" class="cls_035"><span class="cls_035">Python 2.x</span></div>
<div style="position:absolute;left:428.23px;top:236.08px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_056">99 </span><span class="cls_057">/ </span><span class="cls_056">100</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:428.23px;top:260.08px" class="cls_064"><span class="cls_064">0.99</span></div>
<div style="position:absolute;left:44.68px;top:257.52px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:71.68px;top:257.52px" class="cls_035"><span class="cls_035">If you want the</span></div>
<div style="position:absolute;left:428.23px;top:284.08px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_056">10.0 </span><span class="cls_057">/</span><span class="cls_056"> 2.0</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:71.68px;top:286.32px" class="cls_035"><span class="cls_035">remainder use the</span></div>
<div style="position:absolute;left:428.23px;top:308.08px" class="cls_056"><span class="cls_056">5.0</span></div>
<div style="position:absolute;left:71.68px;top:315.36px" class="cls_035"><span class="cls_035">modulo operator (%)</span></div>
<div style="position:absolute;left:428.23px;top:332.08px" class="cls_056"><span class="cls_056">>>> </span><span class="cls_059">print(</span><span class="cls_056">99.0 </span><span class="cls_057">/</span><span class="cls_056"> 100.0</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:428.23px;top:356.08px" class="cls_056"><span class="cls_056">0.99</span></div>
<div style="position:absolute;left:44.68px;top:351.36px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:71.68px;top:351.36px" class="cls_035"><span class="cls_035">If you want the integer</span></div>
<div style="position:absolute;left:71.68px;top:380.40px" class="cls_035"><span class="cls_035">quotient use the floor</span></div>
<div style="position:absolute;left:71.68px;top:409.44px" class="cls_035"><span class="cls_035">operator (//)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:193.21px;top:17.76px" class="cls_009"><span class="cls_009">String Conversions</span></div>
<div style="position:absolute;left:387.57px;top:130.00px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_066">sval</span><span class="cls_065"> = '123'</span></div>
<div style="position:absolute;left:387.57px;top:145.84px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_067">type</span><span class="cls_065">(</span><span class="cls_066">sval</span><span class="cls_065">)</span></div>
<div style="position:absolute;left:44.40px;top:146.16px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:71.40px;top:146.16px" class="cls_035"><span class="cls_035">Use int() and float()</span></div>
<div style="position:absolute;left:387.57px;top:162.88px" class="cls_065"><span class="cls_065">&lt;class 'str'></span></div>
<div style="position:absolute;left:387.57px;top:179.92px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_067">print</span><span class="cls_065">(</span><span class="cls_066">sval</span><span class="cls_069"> +</span><span class="cls_065"> 1)</span></div>
<div style="position:absolute;left:71.40px;top:175.20px" class="cls_035"><span class="cls_035">to convert strings to</span></div>
<div style="position:absolute;left:387.57px;top:196.96px" class="cls_068"><span class="cls_068">Traceback (most recent call last):</span></div>
<div style="position:absolute;left:387.57px;top:214.00px" class="cls_068"><span class="cls_068">File "&lt;stdin>", line 1, in &lt;module></span></div>
<div style="position:absolute;left:71.40px;top:204.24px" class="cls_035"><span class="cls_035">numeric</span></div>
<div style="position:absolute;left:387.57px;top:229.84px" class="cls_068"><span class="cls_068">TypeError: Can't convert 'int' object</span></div>
<div style="position:absolute;left:387.57px;top:246.88px" class="cls_068"><span class="cls_068">to str implicitly</span></div>
<div style="position:absolute;left:44.40px;top:240.24px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:71.40px;top:240.24px" class="cls_035"><span class="cls_035">Error if string does</span></div>
<div style="position:absolute;left:387.57px;top:263.92px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_066">ival</span><span class="cls_065"> = </span><span class="cls_067">int</span><span class="cls_065">(</span><span class="cls_066">sval</span><span class="cls_065">)</span></div>
<div style="position:absolute;left:387.57px;top:280.96px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_067">type</span><span class="cls_065">(</span><span class="cls_066">ival</span><span class="cls_065">)</span></div>
<div style="position:absolute;left:71.40px;top:268.32px" class="cls_035"><span class="cls_035">not contain numeric</span></div>
<div style="position:absolute;left:387.57px;top:298.00px" class="cls_065"><span class="cls_065">&lt;class 'int'></span></div>
<div style="position:absolute;left:71.40px;top:297.12px" class="cls_035"><span class="cls_035">values</span></div>
<div style="position:absolute;left:387.57px;top:313.84px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_067">print</span><span class="cls_065">(</span><span class="cls_066">ival</span><span class="cls_065"> + 1)</span></div>
<div style="position:absolute;left:387.57px;top:330.88px" class="cls_065"><span class="cls_065">124</span></div>
<div style="position:absolute;left:44.40px;top:333.12px" class="cls_035"><span class="cls_035">•</span></div>
<div style="position:absolute;left:71.40px;top:333.12px" class="cls_035"><span class="cls_035">Use str() to convert</span></div>
<div style="position:absolute;left:387.57px;top:347.92px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_066">nsv</span><span class="cls_065"> = 'hello bob'</span></div>
<div style="position:absolute;left:387.57px;top:364.96px" class="cls_065"><span class="cls_065">>>> </span><span class="cls_066">niv</span><span class="cls_065"> = </span><span class="cls_067">int</span><span class="cls_065">(</span><span class="cls_066">nsv</span><span class="cls_065">)</span></div>
<div style="position:absolute;left:71.40px;top:362.16px" class="cls_035"><span class="cls_035">numeric to string</span></div>
<div style="position:absolute;left:387.57px;top:382.00px" class="cls_068"><span class="cls_068">Traceback (most recent call last):</span></div>
<div style="position:absolute;left:387.57px;top:397.84px" class="cls_068"><span class="cls_068">File "&lt;stdin>", line 1, in &lt;module></span></div>
<div style="position:absolute;left:387.57px;top:414.88px" class="cls_068"><span class="cls_068">ValueError: invalid literal for int()</span></div>
<div style="position:absolute;left:387.57px;top:431.92px" class="cls_068"><span class="cls_068">with base 10: 'x'</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:198.21px;top:17.76px" class="cls_009"><span class="cls_009">User Input - String</span></div>
<div style="position:absolute;left:101.88px;top:106.48px" class="cls_008"><span class="cls_008">• input() function is used to read</span></div>
<div style="position:absolute;left:128.88px;top:145.36px" class="cls_008"><span class="cls_008">data from the user</span></div>
<div style="position:absolute;left:101.88px;top:191.44px" class="cls_008"><span class="cls_008">• input() function returns a string</span></div>
<div style="position:absolute;left:165.88px;top:315.04px" class="cls_055"><span class="cls_055">name</span><span class="cls_056"> = </span><span class="cls_059">input</span><span class="cls_056">('Who are you? ')</span></div>
<div style="position:absolute;left:165.88px;top:339.04px" class="cls_059"><span class="cls_059">print(</span><span class="cls_056">'Welcome', </span><span class="cls_055">name</span><span class="cls_059">)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:178.21px;top:17.76px" class="cls_009"><span class="cls_009">User Input - Numeric</span></div>
<div style="position:absolute;left:101.88px;top:106.48px" class="cls_008"><span class="cls_008">• Use input() to read user input as a</span></div>
<div style="position:absolute;left:128.88px;top:145.36px" class="cls_008"><span class="cls_008">string</span></div>
<div style="position:absolute;left:101.88px;top:191.44px" class="cls_008"><span class="cls_008">• Use int() to convert string to integer</span></div>
<div style="position:absolute;left:203.61px;top:320.08px" class="cls_055"><span class="cls_055">inp</span><span class="cls_056"> = </span><span class="cls_059">input(</span><span class="cls_056">'Europe floor?'</span><span class="cls_059">)</span></div>
<div style="position:absolute;left:203.61px;top:344.08px" class="cls_055"><span class="cls_055">usf</span><span class="cls_056"> = </span><span class="cls_059">int(</span><span class="cls_055">inp</span><span class="cls_059">)</span><span class="cls_057"> +</span><span class="cls_056"> 1</span></div>
<div style="position:absolute;left:203.61px;top:368.08px" class="cls_059"><span class="cls_059">print(</span><span class="cls_056">'US floor', </span><span class="cls_055">usf</span><span class="cls_059">)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:179.21px;top:17.76px" class="cls_009"><span class="cls_009">Comments in Python</span></div>
<div style="position:absolute;left:107.92px;top:147.12px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> Anything after a “#” is ignored by</span></div>
<div style="position:absolute;left:134.92px;top:186.00px" class="cls_014"><span class="cls_014">Python and hence is a comment</span></div>
<div style="position:absolute;left:107.92px;top:233.04px" class="cls_013"><span class="cls_013">§</span><span class="cls_014"> Why write comments in code:</span></div>
<div style="position:absolute;left:143.92px;top:279.04px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Describe the algorithm or statement</span></div>
<div style="position:absolute;left:143.92px;top:320.08px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Document code author and other</span></div>
<div style="position:absolute;left:166.42px;top:353.92px" class="cls_017"><span class="cls_017">details</span></div>
<div style="position:absolute;left:143.92px;top:394.00px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Commenting code is used in</span></div>
<div style="position:absolute;left:166.42px;top:429.04px" class="cls_017"><span class="cls_017">debugging programs</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:187.21px;top:17.76px" class="cls_009"><span class="cls_009">Comment Examples</span></div>
<div style="position:absolute;left:144.92px;top:76.40px" class="cls_061"><span class="cls_061"># Get the name of the file and open it</span></div>
<div style="position:absolute;left:144.92px;top:96.32px" class="cls_070"><span class="cls_070">name = input('Enter file:')</span></div>
<div style="position:absolute;left:144.92px;top:115.28px" class="cls_070"><span class="cls_070">handle = open(name, 'r')</span></div>
<div style="position:absolute;left:144.92px;top:153.20px" class="cls_061"><span class="cls_061"># Count word frequency</span></div>
<div style="position:absolute;left:144.92px;top:172.40px" class="cls_070"><span class="cls_070">counts = dict()</span></div>
<div style="position:absolute;left:144.92px;top:192.32px" class="cls_070"><span class="cls_070">for line in handle:</span></div>
<div style="position:absolute;left:183.42px;top:211.28px" class="cls_070"><span class="cls_070">words = line.split()</span></div>
<div style="position:absolute;left:183.42px;top:230.24px" class="cls_070"><span class="cls_070">for word in words:</span></div>
<div style="position:absolute;left:221.92px;top:249.20px" class="cls_070"><span class="cls_070">counts[word] = counts.get(word,0) + 1</span></div>
<div style="position:absolute;left:144.92px;top:288.32px" class="cls_061"><span class="cls_061"># Find the most common word</span></div>
<div style="position:absolute;left:144.92px;top:307.28px" class="cls_070"><span class="cls_070">bigcount = None</span></div>
<div style="position:absolute;left:144.92px;top:326.24px" class="cls_070"><span class="cls_070">bigword = None</span></div>
<div style="position:absolute;left:144.92px;top:345.20px" class="cls_070"><span class="cls_070">for word,count in counts.items():</span></div>
<div style="position:absolute;left:183.42px;top:364.40px" class="cls_070"><span class="cls_070">if bigcount is None or count > bigcount:</span></div>
<div style="position:absolute;left:221.92px;top:384.32px" class="cls_070"><span class="cls_070">bigword = word</span></div>
<div style="position:absolute;left:221.92px;top:403.28px" class="cls_070"><span class="cls_070">bigcount = count</span></div>
<div style="position:absolute;left:144.92px;top:441.20px" class="cls_061"><span class="cls_061"># All done</span></div>
<div style="position:absolute;left:144.92px;top:460.40px" class="cls_070"><span class="cls_070">print(bigword, bigcount)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.21px;top:17.76px" class="cls_009"><span class="cls_009">Summary</span></div>
<div style="position:absolute;left:107.92px;top:110.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Jupyter Notebook is the IDE for this class</span></div>
<div style="position:absolute;left:107.92px;top:146.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Constants and Variables</span></div>
<div style="position:absolute;left:107.92px;top:182.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Statements and Assignment</span></div>
<div style="position:absolute;left:107.92px;top:218.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Operators and Operands</span></div>
<div style="position:absolute;left:107.92px;top:254.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Expressions (numeric and string)</span></div>
<div style="position:absolute;left:107.92px;top:290.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Python type and type conversions</span></div>
<div style="position:absolute;left:107.92px;top:326.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> User input using the input() function</span></div>
<div style="position:absolute;left:107.92px;top:362.60px" class="cls_071"><span class="cls_071">§</span><span class="cls_072"> Comments in code</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.55px;top:17.76px" class="cls_009"><span class="cls_009">To Do List for Next Class</span></div>
<div style="position:absolute;left:107.92px;top:97.60px" class="cls_022"><span class="cls_022">• Read Chapters 2 and 3</span></div>
<div style="position:absolute;left:107.92px;top:150.64px" class="cls_022"><span class="cls_022">• Complete installation of Jupyter</span></div>
<div style="position:absolute;left:134.92px;top:196.48px" class="cls_022"><span class="cls_022">Notebook</span></div>
<div style="position:absolute;left:107.92px;top:250.48px" class="cls_022"><span class="cls_022">• Execute the code snippets in the</span></div>
<div style="position:absolute;left:134.92px;top:296.56px" class="cls_022"><span class="cls_022">book</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.21px;top:17.76px" class="cls_009"><span class="cls_009">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.21px;top:60.72px" class="cls_009"><span class="cls_009">tions</span></div>
<div style="position:absolute;left:107.92px;top:127.92px" class="cls_073"><span class="cls_073">§</span><span class="cls_074">  These slides are Copyright 2010- Charles R. Severance</span></div>
<div style="position:absolute;left:134.92px;top:148.80px" class="cls_074"><span class="cls_074">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.92px;top:170.88px" class="cls_074"><span class="cls_074">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.92px;top:192.00px" class="cls_074"><span class="cls_074">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.92px;top:213.84px" class="cls_074"><span class="cls_074">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.92px;top:235.92px" class="cls_074"><span class="cls_074">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.92px;top:256.80px" class="cls_074"><span class="cls_074">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.92px;top:278.88px" class="cls_074"><span class="cls_074">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.92px;top:300.00px" class="cls_074"><span class="cls_074">materials.</span></div>
<div style="position:absolute;left:107.92px;top:351.84px" class="cls_073"><span class="cls_073">§</span><span class="cls_074">  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.92px;top:373.92px" class="cls_074"><span class="cls_074">Michigan School of Information</span></div>
<div style="position:absolute;left:107.92px;top:426.00px" class="cls_073"><span class="cls_073">§</span><span class="cls_074">  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.92px;top:446.88px" class="cls_074"><span class="cls_074">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4Expressions/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:522.00px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>

</body>
</html>
