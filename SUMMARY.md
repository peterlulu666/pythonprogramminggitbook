# Summary

* [Introduction](README.md)
* [Lecture 1 Class Policy](lecture1ClassPolicy.md)
* [Lecture 2 Introduction](lecture2Introduction.md)
* [Lecture 3 Installation](lecture3Installation.md)
* [Lecture 4 Chapter 2 Expressions](lecture4Expressions.md)
* [Lecture 5 Chapter 3 Conditional](lecture5Conditional.md)
* [Lecture 6 Chapter 4 Functions](lecture6Functions.md)
* [Lecture 7 Print Format](lecture7PrintFormat.md)
* [Lecture 8 Chapter 5 Iteration](lecture8Iteration.md)
* [Chapter 6 Strings](chapter6string.md)
* [Chapter 7 Files](chapter7file.md)
* [Chapter 8](chapter8.md)
* [Chapter 9](chapter9.md)
* [Chapter 10](chapter10.md)
* [Chapter 10 Supplement](chapter10Supplement.md)
* [Chapter 11](chapter11.md)
* [Chapter 12](chapter12.md)
* [Chapter 13](chapter13.md)
* [Chapter 15](chapter15.md)

























