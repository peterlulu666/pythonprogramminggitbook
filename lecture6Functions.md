<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(17,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(30,71,124);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:19.0px;color:rgb(60,63,67);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:19.0px;color:rgb(60,63,67);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:19.0px;color:rgb(60,63,67);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:19.0px;color:rgb(60,63,67);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_051{font-family:Arial,serif;font-size:19.0px;color:rgb(60,63,67);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_051{font-family:Arial,serif;font-size:19.0px;color:rgb(60,63,67);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:"Calibri",serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:"Calibri",serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:19.0px;color:rgb(82,85,90);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:19.0px;color:rgb(82,85,90);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:15.1px;color:rgb(60,63,67);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:15.1px;color:rgb(60,63,67);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_052{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_052{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:17.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Courier New,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_022{font-family:Courier New,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_023{font-family:Courier New,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Courier New,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Courier New,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:18.1px;color:rgb(255,125,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:18.1px;color:rgb(243,242,243);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:18.1px;color:rgb(243,242,243);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_037{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_039{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_039{font-family:Arial,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_053{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: line-through}
div.cls_053{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_040{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,251,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_040{font-family:Courier New,serif;font-size:18.1px;color:rgb(0,251,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_043{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:27.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:27.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_045{font-family:Arial,serif;font-size:27.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_045{font-family:Arial,serif;font-size:27.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:24.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_046{font-family:Arial,serif;font-size:24.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:24.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_047{font-family:Arial,serif;font-size:24.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_048{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_048{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_049{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_049{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture6Functions/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.57px;top:185.68px" class="cls_002"><span class="cls_002">Chapter 4:</span></div>
<div style="position:absolute;left:44.57px;top:222.64px" class="cls_002"><span class="cls_002">Functions</span></div>
<div style="position:absolute;left:44.93px;top:353.52px" class="cls_003"><span class="cls_003">INSY 5336: Python Programming</span></div>
<div style="position:absolute;left:44.57px;top:407.28px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:44.57px;top:432.16px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:155.76px;top:222.40px" class="cls_007"><span class="cls_007">Chapter 4: Functions</span></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:200.16px;top:16.56px" class="cls_008"><span class="cls_008">Stored Statements</span></div>
<div style="position:absolute;left:48.21px;top:46.40px" class="cls_009"><span class="cls_009">def myprint():</span></div>
<div style="position:absolute;left:395.93px;top:78.72px" class="cls_010"><span class="cls_010">def </span><span class="cls_011">myprint():</span></div>
<div style="position:absolute;left:439.03px;top:99.60px" class="cls_010"><span class="cls_010">print</span><span class="cls_011">('Hello')</span></div>
<div style="position:absolute;left:219.34px;top:114.80px" class="cls_009"><span class="cls_009">print(’Hello’)</span></div>
<div style="position:absolute;left:439.03px;top:121.68px" class="cls_010"><span class="cls_010">print</span><span class="cls_011">(’World')</span></div>
<div style="position:absolute;left:216.84px;top:133.76px" class="cls_009"><span class="cls_009">print(‘World’)</span></div>
<div style="position:absolute;left:63.91px;top:167.84px" class="cls_009"><span class="cls_009">myprint()</span></div>
<div style="position:absolute;left:395.98px;top:165.60px" class="cls_011"><span class="cls_011">myprint()</span></div>
<div style="position:absolute;left:395.98px;top:186.72px" class="cls_010"><span class="cls_010">print</span><span class="cls_011">(’Hi')</span></div>
<div style="position:absolute;left:395.98px;top:208.56px" class="cls_011"><span class="cls_011">myprint()</span></div>
<div style="position:absolute;left:395.98px;top:230.64px" class="cls_011"><span class="cls_011">Print(‘Finished’)</span></div>
<div style="position:absolute;left:62.90px;top:250.40px" class="cls_009"><span class="cls_009">print(‘Hi’)</span></div>
<div style="position:absolute;left:234.29px;top:288.16px" class="cls_012"><span class="cls_012">• Such reusable code segments</span></div>
<div style="position:absolute;left:67.54px;top:339.44px" class="cls_009"><span class="cls_009">myprint()</span></div>
<div style="position:absolute;left:261.29px;top:327.04px" class="cls_012"><span class="cls_012">are called functions</span></div>
<div style="position:absolute;left:234.29px;top:373.12px" class="cls_012"><span class="cls_012">• Functions can be built in or</span></div>
<div style="position:absolute;left:261.29px;top:411.04px" class="cls_012"><span class="cls_012">user defined</span></div>
<div style="position:absolute;left:40.45px;top:434.96px" class="cls_009"><span class="cls_009">print(’Finished')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:80.66px;top:38.80px" class="cls_013"><span class="cls_013">Functions</span></div>
<div style="position:absolute;left:81.97px;top:83.24px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  </span><span class="cls_051">Function:</span><span class="cls_016"> a block of prewritten code that performs an operation.</span></div>
<div style="position:absolute;left:108.97px;top:103.40px" class="cls_016"><span class="cls_016">It</span><span class="cls_014"> runs when it is called by its function name. You can pass</span></div>
<div style="position:absolute;left:108.97px;top:124.28px" class="cls_014"><span class="cls_014">data, known as parameters, into a</span><span class="cls_017"> function</span><span class="cls_014">. A</span><span class="cls_017"> function</span><span class="cls_014"> can</span></div>
<div style="position:absolute;left:108.97px;top:145.40px" class="cls_014"><span class="cls_014">return data as a result.</span></div>
<div style="position:absolute;left:117.97px;top:169.32px" class="cls_018"><span class="cls_018">-  Parameters are used to pass data into the function</span></div>
<div style="position:absolute;left:117.97px;top:189.24px" class="cls_018"><span class="cls_018">-  Arguments are data given to a function</span></div>
<div style="position:absolute;left:117.97px;top:209.40px" class="cls_018"><span class="cls_018">-  E.g., print(), int(), float(), str()</span></div>
<div style="position:absolute;left:81.97px;top:237.28px" class="cls_019"><span class="cls_019">•</span><span class="cls_020">  </span><span class="cls_052">Nested function call</span><span class="cls_019">: general format:</span></div>
<div style="position:absolute;left:117.97px;top:267.20px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  function1</span><span class="cls_023">(</span><span class="cls_022">function2</span><span class="cls_023">(</span><span class="cls_022">argument</span><span class="cls_023">))</span></div>
<div style="position:absolute;left:153.97px;top:296.36px" class="cls_024"><span class="cls_024">•</span><span class="cls_016"> value returned by function2 is passed to function1</span></div>
<div style="position:absolute;left:81.97px;top:348.40px" class="cls_019"><span class="cls_019">•  Function only works if values passed to parameters are</span></div>
<div style="position:absolute;left:108.97px;top:370.24px" class="cls_019"><span class="cls_019">valid types, otherwise, throw exceptions.</span></div>
<div style="position:absolute;left:117.97px;top:396.28px" class="cls_021"><span class="cls_021">- Refer to the previous data type conversion examples</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:195.72px;top:16.56px" class="cls_008"><span class="cls_008">Types of Functions</span></div>
<div style="position:absolute;left:35.90px;top:140.08px" class="cls_012"><span class="cls_012">Two kinds of functions in Python:</span></div>
<div style="position:absolute;left:35.90px;top:233.92px" class="cls_012"><span class="cls_012">• Built-in functions provided by Python. E.g.</span></div>
<div style="position:absolute;left:62.90px;top:273.04px" class="cls_012"><span class="cls_012">print(), input(), type(), float(), int(), etc.</span></div>
<div style="position:absolute;left:71.85px;top:318.08px" class="cls_025"><span class="cls_025">- built-in function names are treated as reserved</span></div>
<div style="position:absolute;left:94.45px;top:351.92px" class="cls_025"><span class="cls_025">words</span></div>
<div style="position:absolute;left:35.90px;top:393.04px" class="cls_012"><span class="cls_012">• User defined functions</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:261.38px;top:16.56px" class="cls_008"><span class="cls_008">Max Function</span></div>
<div style="position:absolute;left:189.05px;top:100.08px" class="cls_026"><span class="cls_026">>>></span><span class="cls_027"> big </span><span class="cls_026">=</span><span class="cls_028"> max</span><span class="cls_026">('Hello  world')</span></div>
<div style="position:absolute;left:189.05px;top:121.92px" class="cls_026"><span class="cls_026">>>></span><span class="cls_010"> print</span><span class="cls_026">(</span><span class="cls_027">big</span><span class="cls_026">)</span></div>
<div style="position:absolute;left:189.05px;top:143.52px" class="cls_026"><span class="cls_026">w</span></div>
<div style="position:absolute;left:88.58px;top:253.68px" class="cls_029"><span class="cls_029">'Hello world'</span></div>
<div style="position:absolute;left:563.89px;top:253.68px" class="cls_031"><span class="cls_031">'w'</span></div>
<div style="position:absolute;left:345.80px;top:256.80px" class="cls_030"><span class="cls_030">max()</span></div>
<div style="position:absolute;left:100.68px;top:274.80px" class="cls_032"><span class="cls_032">(a string)</span></div>
<div style="position:absolute;left:538.37px;top:274.80px" class="cls_030"><span class="cls_030">(a string)</span></div>
<div style="position:absolute;left:337.30px;top:277.92px" class="cls_030"><span class="cls_030">function</span></div>
<div style="position:absolute;left:91.06px;top:356.32px" class="cls_012"><span class="cls_012">• A built-in function is a black box of</span></div>
<div style="position:absolute;left:118.06px;top:395.44px" class="cls_012"><span class="cls_012">code that perform a certain task</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:229.34px;top:16.56px" class="cls_008"><span class="cls_008">Built-in functions</span></div>
<div style="position:absolute;left:107.95px;top:426.36px" class="cls_033"><span class="cls_033">• Built-in functions are always available</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:175.32px;top:16.56px" class="cls_008"><span class="cls_008">User Defined Functions</span></div>
<div style="position:absolute;left:61.39px;top:148.48px" class="cls_012"><span class="cls_012">• Defined by us</span></div>
<div style="position:absolute;left:61.39px;top:195.52px" class="cls_012"><span class="cls_012">• Defined by someone else</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:170.28px;top:16.56px" class="cls_008"><span class="cls_008">Functions defined by us</span></div>
<div style="position:absolute;left:61.39px;top:93.36px" class="cls_034"><span class="cls_034">• We create a new function using the def</span></div>
<div style="position:absolute;left:88.39px;top:125.28px" class="cls_034"><span class="cls_034">keyword followed by optional parameters</span></div>
<div style="position:absolute;left:88.39px;top:157.20px" class="cls_034"><span class="cls_034">in parentheses</span></div>
<div style="position:absolute;left:61.39px;top:196.32px" class="cls_034"><span class="cls_034">• We indent the body of the function</span></div>
<div style="position:absolute;left:61.39px;top:236.40px" class="cls_034"><span class="cls_034">• This defines the function but does not</span></div>
<div style="position:absolute;left:88.39px;top:268.32px" class="cls_034"><span class="cls_034">execute the body of the function</span></div>
<div style="position:absolute;left:69.48px;top:372.72px" class="cls_010"><span class="cls_010">def</span><span class="cls_027"> print_lyrics</span><span class="cls_026">():</span></div>
<div style="position:absolute;left:112.53px;top:393.84px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">("I'm a lumberjack, and I'm okay.")</span></div>
<div style="position:absolute;left:112.53px;top:415.68px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('I sleep all night and I work all day.')</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:236.30px;top:16.56px" class="cls_008"><span class="cls_008">Using a function</span></div>
<div style="position:absolute;left:61.39px;top:96.88px" class="cls_012"><span class="cls_012">• Define the function in the program</span></div>
<div style="position:absolute;left:61.39px;top:143.68px" class="cls_012"><span class="cls_012">• Invoke it as many times as needed</span></div>
<div style="position:absolute;left:118.92px;top:233.28px" class="cls_027"><span class="cls_027">x </span><span class="cls_026">= 5</span></div>
<div style="position:absolute;left:118.92px;top:254.16px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('Hello')</span></div>
<div style="position:absolute;left:118.92px;top:298.08px" class="cls_010"><span class="cls_010">def</span><span class="cls_027"> print_lyrics</span><span class="cls_026">():</span></div>
<div style="position:absolute;left:161.97px;top:320.16px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">("I'm a lumberjack, and I'm okay.")</span></div>
<div style="position:absolute;left:161.97px;top:341.28px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('I sleep all night and I work all</span></div>
<div style="position:absolute;left:118.92px;top:363.12px" class="cls_026"><span class="cls_026">day.')</span></div>
<div style="position:absolute;left:118.92px;top:407.28px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('Yo')</span></div>
<div style="position:absolute;left:118.92px;top:428.16px" class="cls_027"><span class="cls_027">x </span><span class="cls_026">= </span><span class="cls_027">x </span><span class="cls_035">+ </span><span class="cls_026">2</span></div>
<div style="position:absolute;left:118.92px;top:450.24px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">(</span><span class="cls_027">x</span><span class="cls_026">)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:201.86px;top:16.56px" class="cls_008"><span class="cls_008">Function Arguments</span></div>
<div style="position:absolute;left:61.39px;top:90.84px" class="cls_033"><span class="cls_033">• An argument is a value we pass into the</span></div>
<div style="position:absolute;left:88.39px;top:116.76px" class="cls_033"><span class="cls_033">function as its input when we call the function</span></div>
<div style="position:absolute;left:61.39px;top:149.88px" class="cls_033"><span class="cls_033">• We use arguments so we can direct the</span></div>
<div style="position:absolute;left:88.39px;top:175.80px" class="cls_033"><span class="cls_033">function to do different kinds of work when we</span></div>
<div style="position:absolute;left:88.39px;top:201.72px" class="cls_033"><span class="cls_033">call it at different times</span></div>
<div style="position:absolute;left:61.39px;top:233.88px" class="cls_033"><span class="cls_033">• We put the arguments in parentheses after the</span></div>
<div style="position:absolute;left:88.39px;top:258.84px" class="cls_033"><span class="cls_033">name of the function</span></div>
<div style="position:absolute;left:261.67px;top:328.80px" class="cls_031"><span class="cls_031">big </span><span class="cls_030">= </span><span class="cls_036">max</span><span class="cls_030">(</span><span class="cls_029">'Hello world'</span><span class="cls_030">)</span></div>
<div style="position:absolute;left:470.09px;top:433.16px" class="cls_037"><span class="cls_037">argument</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:198.14px;top:16.56px" class="cls_008"><span class="cls_008">Function Parameters</span></div>
<div style="position:absolute;left:386.38px;top:95.52px" class="cls_026"><span class="cls_026">>>> </span><span class="cls_010">def</span><span class="cls_027"> greet</span><span class="cls_026">(</span><span class="cls_035">lang</span><span class="cls_026">):</span></div>
<div style="position:absolute;left:32.62px;top:93.84px" class="cls_034"><span class="cls_034">•</span></div>
<div style="position:absolute;left:59.62px;top:93.84px" class="cls_034"><span class="cls_034">A parameter is a</span></div>
<div style="position:absolute;left:472.43px;top:116.40px" class="cls_010"><span class="cls_010">if </span><span class="cls_035">lang </span><span class="cls_026">== 'es':</span></div>
<div style="position:absolute;left:59.62px;top:125.76px" class="cls_034"><span class="cls_034">variable which we use</span></div>
<div style="position:absolute;left:504.58px;top:138.48px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('Hola')</span></div>
<div style="position:absolute;left:472.43px;top:160.56px" class="cls_010"><span class="cls_010">elif </span><span class="cls_035">lang </span><span class="cls_026">== 'fr':</span></div>
<div style="position:absolute;left:59.62px;top:157.92px" class="cls_034"><span class="cls_034">in the function</span></div>
<div style="position:absolute;left:504.58px;top:181.44px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('Bonjour')</span></div>
<div style="position:absolute;left:59.62px;top:190.80px" class="cls_034"><span class="cls_034">definition.</span></div>
<div style="position:absolute;left:472.43px;top:203.52px" class="cls_010"><span class="cls_010">else:</span></div>
<div style="position:absolute;left:504.58px;top:224.40px" class="cls_010"><span class="cls_010">print</span><span class="cls_026">('Hello')</span></div>
<div style="position:absolute;left:32.62px;top:229.92px" class="cls_034"><span class="cls_034">•</span></div>
<div style="position:absolute;left:59.62px;top:229.92px" class="cls_034"><span class="cls_034">It is a “handle” that</span></div>
<div style="position:absolute;left:386.38px;top:268.56px" class="cls_026"><span class="cls_026">>>></span><span class="cls_027"> greet</span><span class="cls_026">(</span><span class="cls_011">'en'</span><span class="cls_026">)</span></div>
<div style="position:absolute;left:59.62px;top:262.80px" class="cls_034"><span class="cls_034">allows the code in the</span></div>
<div style="position:absolute;left:386.38px;top:289.44px" class="cls_026"><span class="cls_026">Hello</span></div>
<div style="position:absolute;left:59.62px;top:294.96px" class="cls_034"><span class="cls_034">function to access the</span></div>
<div style="position:absolute;left:386.38px;top:311.52px" class="cls_026"><span class="cls_026">>>></span><span class="cls_027"> greet</span><span class="cls_026">(</span><span class="cls_011">'es'</span><span class="cls_026">)</span></div>
<div style="position:absolute;left:386.38px;top:332.40px" class="cls_026"><span class="cls_026">Hola</span></div>
<div style="position:absolute;left:59.62px;top:326.88px" class="cls_034"><span class="cls_034">arguments for a</span></div>
<div style="position:absolute;left:386.38px;top:354.48px" class="cls_026"><span class="cls_026">>>></span><span class="cls_027"> greet</span><span class="cls_026">(</span><span class="cls_011">'fr'</span><span class="cls_026">)</span></div>
<div style="position:absolute;left:59.62px;top:359.76px" class="cls_034"><span class="cls_034">particular function</span></div>
<div style="position:absolute;left:386.38px;top:376.56px" class="cls_026"><span class="cls_026">Bonjour</span></div>
<div style="position:absolute;left:386.38px;top:397.44px" class="cls_026"><span class="cls_026">>>></span></div>
<div style="position:absolute;left:59.62px;top:391.92px" class="cls_034"><span class="cls_034">invocation.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:257.30px;top:16.56px" class="cls_008"><span class="cls_008">Return Values</span></div>
<div style="position:absolute;left:14.59px;top:90.84px" class="cls_033"><span class="cls_033">•</span></div>
<div style="position:absolute;left:41.59px;top:90.84px" class="cls_033"><span class="cls_033">Often a function will take</span></div>
<div style="position:absolute;left:363.89px;top:115.92px" class="cls_026"><span class="cls_026">>>> </span><span class="cls_010">def</span><span class="cls_027"> greet</span><span class="cls_026">(</span><span class="cls_035">lang</span><span class="cls_026">):</span></div>
<div style="position:absolute;left:41.59px;top:116.76px" class="cls_033"><span class="cls_033">its arguments, do some</span></div>
<div style="position:absolute;left:449.90px;top:137.52px" class="cls_010"><span class="cls_010">if </span><span class="cls_035">lang </span><span class="cls_026">== 'es':</span></div>
<div style="position:absolute;left:42.45px;top:139.80px" class="cls_033"><span class="cls_033">computation, and return</span></div>
<div style="position:absolute;left:492.90px;top:158.64px" class="cls_010"><span class="cls_010">return</span><span class="cls_028"> 'Hola'</span></div>
<div style="position:absolute;left:42.45px;top:160.68px" class="cls_033"><span class="cls_033">a value</span></div>
<div style="position:absolute;left:449.90px;top:180.48px" class="cls_010"><span class="cls_010">elif </span><span class="cls_035">lang </span><span class="cls_026">== 'fr':</span></div>
<div style="position:absolute;left:492.90px;top:202.56px" class="cls_010"><span class="cls_010">return</span><span class="cls_028"> 'Bonjour'</span></div>
<div style="position:absolute;left:14.59px;top:198.84px" class="cls_033"><span class="cls_033">•</span></div>
<div style="position:absolute;left:41.59px;top:198.84px" class="cls_033"><span class="cls_033">The</span><span class="cls_038"> return</span><span class="cls_033"> keyword is</span></div>
<div style="position:absolute;left:449.95px;top:223.44px" class="cls_010"><span class="cls_010">else:</span></div>
<div style="position:absolute;left:42.45px;top:221.88px" class="cls_033"><span class="cls_033">used for this.</span></div>
<div style="position:absolute;left:492.90px;top:245.52px" class="cls_010"><span class="cls_010">return</span><span class="cls_028"> 'Hello'</span></div>
<div style="position:absolute;left:14.59px;top:260.76px" class="cls_033"><span class="cls_033">•</span></div>
<div style="position:absolute;left:41.59px;top:260.76px" class="cls_033"><span class="cls_033">A “fruitful” function is one</span></div>
<div style="position:absolute;left:363.89px;top:288.72px" class="cls_026"><span class="cls_026">>>></span><span class="cls_010"> print</span><span class="cls_026">(</span><span class="cls_027">greet</span><span class="cls_026">(</span><span class="cls_011">'en'</span><span class="cls_026">),'Glenn')</span></div>
<div style="position:absolute;left:41.59px;top:286.68px" class="cls_033"><span class="cls_033">that produces a result (or</span></div>
<div style="position:absolute;left:363.89px;top:309.84px" class="cls_026"><span class="cls_026">Hello Glenn</span></div>
<div style="position:absolute;left:41.59px;top:312.84px" class="cls_033"><span class="cls_033">return value)</span></div>
<div style="position:absolute;left:363.89px;top:331.68px" class="cls_026"><span class="cls_026">>>> </span><span class="cls_010">print</span><span class="cls_026">(</span><span class="cls_027">greet</span><span class="cls_026">(</span><span class="cls_011">'es'</span><span class="cls_026">),'Sally')</span></div>
<div style="position:absolute;left:14.59px;top:344.76px" class="cls_033"><span class="cls_033">•</span></div>
<div style="position:absolute;left:41.59px;top:344.76px" class="cls_033"><span class="cls_033">The return statement</span></div>
<div style="position:absolute;left:363.89px;top:353.76px" class="cls_026"><span class="cls_026">Hola Sally</span></div>
<div style="position:absolute;left:363.89px;top:374.64px" class="cls_026"><span class="cls_026">>>> </span><span class="cls_010">print</span><span class="cls_026">(</span><span class="cls_027">greet</span><span class="cls_026">(</span><span class="cls_011">'fr'</span><span class="cls_026">),'Michael')</span></div>
<div style="position:absolute;left:41.59px;top:369.72px" class="cls_033"><span class="cls_033">ends the function</span></div>
<div style="position:absolute;left:363.89px;top:396.72px" class="cls_026"><span class="cls_026">Bonjour Michael</span></div>
<div style="position:absolute;left:41.59px;top:395.88px" class="cls_033"><span class="cls_033">execution and “sends</span></div>
<div style="position:absolute;left:363.89px;top:417.84px" class="cls_026"><span class="cls_026">>>></span></div>
<div style="position:absolute;left:41.59px;top:421.80px" class="cls_033"><span class="cls_033">back” the result of the</span></div>
<div style="position:absolute;left:41.59px;top:447.72px" class="cls_033"><span class="cls_033">function</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:54.74px;top:16.56px" class="cls_008"><span class="cls_008">Arguments, Parameters & Results</span></div>
<div style="position:absolute;left:44.87px;top:115.92px" class="cls_026"><span class="cls_026">>>></span><span class="cls_027"> big </span><span class="cls_026">=</span><span class="cls_028"> max</span><span class="cls_026">(</span><span class="cls_027">'Hello  world'</span><span class="cls_026">)</span></div>
<div style="position:absolute;left:44.87px;top:138.00px" class="cls_026"><span class="cls_026">>>></span><span class="cls_010"> print</span><span class="cls_026">(</span><span class="cls_027">big</span><span class="cls_026">)</span></div>
<div style="position:absolute;left:44.87px;top:159.60px" class="cls_027"><span class="cls_027">w</span></div>
<div style="position:absolute;left:467.21px;top:165.12px" class="cls_039"><span class="cls_039">Parameter</span></div>
<div style="position:absolute;left:324.86px;top:267.12px" class="cls_010"><span class="cls_010">def</span><span class="cls_026"> max(</span><span class="cls_035">in</span><span class="cls_053">p</span><span class="cls_026">):</span></div>
<div style="position:absolute;left:357.11px;top:288.00px" class="cls_026"><span class="cls_026">blah</span></div>
<div style="position:absolute;left:357.11px;top:310.08px" class="cls_026"><span class="cls_026">blah</span></div>
<div style="position:absolute;left:82.33px;top:324.24px" class="cls_029"><span class="cls_029">'Hello world'</span></div>
<div style="position:absolute;left:630.61px;top:323.04px" class="cls_031"><span class="cls_031">'w'</span></div>
<div style="position:absolute;left:357.11px;top:332.16px" class="cls_010"><span class="cls_010">for </span><span class="cls_026">x </span><span class="cls_010">in</span><span class="cls_040"> inp</span><span class="cls_026">:</span></div>
<div style="position:absolute;left:378.61px;top:353.04px" class="cls_026"><span class="cls_026">blah</span></div>
<div style="position:absolute;left:378.56px;top:375.12px" class="cls_026"><span class="cls_026">blah</span></div>
<div style="position:absolute;left:612.56px;top:392.88px" class="cls_031"><span class="cls_031">Result</span></div>
<div style="position:absolute;left:26.92px;top:396.96px" class="cls_029"><span class="cls_029">Argument</span></div>
<div style="position:absolute;left:357.11px;top:396.00px" class="cls_027"><span class="cls_027">return 'w'</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:201.14px;top:16.56px" class="cls_008"><span class="cls_008">Optional Parameters</span></div>
<div style="position:absolute;left:185.88px;top:100.32px" class="cls_026"><span class="cls_026">def multiply(x, y, z=1, a=1):</span></div>
<div style="position:absolute;left:221.88px;top:121.44px" class="cls_026"><span class="cls_026">return(x*y*z*a)</span></div>
<div style="position:absolute;left:185.88px;top:165.36px" class="cls_026"><span class="cls_026">print(multiply(2,3))</span></div>
<div style="position:absolute;left:185.88px;top:187.44px" class="cls_026"><span class="cls_026">print(multiply(2,3,2))</span></div>
<div style="position:absolute;left:185.88px;top:208.32px" class="cls_026"><span class="cls_026">print(multiply(2))</span></div>
<div style="position:absolute;left:14.59px;top:264.24px" class="cls_034"><span class="cls_034">• A function can have a list of parameters, some</span></div>
<div style="position:absolute;left:41.59px;top:293.28px" class="cls_034"><span class="cls_034">of which have a default value</span></div>
<div style="position:absolute;left:14.59px;top:329.28px" class="cls_034"><span class="cls_034">• These default valued parameters are called</span></div>
<div style="position:absolute;left:41.59px;top:358.32px" class="cls_034"><span class="cls_034">optional parameters</span></div>
<div style="position:absolute;left:14.59px;top:394.32px" class="cls_034"><span class="cls_034">• All required parameters must be listed before</span></div>
<div style="position:absolute;left:41.59px;top:423.36px" class="cls_034"><span class="cls_034">any optional parameters</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:126.72px;top:16.56px" class="cls_008"><span class="cls_008">To function or not to function</span></div>
<div style="position:absolute;left:61.39px;top:93.36px" class="cls_034"><span class="cls_034">• Organize your code into “paragraphs” -</span></div>
<div style="position:absolute;left:88.39px;top:125.28px" class="cls_034"><span class="cls_034">capture a complete thought and “name it”</span></div>
<div style="position:absolute;left:61.39px;top:164.40px" class="cls_034"><span class="cls_034">• Don’t repeat yourself - make it work once</span></div>
<div style="position:absolute;left:88.39px;top:196.32px" class="cls_034"><span class="cls_034">and then reuse it</span></div>
<div style="position:absolute;left:61.39px;top:235.20px" class="cls_034"><span class="cls_034">• If something gets too long or complex,</span></div>
<div style="position:absolute;left:88.39px;top:267.36px" class="cls_034"><span class="cls_034">break it up into logical chunks and put</span></div>
<div style="position:absolute;left:88.39px;top:299.28px" class="cls_034"><span class="cls_034">those chunks in functions</span></div>
<div style="position:absolute;left:61.39px;top:339.36px" class="cls_034"><span class="cls_034">• Make a library of common stuff that you</span></div>
<div style="position:absolute;left:88.39px;top:371.28px" class="cls_034"><span class="cls_034">do over and over - perhaps share this</span></div>
<div style="position:absolute;left:88.39px;top:403.20px" class="cls_034"><span class="cls_034">with your friends...</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:239.18px;top:16.56px" class="cls_008"><span class="cls_008">Some Examples</span></div>
<div style="position:absolute;left:171.48px;top:81.12px" class="cls_026"><span class="cls_026">def proc(x):</span></div>
<div style="position:absolute;left:207.48px;top:102.24px" class="cls_026"><span class="cls_026">return(x + 2)</span></div>
<div style="position:absolute;left:171.48px;top:124.32px" class="cls_026"><span class="cls_026">def main():</span></div>
<div style="position:absolute;left:207.48px;top:146.16px" class="cls_026"><span class="cls_026">x = proc(5)</span></div>
<div style="position:absolute;left:207.48px;top:167.28px" class="cls_026"><span class="cls_026">print(x)</span></div>
<div style="position:absolute;left:171.48px;top:189.12px" class="cls_026"><span class="cls_026">main()</span></div>
<div style="position:absolute;left:14.59px;top:285.28px" class="cls_012"><span class="cls_012">• It is common to write a main() function to call</span></div>
<div style="position:absolute;left:41.59px;top:320.32px" class="cls_012"><span class="cls_012">other functions</span></div>
<div style="position:absolute;left:14.59px;top:363.28px" class="cls_012"><span class="cls_012">• The function definitions and calls can be in</span></div>
<div style="position:absolute;left:41.59px;top:398.32px" class="cls_012"><span class="cls_012">different Jupyter Notebook cells</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.22px;top:16.56px" class="cls_008"><span class="cls_008">Example Problem</span></div>
<div style="position:absolute;left:28.03px;top:78.56px" class="cls_041"><span class="cls_041">A nutritionist who works for a fitness club helps members by</span></div>
<div style="position:absolute;left:28.03px;top:99.68px" class="cls_041"><span class="cls_041">evaluating their diets. As part of her evaluation, she asks members</span></div>
<div style="position:absolute;left:28.03px;top:120.56px" class="cls_041"><span class="cls_041">for the number of fat grams, protein grams and carbohydrate grams</span></div>
<div style="position:absolute;left:28.03px;top:141.68px" class="cls_041"><span class="cls_041">that they consumed in a day. Then, she calculates the number of</span></div>
<div style="position:absolute;left:28.03px;top:163.52px" class="cls_041"><span class="cls_041">calories that result from the fat, using the following formula:</span></div>
<div style="position:absolute;left:64.03px;top:189.52px" class="cls_020"><span class="cls_020">calories from fat = fat grams * 9</span></div>
<div style="position:absolute;left:28.03px;top:213.68px" class="cls_041"><span class="cls_041">Next, she calculates the number of calories that result from the</span></div>
<div style="position:absolute;left:28.03px;top:234.56px" class="cls_041"><span class="cls_041">protein, using the following formula:</span></div>
<div style="position:absolute;left:64.03px;top:260.56px" class="cls_020"><span class="cls_020">calories from protein = protein grams * 4</span></div>
<div style="position:absolute;left:28.03px;top:284.48px" class="cls_041"><span class="cls_041">Finally, she calculates the number of calories that result from the</span></div>
<div style="position:absolute;left:28.03px;top:305.60px" class="cls_041"><span class="cls_041">carbohydrates, using the following formula:</span></div>
<div style="position:absolute;left:64.03px;top:332.56px" class="cls_020"><span class="cls_020">calories from carbs = carb grams * 4</span></div>
<div style="position:absolute;left:28.03px;top:353.60px" class="cls_041"><span class="cls_041">Define a function that will accept the fat grams and carb grams</span></div>
<div style="position:absolute;left:28.03px;top:377.60px" class="cls_041"><span class="cls_041">consumed as input and display the total calories consumed.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.34px;top:16.56px" class="cls_008"><span class="cls_008">Example Solution</span></div>
<div style="position:absolute;left:28.03px;top:79.68px" class="cls_042"><span class="cls_042">A nutritionist who works for a fitness club helps members by evaluating their diets.</span></div>
<div style="position:absolute;left:28.03px;top:96.72px" class="cls_042"><span class="cls_042">As part of her evaluation, she asks members for the number of fat grams, protein</span></div>
<div style="position:absolute;left:28.03px;top:114.72px" class="cls_042"><span class="cls_042">grams and carbohydrate grams that they consumed in a day. Then, she calculates</span></div>
<div style="position:absolute;left:28.03px;top:131.76px" class="cls_042"><span class="cls_042">the number of calories that result from the fat, using the following formula:</span></div>
<div style="position:absolute;left:64.03px;top:153.72px" class="cls_043"><span class="cls_043">calories from fat = fat grams * 9</span></div>
<div style="position:absolute;left:28.03px;top:170.88px" class="cls_042"><span class="cls_042">Next, she calculates the number of calories that result from the protein, using the</span></div>
<div style="position:absolute;left:28.03px;top:188.88px" class="cls_042"><span class="cls_042">following formula:</span></div>
<div style="position:absolute;left:64.03px;top:209.88px" class="cls_043"><span class="cls_043">calories from protein = protein grams * 4</span></div>
<div style="position:absolute;left:28.03px;top:227.76px" class="cls_042"><span class="cls_042">Finally, she calculates the number of calories that result from the carbohydrates,</span></div>
<div style="position:absolute;left:28.03px;top:244.80px" class="cls_042"><span class="cls_042">using the following formula:</span></div>
<div style="position:absolute;left:64.03px;top:266.76px" class="cls_043"><span class="cls_043">calories from carbs = carb grams * 4</span></div>
<div style="position:absolute;left:28.03px;top:283.68px" class="cls_042"><span class="cls_042">Define a function that will accept the fat grams and carb grams consumed as input</span></div>
<div style="position:absolute;left:28.03px;top:301.68px" class="cls_042"><span class="cls_042">and display the total calories consumed.</span></div>
<div style="position:absolute;left:83.28px;top:363.36px" class="cls_026"><span class="cls_026">def compute_consumption(fat_gm, pro_gms, carb_gms):</span></div>
<div style="position:absolute;left:126.33px;top:384.24px" class="cls_026"><span class="cls_026">return (fat_gm * 9 + (carb_gm +pro_gms) * 4)</span></div>
<div style="position:absolute;left:83.28px;top:428.40px" class="cls_026"><span class="cls_026">compute_consumption(20,25,80)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:141.24px;top:16.56px" class="cls_008"><span class="cls_008">Functions written by others</span></div>
<div style="position:absolute;left:61.39px;top:96.88px" class="cls_012"><span class="cls_012">• Functions written by others can be</span></div>
<div style="position:absolute;left:88.39px;top:135.76px" class="cls_012"><span class="cls_012">used in our programs by the import</span></div>
<div style="position:absolute;left:88.39px;top:173.68px" class="cls_012"><span class="cls_012">command</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:176.28px;top:16.56px" class="cls_008"><span class="cls_008">Modules and Functions</span></div>
<div style="position:absolute;left:70.01px;top:92.12px" class="cls_037"><span class="cls_037">fibo.py file</span></div>
<div style="position:absolute;left:361.30px;top:92.12px" class="cls_037"><span class="cls_037">Main program</span></div>
<div style="position:absolute;left:17.69px;top:126.48px" class="cls_042"><span class="cls_042">def fib(n):</span></div>
<div style="position:absolute;left:113.64px;top:126.48px" class="cls_042"><span class="cls_042"># write Fibonacci</span></div>
<div style="position:absolute;left:335.40px;top:127.20px" class="cls_042"><span class="cls_042">import fibo</span></div>
<div style="position:absolute;left:17.69px;top:143.28px" class="cls_042"><span class="cls_042">series up to n</span></div>
<div style="position:absolute;left:335.40px;top:148.08px" class="cls_042"><span class="cls_042">fibo.fib(1000)</span></div>
<div style="position:absolute;left:37.69px;top:165.36px" class="cls_042"><span class="cls_042">a, b = 0, 1</span></div>
<div style="position:absolute;left:335.40px;top:170.16px" class="cls_042"><span class="cls_042">1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987</span></div>
<div style="position:absolute;left:37.68px;top:187.68px" class="cls_042"><span class="cls_042">while b &lt; n:</span></div>
<div style="position:absolute;left:335.40px;top:192.00px" class="cls_042"><span class="cls_042">fibo.fib2(100)</span></div>
<div style="position:absolute;left:57.63px;top:208.56px" class="cls_042"><span class="cls_042">print(b, end=' ')</span></div>
<div style="position:absolute;left:335.40px;top:212.88px" class="cls_042"><span class="cls_042">[1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]</span></div>
<div style="position:absolute;left:57.63px;top:230.64px" class="cls_042"><span class="cls_042">a, b = b, a+b</span></div>
<div style="position:absolute;left:37.73px;top:251.52px" class="cls_042"><span class="cls_042">print()</span></div>
<div style="position:absolute;left:17.69px;top:294.96px" class="cls_042"><span class="cls_042">def fib2(n):</span></div>
<div style="position:absolute;left:118.64px;top:294.96px" class="cls_042"><span class="cls_042"># return Fibonacci</span></div>
<div style="position:absolute;left:17.69px;top:312.00px" class="cls_042"><span class="cls_042">series up to n</span></div>
<div style="position:absolute;left:274.85px;top:303.48px" class="cls_033"><span class="cls_033">• Write the module in a file</span></div>
<div style="position:absolute;left:37.69px;top:333.84px" class="cls_042"><span class="cls_042">result = []</span></div>
<div style="position:absolute;left:274.85px;top:339.48px" class="cls_033"><span class="cls_033">• The module can contain functions</span></div>
<div style="position:absolute;left:37.69px;top:355.92px" class="cls_042"><span class="cls_042">a, b = 0, 1</span></div>
<div style="position:absolute;left:37.68px;top:377.76px" class="cls_042"><span class="cls_042">while b &lt; n:</span></div>
<div style="position:absolute;left:274.85px;top:375.48px" class="cls_033"><span class="cls_033">• In the main program, import the</span></div>
<div style="position:absolute;left:57.63px;top:398.64px" class="cls_042"><span class="cls_042">result.append(b)</span></div>
<div style="position:absolute;left:301.85px;top:404.52px" class="cls_033"><span class="cls_033">module and use the functions</span></div>
<div style="position:absolute;left:57.63px;top:420.72px" class="cls_042"><span class="cls_042">a, b = b, a+b</span></div>
<div style="position:absolute;left:37.73px;top:441.84px" class="cls_042"><span class="cls_042">return result</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:144.24px;top:16.56px" class="cls_008"><span class="cls_008">Common Modules: random</span></div>
<div style="position:absolute;left:61.39px;top:96.88px" class="cls_012"><span class="cls_012">• The Random module has functions to</span></div>
<div style="position:absolute;left:88.39px;top:135.76px" class="cls_012"><span class="cls_012">generate pseudo random numbers</span></div>
<div style="position:absolute;left:61.39px;top:181.84px" class="cls_012"><span class="cls_012">• random.random() gives a pseudo</span></div>
<div style="position:absolute;left:88.39px;top:219.76px" class="cls_012"><span class="cls_012">random number</span></div>
<div style="position:absolute;left:61.39px;top:266.80px" class="cls_012"><span class="cls_012">• Random.randint(5,10) return a pseudo</span></div>
<div style="position:absolute;left:88.39px;top:304.72px" class="cls_012"><span class="cls_012">random integer between 5 and 9</span></div>
<div style="position:absolute;left:240.36px;top:356.88px" class="cls_010"><span class="cls_010">import </span><span class="cls_027">random</span></div>
<div style="position:absolute;left:240.36px;top:378.00px" class="cls_026"><span class="cls_026">for i in range(10):</span></div>
<div style="position:absolute;left:276.36px;top:400.08px" class="cls_026"><span class="cls_026">x = random.random()</span></div>
<div style="position:absolute;left:276.36px;top:421.92px" class="cls_026"><span class="cls_026">print(x)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:167.28px;top:16.56px" class="cls_008"><span class="cls_008">Common Modules: math</span></div>
<div style="position:absolute;left:61.39px;top:96.88px" class="cls_012"><span class="cls_012">• The Math module has functions to</span></div>
<div style="position:absolute;left:88.39px;top:135.76px" class="cls_012"><span class="cls_012">perform familiar mathematical functions</span></div>
<div style="position:absolute;left:61.39px;top:181.84px" class="cls_012"><span class="cls_012">• math.log10(x) gives the log to base 10</span></div>
<div style="position:absolute;left:88.39px;top:219.76px" class="cls_012"><span class="cls_012">of x. This is used in regressions</span></div>
<div style="position:absolute;left:61.39px;top:266.80px" class="cls_012"><span class="cls_012">• math.sqrt(x) gives the square root of x</span></div>
<div style="position:absolute;left:240.36px;top:368.16px" class="cls_010"><span class="cls_010">import </span><span class="cls_027">math</span></div>
<div style="position:absolute;left:240.36px;top:390.24px" class="cls_026"><span class="cls_026">print(math.sqrt(x))</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:193.20px;top:16.56px" class="cls_008"><span class="cls_008">Common Modules: re</span></div>
<div style="position:absolute;left:61.39px;top:96.88px" class="cls_012"><span class="cls_012">• The re module has functions to perform</span></div>
<div style="position:absolute;left:88.39px;top:135.76px" class="cls_012"><span class="cls_012">pattern matching</span></div>
<div style="position:absolute;left:61.39px;top:181.84px" class="cls_012"><span class="cls_012">• Pattern matching is used to manipulate</span></div>
<div style="position:absolute;left:88.39px;top:219.76px" class="cls_012"><span class="cls_012">textual data</span></div>
<div style="position:absolute;left:61.39px;top:266.80px" class="cls_012"><span class="cls_012">• re.match() and re.search() are useful</span></div>
<div style="position:absolute;left:88.39px;top:304.24px" class="cls_012"><span class="cls_012">functions</span></div>
<div style="position:absolute;left:316.85px;top:331.20px" class="cls_010"><span class="cls_010">import</span><span class="cls_027"> re</span></div>
<div style="position:absolute;left:316.85px;top:352.08px" class="cls_027"><span class="cls_027">pattern = "Cookie"</span></div>
<div style="position:absolute;left:316.85px;top:374.16px" class="cls_027"><span class="cls_027">sequence = "Cookie"</span></div>
<div style="position:absolute;left:316.85px;top:395.28px" class="cls_027"><span class="cls_027">if re.match(pattern, sequence):</span></div>
<div style="position:absolute;left:339.47px;top:417.12px" class="cls_027"><span class="cls_027">print("Match!")</span></div>
<div style="position:absolute;left:316.85px;top:439.20px" class="cls_027"><span class="cls_027">else: print("Not a match!")</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:277.20px;top:16.56px" class="cls_008"><span class="cls_008">Summary</span></div>
<div style="position:absolute;left:81.82px;top:89.16px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> Functions</span></div>
<div style="position:absolute;left:117.82px;top:131.04px" class="cls_046"><span class="cls_046">§</span><span class="cls_047"> Built-in functions</span></div>
<div style="position:absolute;left:117.82px;top:169.20px" class="cls_046"><span class="cls_046">§</span><span class="cls_047"> User defined functions</span></div>
<div style="position:absolute;left:81.82px;top:206.04px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> Built-in functions are not reserved words</span></div>
<div style="position:absolute;left:81.82px;top:249.00px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> def command used to define a function</span></div>
<div style="position:absolute;left:81.82px;top:291.00px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> Arguments, Parameters & Return values</span></div>
<div style="position:absolute;left:81.82px;top:333.00px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> Guidelines for user function definitions</span></div>
<div style="position:absolute;left:81.82px;top:376.20px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> Modules</span></div>
<div style="position:absolute;left:81.82px;top:418.20px" class="cls_044"><span class="cls_044">§</span><span class="cls_045"> Common Modules</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:146.50px;top:16.56px" class="cls_008"><span class="cls_008">To Do List for Next Class</span></div>
<div style="position:absolute;left:107.95px;top:96.16px" class="cls_048"><span class="cls_048">• Read Chapters 4 and 5</span></div>
<div style="position:absolute;left:107.95px;top:146.08px" class="cls_048"><span class="cls_048">• Execute code snippets and</span></div>
<div style="position:absolute;left:134.95px;top:192.16px" class="cls_048"><span class="cls_048">chapter exercises in the book</span></div>
<div style="position:absolute;left:107.95px;top:250.24px" class="cls_048"><span class="cls_048">• Start working on Homework 1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.21px;top:16.56px" class="cls_008"><span class="cls_008">Acknowledgements/Contribu</span></div>
<div style="position:absolute;left:316.31px;top:59.76px" class="cls_008"><span class="cls_008">tions</span></div>
<div style="position:absolute;left:107.95px;top:127.44px" class="cls_049"><span class="cls_049">§</span><span class="cls_050">  These slides are Copyright 2010-  Charles R. Severance</span></div>
<div style="position:absolute;left:134.95px;top:148.56px" class="cls_050"><span class="cls_050">(</span><A HREF="http://www.dr-chuck.com/">www.dr-chuck.com</A>) of the University of Michigan School of</div>
<div style="position:absolute;left:134.95px;top:170.40px" class="cls_050"><span class="cls_050">Information and made available under a Creative</span></div>
<div style="position:absolute;left:134.95px;top:191.52px" class="cls_050"><span class="cls_050">Commons Attribution 4.0 License.  Please maintain this last</span></div>
<div style="position:absolute;left:134.95px;top:213.36px" class="cls_050"><span class="cls_050">slide in all copies of the document to comply with the</span></div>
<div style="position:absolute;left:134.95px;top:235.44px" class="cls_050"><span class="cls_050">attribution requirements of the license.  If you make a</span></div>
<div style="position:absolute;left:134.95px;top:256.56px" class="cls_050"><span class="cls_050">change, feel free to add your name and organization to the</span></div>
<div style="position:absolute;left:134.95px;top:278.40px" class="cls_050"><span class="cls_050">list of contributors on this page as you republish the</span></div>
<div style="position:absolute;left:134.95px;top:299.52px" class="cls_050"><span class="cls_050">materials.</span></div>
<div style="position:absolute;left:107.95px;top:352.56px" class="cls_049"><span class="cls_049">§</span><span class="cls_050">  Initial Development: Charles Severance, University of</span></div>
<div style="position:absolute;left:134.90px;top:374.40px" class="cls_050"><span class="cls_050">Michigan School of Information</span></div>
<div style="position:absolute;left:107.95px;top:427.44px" class="cls_049"><span class="cls_049">§</span><span class="cls_050">  Updated 2018: Jayarajan Samuel, University of Texas at</span></div>
<div style="position:absolute;left:134.90px;top:448.56px" class="cls_050"><span class="cls_050">Arlington, College of Business</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6Functions/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:521.76px" class="cls_006"><span class="cls_006">INSY5339</span></div>
</div>

</body>
</html>
