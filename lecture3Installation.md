<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_028{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:32.0px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:14.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:14.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:14.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:14.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_029{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:20.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:20.1px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:14.0px;color:rgb(0,111,192);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:14.0px;color:rgb(0,111,192);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:18.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:18.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:16.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:16.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:16.0px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:16.0px;color:rgb(64,63,64);font-weight:normal;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture3Installation/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:45.31px;top:306.16px" class="cls_002"><span class="cls_002">PYTHON PROGRAMMING</span></div>
<div style="position:absolute;left:44.93px;top:353.76px" class="cls_003"><span class="cls_003">INSY 5336</span></div>
<div style="position:absolute;left:44.56px;top:407.04px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:45.31px;top:443.92px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
<div style="position:absolute;left:45.31px;top:470.80px" class="cls_005"><span class="cls_005">Information Systems and Operations Management</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:87.48px;top:64.08px" class="cls_007"><span class="cls_007">Setting up Python</span></div>
<div style="position:absolute;left:107.95px;top:144.88px" class="cls_006"><span class="cls_006">§ Python setup details:</span></div>
<div style="position:absolute;left:143.95px;top:198.88px" class="cls_006"><span class="cls_006">§ We will use Python 3.X</span></div>
<div style="position:absolute;left:143.95px;top:252.88px" class="cls_006"><span class="cls_006">§ (not python 2.x; there are</span></div>
<div style="position:absolute;left:170.95px;top:298.96px" class="cls_006"><span class="cls_006">some difference)</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:50.71px;top:57.60px" class="cls_007"><span class="cls_007">Setting up Python</span></div>
<div style="position:absolute;left:47.06px;top:133.36px" class="cls_008"><span class="cls_008">§ Python setup details:</span></div>
<div style="position:absolute;left:83.06px;top:180.16px" class="cls_008"><span class="cls_008">§ We also need the following packages</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:72.00px;top:46.80px" class="cls_007"><span class="cls_007">Setting up Python</span></div>
<div style="position:absolute;left:107.95px;top:118.48px" class="cls_006"><span class="cls_006">§ Anaconda is the most popular</span></div>
<div style="position:absolute;left:134.95px;top:164.56px" class="cls_006"><span class="cls_006">Python Data Science platform</span></div>
<div style="position:absolute;left:107.95px;top:218.56px" class="cls_006"><span class="cls_006">§ Anaconda is downloadable at:</span></div>
<div style="position:absolute;left:134.95px;top:264.64px" class="cls_028"><span class="cls_028"> </span><A HREF="https://www.anaconda.com/">https://www.anaconda.com</A> </div>
<div style="position:absolute;left:107.95px;top:318.64px" class="cls_006"><span class="cls_006">§ Jupyter Notebook is a web based</span></div>
<div style="position:absolute;left:134.95px;top:364.48px" class="cls_006"><span class="cls_006">Python IDE</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">Setting up Python</span></div>
<div style="position:absolute;left:47.06px;top:76.48px" class="cls_006"><span class="cls_006">§ How to run Python</span></div>
<div style="position:absolute;left:82.06px;top:118.48px" class="cls_010"><span class="cls_010">- The Jupyter shell</span></div>
<div style="position:absolute;left:118.06px;top:150.56px" class="cls_011"><span class="cls_011">• For interactive testing of statements</span></div>
<div style="position:absolute;left:118.06px;top:176.48px" class="cls_011"><span class="cls_011">• Type </span><span class="cls_012">jupyter</span><span class="cls_011"> on the command line</span></div>
<div style="position:absolute;left:82.06px;top:234.64px" class="cls_010"><span class="cls_010">- Can also run programs directly from the command</span></div>
<div style="position:absolute;left:104.56px;top:259.60px" class="cls_010"><span class="cls_010">line</span></div>
<div style="position:absolute;left:118.06px;top:290.48px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> python filename.py</span></div>
<div style="position:absolute;left:118.06px;top:316.64px" class="cls_011"><span class="cls_011">• will run the python code in filename.py</span></div>
<div style="position:absolute;left:82.06px;top:374.56px" class="cls_010"><span class="cls_010">- Jupyter notebook</span></div>
<div style="position:absolute;left:118.06px;top:405.44px" class="cls_011"><span class="cls_011">• Combine code, results, and text in one webpage</span></div>
<div style="position:absolute;left:118.06px;top:431.60px" class="cls_011"><span class="cls_011">• Run </span><span class="cls_012">jupyter notebook </span><span class="cls_011">on the commandline</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">Setting up Python</span></div>
<div style="position:absolute;left:57.90px;top:99.04px" class="cls_006"><span class="cls_006">§ Most of the code I show will be in</span></div>
<div style="position:absolute;left:84.90px;top:144.88px" class="cls_006"><span class="cls_006">notebook form</span></div>
<div style="position:absolute;left:57.90px;top:238.00px" class="cls_006"><span class="cls_006">§ For assignments, you will create</span></div>
<div style="position:absolute;left:84.90px;top:284.08px" class="cls_006"><span class="cls_006">notebooks</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:57.90px;top:99.04px" class="cls_006"><span class="cls_006">§ First, install Python using the Anaconda</span></div>
<div style="position:absolute;left:84.90px;top:144.88px" class="cls_006"><span class="cls_006">website</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:57.90px;top:99.04px" class="cls_006"><span class="cls_006">§ Open </span><span class="cls_013">jupyter notebook</span><span class="cls_006">:</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:200.08px;top:100.56px" class="cls_014"><span class="cls_014">Change the name</span></div>
<div style="position:absolute;left:209.58px;top:121.68px" class="cls_014"><span class="cls_014">of the notebook</span></div>
<div style="position:absolute;left:197.46px;top:341.28px" class="cls_014"><span class="cls_014">Write some code here;</span></div>
<div style="position:absolute;left:221.29px;top:362.40px" class="cls_014"><span class="cls_014">then hit Alt-Enter</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:278.00px;top:144.72px" class="cls_014"><span class="cls_014">Change the cell type</span></div>
<div style="position:absolute;left:302.50px;top:165.84px" class="cls_014"><span class="cls_014">to “Markdown”</span></div>
<div style="position:absolute;left:328.00px;top:301.44px" class="cls_014"><span class="cls_014">Output of the code</span></div>
<div style="position:absolute;left:158.28px;top:395.76px" class="cls_014"><span class="cls_014">New cell</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:358.90px;top:384.32px" class="cls_015"><span class="cls_015">This cell is now interpreted as</span></div>
<div style="position:absolute;left:357.47px;top:403.28px" class="cls_015"><span class="cls_015">“Markdown”, which is text with</span></div>
<div style="position:absolute;left:345.72px;top:422.24px" class="cls_015"><span class="cls_015">q _</span><span class="cls_016">italics</span><span class="cls_015">_</span></div>
<div style="position:absolute;left:345.72px;top:441.20px" class="cls_015"><span class="cls_015">q</span><span class="cls_017"> **bold**</span></div>
<div style="position:absolute;left:345.72px;top:460.16px" class="cls_015"><span class="cls_015">q and many other options</span></div>
<div style="position:absolute;left:397.97px;top:480.32px" class="cls_015"><span class="cls_015">Again, hit Alt-Enter</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:487.53px;top:294.72px" class="cls_014"><span class="cls_014">The Markdown cell</span></div>
<div style="position:absolute;left:487.03px;top:315.84px" class="cls_014"><span class="cls_014">gets converted into</span></div>
<div style="position:absolute;left:481.53px;top:337.92px" class="cls_014"><span class="cls_014">italicized and bolded</span></div>
<div style="position:absolute;left:548.53px;top:358.80px" class="cls_014"><span class="cls_014">text</span></div>
<div style="position:absolute;left:13.08px;top:418.56px" class="cls_014"><span class="cls_014">Save when</span></div>
<div style="position:absolute;left:8.08px;top:439.44px" class="cls_014"><span class="cls_014">you’re done!</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">How to create Python notebooks</span></div>
<div style="position:absolute;left:284.36px;top:397.12px" class="cls_018"><span class="cls_018">The code just created is saved under your Jupyter notebook</span></div>
<div style="position:absolute;left:284.36px;top:413.20px" class="cls_018"><span class="cls_018">Directory. The default directory is:</span></div>
<div style="position:absolute;left:284.36px;top:430.00px" class="cls_019"><span class="cls_019">C:\Users\username</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">Change Jupyter Notebook Dir</span></div>
<div style="position:absolute;left:47.06px;top:77.44px" class="cls_005"><span class="cls_005">The detailed operation of changing the jupyter notebook directory varies according to your</span></div>
<div style="position:absolute;left:47.06px;top:100.48px" class="cls_005"><span class="cls_005">operating system. The following instructions is specific to Windows 10.</span></div>
<div style="position:absolute;left:47.06px;top:125.44px" class="cls_005"><span class="cls_005">For other operating systems please Gooogle online. Below is the reference link:</span></div>
<div style="position:absolute;left:47.06px;top:148.48px" class="cls_029"><span class="cls_029"> </span><A HREF="https://stackoverflow.com/questions/35254852/how-to-change-the-jupyter-start-up-folder">https://stackoverflow.com/questions/35254852/how-to-change-the-jupyter-start-up-folder</A> </div>
<div style="position:absolute;left:47.06px;top:174.64px" class="cls_021"><span class="cls_021">§  Update </span><span class="cls_022">jupyter_notebook_config.py</span><span class="cls_021"> file</span></div>
<div style="position:absolute;left:82.06px;top:207.60px" class="cls_023"><span class="cls_023">- Open cmd (or Anaconda Prompt) and run</span></div>
<div style="position:absolute;left:118.06px;top:232.48px" class="cls_024"><span class="cls_024">jupyter notebook  --generate--config</span></div>
<div style="position:absolute;left:82.06px;top:253.44px" class="cls_023"><span class="cls_023">- This writes a file to</span></div>
<div style="position:absolute;left:118.06px;top:278.56px" class="cls_019"><span class="cls_019">C:\Users\username\.jupyter\jupyter_notebook_config.py</span></div>
<div style="position:absolute;left:82.06px;top:319.44px" class="cls_023"><span class="cls_023">- Open the above file and search for the following line</span></div>
<div style="position:absolute;left:118.06px;top:344.56px" class="cls_019"><span class="cls_019">#c.NotebookApp.notebook_dir = ‘ ‘</span></div>
<div style="position:absolute;left:82.06px;top:365.52px" class="cls_023"><span class="cls_023">- Uncomment the above line by removing the </span><span class="cls_025"># </span><span class="cls_023">at the beginning</span></div>
<div style="position:absolute;left:82.06px;top:391.44px" class="cls_023"><span class="cls_023">- Set the dir to your own path</span></div>
<div style="position:absolute;left:82.06px;top:417.60px" class="cls_023"><span class="cls_023">- Save the file</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">Change Jupyter Notebook Dir</span></div>
<div style="position:absolute;left:47.06px;top:78.64px" class="cls_021"><span class="cls_021">§  Change the Properties of the </span><span class="cls_022">Jupyter Notebook App</span></div>
<div style="position:absolute;left:83.06px;top:112.64px" class="cls_026"><span class="cls_026">§</span><span class="cls_027">  Navigate to</span></div>
<div style="position:absolute;left:83.06px;top:140.48px" class="cls_027"><span class="cls_027">C:\Users\username\AppData\Roaming\Microsoft\Windows\Start</span></div>
<div style="position:absolute;left:83.06px;top:167.60px" class="cls_027"><span class="cls_027">Menu\Programs</span></div>
<div style="position:absolute;left:83.06px;top:222.56px" class="cls_026"><span class="cls_026">§</span><span class="cls_027">  Right Click the Jupyter Notebook App and Choose Properties</span></div>
<div style="position:absolute;left:83.06px;top:250.64px" class="cls_026"><span class="cls_026">§</span><span class="cls_027">  Remove %USERPROFILE% from your Jupyter Notebook shortcut Target</span></div>
<div style="position:absolute;left:105.56px;top:277.52px" class="cls_027"><span class="cls_027">field.  And after \jupyter-notebook-script.py, type your own path.</span></div>
<div style="position:absolute;left:83.06px;top:305.60px" class="cls_026"><span class="cls_026">§</span><span class="cls_027">  Also replace the Start In field with your own path</span></div>
<div style="position:absolute;left:83.06px;top:333.44px" class="cls_026"><span class="cls_026">§</span><span class="cls_027">  Click Apply</span></div>
<div style="position:absolute;left:83.06px;top:361.52px" class="cls_026"><span class="cls_026">§</span><span class="cls_027">  Click Ok to save the change</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:22.80px" class="cls_007"><span class="cls_007">Change Jupyter Notebook Dir</span></div>
<div style="position:absolute;left:47.06px;top:78.64px" class="cls_021"><span class="cls_021">§  Change the Properties of the </span><span class="cls_022">Jupyter Notebook App</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:46.06px;top:4.32px" class="cls_007"><span class="cls_007">Change Jupyter Notebook Dir</span></div>
<div style="position:absolute;left:47.06px;top:61.60px" class="cls_021"><span class="cls_021">§  Create Scripts Using by creating a new text file</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:27.48px;top:64.56px" class="cls_007"><span class="cls_007">Saved Scripts</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3Installation/background21.jpg" width=720 height=540></div>
</div>

</body>
</html>
