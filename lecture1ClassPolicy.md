<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:38.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:24.1px;color:rgb(18,63,159);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(64,63,64);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(64,63,64);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_025{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_026{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_027{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Times,serif;font-size:18.1px;color:rgb(51,51,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Times,serif;font-size:18.1px;color:rgb(51,51,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_028{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_029{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_030{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_031{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="ClassPolicy/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:45.31px;top:306.16px" class="cls_002"><span class="cls_002">PYTHON PROGRAMMING</span></div>
<div style="position:absolute;left:44.93px;top:353.76px" class="cls_003"><span class="cls_003">INSY 5336</span></div>
<div style="position:absolute;left:44.56px;top:407.04px" class="cls_004"><span class="cls_004">Zhuojun Gu</span></div>
<div style="position:absolute;left:45.31px;top:443.92px" class="cls_005"><span class="cls_005">Assistant Professor</span></div>
<div style="position:absolute;left:45.31px;top:470.80px" class="cls_005"><span class="cls_005">Information Systems and Operations Management</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.20px;top:41.44px" class="cls_006"><span class="cls_006">Materials</span></div>
<div style="position:absolute;left:43.20px;top:85.12px" class="cls_007"><span class="cls_007">•  Canvas, </span><A HREF="https://uta.instructure.com/courses/96343">https://uta.instructure.com/courses/96343</A> </span></div>
<div style="position:absolute;left:79.20px;top:142.16px" class="cls_009"><span class="cls_009">-  Syllabus,</span></div>
<div style="position:absolute;left:79.20px;top:165.20px" class="cls_009"><span class="cls_009">-  Lecture slides, articles, assignments, supplementary materials,</span></div>
<div style="position:absolute;left:101.70px;top:184.16px" class="cls_009"><span class="cls_009">grades will be posted on Canvas</span></div>
<div style="position:absolute;left:79.20px;top:207.20px" class="cls_009"><span class="cls_009">-  Important announcements will also be made on Canvas</span></div>
<div style="position:absolute;left:79.20px;top:231.20px" class="cls_009"><span class="cls_009">-  Check your email constantly in case you miss any important updates</span></div>
<div style="position:absolute;left:101.70px;top:250.16px" class="cls_009"><span class="cls_009">or notifications</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.35px;top:64.48px" class="cls_006"><span class="cls_006">Office Hour</span></div>
<div style="position:absolute;left:62.51px;top:114.40px" class="cls_007"><span class="cls_007">•  Instructor (</span><span class="cls_009">Zhuojun Gu</span><span class="cls_007">) office hour</span></div>
<div style="position:absolute;left:98.51px;top:142.40px" class="cls_009"><span class="cls_009">-  Thursday: 1:00 pm - 3:00 pm  @ Canvas</span></div>
<div style="position:absolute;left:98.51px;top:165.44px" class="cls_009"><span class="cls_009">-  Email: zhuojun.gu@uta.edu</span></div>
<div style="position:absolute;left:62.51px;top:212.32px" class="cls_007"><span class="cls_007">•  TA (Siddhi Nair) office hour</span></div>
<div style="position:absolute;left:98.51px;top:240.32px" class="cls_009"><span class="cls_009">-  Tuesday: 4:00 pm - 6:00 pm</span></div>
<div style="position:absolute;left:98.51px;top:263.36px" class="cls_009"><span class="cls_009">-  Email:</span></div>
<div style="position:absolute;left:134.51px;top:286.32px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> </span><span class="cls_026">siddhiramanand.nair@uta.edu</span></div>
<div style="position:absolute;left:62.51px;top:313.44px" class="cls_010"><span class="cls_010">P.S. Don’t wait till the due date of your assignments to make an</span></div>
<div style="position:absolute;left:62.51px;top:334.48px" class="cls_010"><span class="cls_010">appointment</span><span class="cls_007">.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:38.09px;top:43.84px" class="cls_006"><span class="cls_006">Textbooks</span></div>
<div style="position:absolute;left:39.74px;top:85.12px" class="cls_007"><span class="cls_007">•  Python for Everybody: Exploring Data in Python 3 by Dr.</span></div>
<div style="position:absolute;left:66.74px;top:109.12px" class="cls_007"><span class="cls_007">Charles Severance, 2009.</span></div>
<div style="position:absolute;left:75.74px;top:160.16px" class="cls_009"><span class="cls_009">-  The book may be downloaded from </span><A HREF="https://www.py4e.com/book.php">https://www.py4e.com/book.php</A> </span></div>
<div style="position:absolute;left:39.74px;top:213.04px" class="cls_007"><span class="cls_007">•  Other Materials/Resources/Readings</span></div>
<div style="position:absolute;left:75.74px;top:241.04px" class="cls_009"><span class="cls_009">-  Python for Data Analysis by Wes McKinney</span></div>
<div style="position:absolute;left:75.74px;top:264.08px" class="cls_009"><span class="cls_009">-</span><span class="cls_012">  </span><span class="cls_027">https://www.kevinsheppard.com/Main_Page</span></div>
<div style="position:absolute;left:75.74px;top:287.12px" class="cls_009"><span class="cls_009">-</span><span class="cls_012">  </span><span class="cls_027">https://developers.google.com/edu/python/</span></div>
<div style="position:absolute;left:75.74px;top:310.16px" class="cls_009"><span class="cls_009">-  Also check out </span><A HREF="http://www.coursera.org/">www.coursera.org</A><span class="cls_009"> and </span><A HREF="http://www.udacity.com/">www.udacity.com</A> </span><span class="cls_009"> for</span></div>
<div style="position:absolute;left:98.24px;top:329.12px" class="cls_009"><span class="cls_009">introductory Python courses</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:42.16px;top:33.56px" class="cls_013"><span class="cls_013">Software</span></div>
<div style="position:absolute;left:42.16px;top:78.00px" class="cls_014"><span class="cls_014">Install  Python</span></div>
<div style="position:absolute;left:159.48px;top:78.00px" class="cls_014"><span class="cls_014">3 and  Jupyter  using  the</span><span class="cls_015">  </span><A HREF="https://www.anaconda.com/downloads">Anaconda  Distribution</A> </span><span class="cls_014">,  which</span></div>
<div style="position:absolute;left:42.66px;top:100.08px" class="cls_014"><span class="cls_014">includes Python, the Jupyter Notebook (used for python scripts), and other</span></div>
<div style="position:absolute;left:42.66px;top:121.92px" class="cls_014"><span class="cls_014">commonly used packages for scientific computing and data science.</span></div>
<div style="position:absolute;left:42.16px;top:159.36px" class="cls_016"><span class="cls_016">More details will be given in class.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.62px;top:40.48px" class="cls_006"><span class="cls_006">Course Evaluation and Grading Schema</span></div>
<div style="position:absolute;left:41.40px;top:94.80px" class="cls_017"><span class="cls_017">Assignment</span></div>
<div style="position:absolute;left:346.56px;top:94.80px" class="cls_017"><span class="cls_017">Weight</span></div>
<div style="position:absolute;left:466.77px;top:94.80px" class="cls_017"><span class="cls_017">Final Average</span></div>
<div style="position:absolute;left:588.02px;top:94.80px" class="cls_017"><span class="cls_017">Final Letter</span></div>
<div style="position:absolute;left:41.40px;top:109.44px" class="cls_018"><span class="cls_018">Homework</span></div>
<div style="position:absolute;left:588.02px;top:109.92px" class="cls_017"><span class="cls_017">Grade</span></div>
<div style="position:absolute;left:197.96px;top:124.56px" class="cls_018"><span class="cls_018">3 Homework Assignments</span></div>
<div style="position:absolute;left:370.44px;top:124.56px" class="cls_018"><span class="cls_018">30% in total</span></div>
<div style="position:absolute;left:466.77px;top:125.04px" class="cls_018"><span class="cls_018">90.00%+</span></div>
<div style="position:absolute;left:588.02px;top:125.04px" class="cls_018"><span class="cls_018">A</span></div>
<div style="position:absolute;left:466.77px;top:139.68px" class="cls_018"><span class="cls_018">80.00%+</span></div>
<div style="position:absolute;left:588.02px;top:139.68px" class="cls_018"><span class="cls_018">B</span></div>
<div style="position:absolute;left:41.40px;top:148.80px" class="cls_018"><span class="cls_018">Exam 1</span></div>
<div style="position:absolute;left:346.56px;top:148.80px" class="cls_018"><span class="cls_018">20%</span></div>
<div style="position:absolute;left:466.77px;top:154.32px" class="cls_018"><span class="cls_018">70.00%+</span></div>
<div style="position:absolute;left:588.02px;top:154.32px" class="cls_018"><span class="cls_018">C</span></div>
<div style="position:absolute;left:41.40px;top:163.44px" class="cls_018"><span class="cls_018">Exam 2</span></div>
<div style="position:absolute;left:346.56px;top:163.44px" class="cls_018"><span class="cls_018">20%</span></div>
<div style="position:absolute;left:466.77px;top:169.20px" class="cls_018"><span class="cls_018">60.00%+</span></div>
<div style="position:absolute;left:588.02px;top:169.20px" class="cls_018"><span class="cls_018">D</span></div>
<div style="position:absolute;left:41.40px;top:178.08px" class="cls_018"><span class="cls_018">Exam 3</span></div>
<div style="position:absolute;left:346.56px;top:178.08px" class="cls_018"><span class="cls_018">20%</span></div>
<div style="position:absolute;left:466.77px;top:183.84px" class="cls_018"><span class="cls_018">&lt;60%</span></div>
<div style="position:absolute;left:588.02px;top:183.84px" class="cls_018"><span class="cls_018">F</span></div>
<div style="position:absolute;left:41.40px;top:192.96px" class="cls_018"><span class="cls_018">Final Project</span></div>
<div style="position:absolute;left:346.56px;top:192.96px" class="cls_018"><span class="cls_018">10%</span></div>
<div style="position:absolute;left:41.40px;top:207.60px" class="cls_017"><span class="cls_017">Total</span></div>
<div style="position:absolute;left:346.56px;top:207.60px" class="cls_018"><span class="cls_018">100%</span></div>
<div style="position:absolute;left:54.04px;top:272.56px" class="cls_019"><span class="cls_019">Expectations for Out-of-Class Study:</span></div>
<div style="position:absolute;left:54.04px;top:296.64px" class="cls_016"><span class="cls_016">Beyond the time required to attend each class meeting, students enrolled in this course</span></div>
<div style="position:absolute;left:54.54px;top:319.68px" class="cls_016"><span class="cls_016">should expect to spend at least an additional 9 hours per week of their own time in</span></div>
<div style="position:absolute;left:54.54px;top:341.52px" class="cls_016"><span class="cls_016">course-related   activities,   including   reading   required   materials,   completing</span></div>
<div style="position:absolute;left:54.54px;top:363.60px" class="cls_016"><span class="cls_016">assignments, and preparing for exams/quizzes.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:35.80px;top:43.12px" class="cls_006"><span class="cls_006">Important Class Policies</span></div>
<div style="position:absolute;left:35.80px;top:91.12px" class="cls_007"><span class="cls_007">•  Class attendance</span></div>
<div style="position:absolute;left:71.80px;top:119.12px" class="cls_009"><span class="cls_009">-  Attendance is not mandatory. We may cover content that is not in the book.</span></div>
<div style="position:absolute;left:71.80px;top:142.16px" class="cls_009"><span class="cls_009">-  If you miss a class, you are responsible for the materials covered.</span></div>
<div style="position:absolute;left:71.80px;top:165.20px" class="cls_009"><span class="cls_009">-  Please see the syllabus and our email for more details regarding the 50/50</span></div>
<div style="position:absolute;left:94.30px;top:185.12px" class="cls_009"><span class="cls_009">classroom rotation plan.</span></div>
<div style="position:absolute;left:35.80px;top:237.04px" class="cls_007"><span class="cls_007">•  Homework submission and appealing scores</span></div>
<div style="position:absolute;left:71.80px;top:288.08px" class="cls_009"><span class="cls_009">-  You will have 3 homework exercises and one individual final project. You will</span></div>
<div style="position:absolute;left:94.30px;top:308.00px" class="cls_009"><span class="cls_009">have about </span><span class="cls_029">a week</span><span class="cls_009"> to complete the homework exercises. Details of the final</span></div>
<div style="position:absolute;left:94.30px;top:327.20px" class="cls_009"><span class="cls_009">project will be provided in class and/or Canvas. Assignments must be turned</span></div>
<div style="position:absolute;left:94.30px;top:346.16px" class="cls_009"><span class="cls_009">in electronically (via Canvas) by the due date and time specified by the</span></div>
<div style="position:absolute;left:94.30px;top:365.12px" class="cls_009"><span class="cls_009">instructor. </span><span class="cls_029">Late submissions will receive a score of 0 (no exceptions)</span><span class="cls_009">.</span></div>
<div style="position:absolute;left:71.80px;top:411.20px" class="cls_009"><span class="cls_009">-  All score appealing for individual assignments/exams must be submitted</span></div>
<div style="position:absolute;left:94.30px;top:430.16px" class="cls_009"><span class="cls_009">within one week of the date the graded item is returned to the class.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:35.80px;top:43.12px" class="cls_006"><span class="cls_006">Important Class Policies</span></div>
<div style="position:absolute;left:42.04px;top:103.12px" class="cls_007"><span class="cls_007">•  Exams</span></div>
<div style="position:absolute;left:78.04px;top:132.16px" class="cls_007"><span class="cls_007">- There are </span><span class="cls_030">no make-up exams</span><span class="cls_007">. Under extenuating</span></div>
<div style="position:absolute;left:100.54px;top:156.16px" class="cls_007"><span class="cls_007">circumstances (e.g., medical emergency, family emergency,</span></div>
<div style="position:absolute;left:100.54px;top:180.16px" class="cls_007"><span class="cls_007">work-related travel, etc.), the average score of other exams</span></div>
<div style="position:absolute;left:100.54px;top:204.16px" class="cls_007"><span class="cls_007">will replace the missed exam score. You can only use this</span></div>
<div style="position:absolute;left:100.54px;top:228.16px" class="cls_007"><span class="cls_007">excuse for </span><span class="cls_031">one</span><span class="cls_007"> exam.</span></div>
<div style="position:absolute;left:78.04px;top:257.20px" class="cls_007"><span class="cls_007">- The final exam will be comprehensive covering all the</span></div>
<div style="position:absolute;left:100.54px;top:281.20px" class="cls_007"><span class="cls_007">contents, whereas regular exams will cover partial contents</span></div>
<div style="position:absolute;left:100.54px;top:305.20px" class="cls_007"><span class="cls_007">(as described in Course Schedule)</span><span class="cls_009">.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.82px;top:42.64px" class="cls_006"><span class="cls_006">Important Class Policies</span></div>
<div style="position:absolute;left:42.04px;top:103.12px" class="cls_007"><span class="cls_007">•  Drop Policy</span></div>
<div style="position:absolute;left:78.04px;top:132.00px" class="cls_010"><span class="cls_010">- After the late registration period, students must see their academic</span></div>
<div style="position:absolute;left:100.54px;top:153.12px" class="cls_010"><span class="cls_010">advisor to drop a class or withdraw. Undeclared students must see</span></div>
<div style="position:absolute;left:100.54px;top:175.20px" class="cls_010"><span class="cls_010">an advisor in the University Advising Center .</span></div>
<div style="position:absolute;left:78.04px;top:201.12px" class="cls_010"><span class="cls_010">- Drops can continue through a point two-thirds of the way through the</span></div>
<div style="position:absolute;left:100.54px;top:223.20px" class="cls_010"><span class="cls_010">term or session.</span></div>
<div style="position:absolute;left:78.04px;top:248.16px" class="cls_010"><span class="cls_010">- It is the student's responsibility to officially withdraw if they do not</span></div>
<div style="position:absolute;left:100.54px;top:272.16px" class="cls_010"><span class="cls_010">plan to attend after registering.</span><span class="cls_021"> Students will not be automatically</span></div>
<div style="position:absolute;left:100.54px;top:294.00px" class="cls_021"><span class="cls_021">dropped for non-attendance</span><span class="cls_010">.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.82px;top:42.64px" class="cls_006"><span class="cls_006">Important Class Policies</span></div>
<div style="position:absolute;left:42.04px;top:103.12px" class="cls_007"><span class="cls_007">•  Disability Accommodations</span></div>
<div style="position:absolute;left:78.04px;top:158.16px" class="cls_010"><span class="cls_010">- Please notify your instructor of any modification/adaptation you may</span></div>
<div style="position:absolute;left:100.54px;top:179.04px" class="cls_010"><span class="cls_010">require to accommodate a disability-related need </span><span class="cls_021">within the first</span></div>
<div style="position:absolute;left:100.54px;top:201.12px" class="cls_021"><span class="cls_021">week</span><span class="cls_010"> of the semester (by </span><span class="cls_021">Sep 5</span><span class="cls_017"><sup>th</sup></span><span class="cls_021">, 2020</span><span class="cls_010">).</span></div>
<div style="position:absolute;left:78.04px;top:253.20px" class="cls_010"><span class="cls_010">- Students are responsible for providing the instructor with official</span></div>
<div style="position:absolute;left:100.54px;top:274.08px" class="cls_010"><span class="cls_010">notification in the form of </span><span class="cls_021">a letter certified by the Office for</span></div>
<div style="position:absolute;left:100.54px;top:296.16px" class="cls_021"><span class="cls_021">Students with Disabilities (OSD)</span><span class="cls_010">. .</span></div>
<div style="position:absolute;left:42.04px;top:347.12px" class="cls_009"><span class="cls_009">The Office for Students with Disabilities, (OSD) </span><A HREF="http://www.uta.edu/disability/">http://www.uta.edu/disability/</A> </span><span class="cls_009"> or calling</span></div>
<div style="position:absolute;left:42.04px;top:367.04px" class="cls_009"><span class="cls_009">817-272-3364</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.62px;top:29.20px" class="cls_022"><span class="cls_022">Academic Honesty</span></div>
<div style="position:absolute;left:61.40px;top:88.00px" class="cls_007"><span class="cls_007">•  UT Arlington student policies in effect</span></div>
<div style="position:absolute;left:97.40px;top:117.04px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> </span><span class="cls_025">https://www.uta.edu/conduct/</span></div>
<div style="position:absolute;left:97.40px;top:145.84px" class="cls_007"><span class="cls_007">- University students are expected to uphold the</span></div>
<div style="position:absolute;left:119.90px;top:169.84px" class="cls_007"><span class="cls_007">Student</span><span class="cls_020"> Honor Code</span><span class="cls_007">: “</span><span class="cls_023">I pledge, on my honor, to</span></div>
<div style="position:absolute;left:119.90px;top:193.84px" class="cls_023"><span class="cls_023">uphold UT Arlington’s tradition of academic integrity, a</span></div>
<div style="position:absolute;left:119.90px;top:217.84px" class="cls_023"><span class="cls_023">tradition that values hard work and honest effort in the</span></div>
<div style="position:absolute;left:119.90px;top:241.84px" class="cls_023"><span class="cls_023">pursuit of academic excellence</span><span class="cls_007">.</span><span class="cls_023"> I promise that I will</span></div>
<div style="position:absolute;left:119.90px;top:265.84px" class="cls_023"><span class="cls_023">submit only work that I personally create or contribute</span></div>
<div style="position:absolute;left:119.90px;top:289.84px" class="cls_023"><span class="cls_023">to group collaborations, and I will appropriately</span></div>
<div style="position:absolute;left:119.90px;top:313.84px" class="cls_023"><span class="cls_023">reference any work from other sources. I will follow the</span></div>
<div style="position:absolute;left:119.90px;top:337.84px" class="cls_023"><span class="cls_023">highest standards of integrity and uphold the spirit of</span></div>
<div style="position:absolute;left:119.90px;top:361.84px" class="cls_023"><span class="cls_023">the Honor Code.</span><span class="cls_007">”</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:43.20px;top:52.48px" class="cls_022"><span class="cls_022">How to use technology in class?</span></div>
<div style="position:absolute;left:67.97px;top:133.68px" class="cls_024"><span class="cls_024">This is a hands-on learning class.</span></div>
<div style="position:absolute;left:67.97px;top:166.56px" class="cls_024"><span class="cls_024">Please bring your laptop to class!</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="ClassPolicy/background13.jpg" width=720 height=540></div>
</div>

</body>
</html>
